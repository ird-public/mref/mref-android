package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class Patient (

    @SerializedName("data"   ) var data   :  Data = Data(),
    @SerializedName("status" ) var status : Int?  = null,
    @SerializedName("referalEntity") var referalEntity: String = "",

)