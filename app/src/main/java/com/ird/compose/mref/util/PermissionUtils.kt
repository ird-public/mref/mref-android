package com.ird.compose.mref.util

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment

object PermissionUtils {

    fun askAccessFineLocationPermission(fragment: Fragment, requestId: Int) {
        fragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            requestId)

    }

    fun checkAccessFineLocationGranted(context: Context): Boolean {
        return ActivityCompat
            .checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
    }

    fun isLocationEnabled(context: Context): Boolean {
        val locationManager: LocationManager =
            context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    fun showGPSNotEnabledDialog(context: Context) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setCancelable(false)
        alertDialog.setTitle("Enable Location")
        alertDialog.setMessage("Your locations setting is not enabled. Please enabled it in settings menu.")
        alertDialog.setPositiveButton("Location Settings") { dialog, which ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            context.startActivity(intent)
        }
        val alert = alertDialog.create()
        alert.show()
    }
}