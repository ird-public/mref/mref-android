/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.survey

import android.net.Uri
import androidx.annotation.StringRes
import com.ird.compose.mref.R
import com.ird.compose.mref.db.dao.LocationDao
import com.ird.compose.mref.model.*

data class SurveyResult(
    val formName: String,
    val result: String,
    val payload : ChildPayload,
    @StringRes val description: Int
)

data class Survey(
    @StringRes val title: Int,
    val questions: List<Question>
)

data class Question(
    val id: Int,
    @StringRes val questionText: Int,
    val answer: PossibleAnswer,
    @StringRes val description: Int? = null,
    val permissionsRequired: List<String> = emptyList(),
    @StringRes val permissionsRationaleText: Int? = null,
    var nextQuestion: Int,
    var prevQuestion: Int,
    var key: Keys

)


sealed class SubmissionStatus{
    class NONE:SubmissionStatus()
    class SUCCESS:SubmissionStatus()
    class FAILURE:SubmissionStatus()
}

/**
 * Type of supported actions for a survey
 */
sealed class SurveyActionType {
    class PICK_DATE : SurveyActionType()
    class TAKE_PHOTO : SurveyActionType()
    class SELECT_CONTACT : SurveyActionType()
    class SCAN_QRCODE : SurveyActionType()
    class SEARCH_FILTER : SurveyActionType()
    class AGE_PICKER : SurveyActionType()
    class AGE_PICKER_CHILD : SurveyActionType()
    data class FETCH_DATA(val identifier: String) : SurveyActionType()
    data class REFERRAL_TYPE(val referralType: String) : SurveyActionType()
}

enum class TextFieldType {
    TEXT,
    NUMERIC,
    ZM_ID,
    ADDRESS
}

enum class Validations {
    EMPTY,
    CNIC,
    PHONE,
    IDENTIFIER,
    EPINUMBER,
    NONE
}

sealed class SurveyActionResult {
    data class Date(val date: String) : SurveyActionResult()
    data class Photo(val uri: Uri) : SurveyActionResult()
    data class Contact(val contact: String) : SurveyActionResult()
    data class QRCode(val id: String, val message: String) : SurveyActionResult()
    data class Text(val text: String) : SurveyActionResult()
}

sealed class PossibleAnswer {
    data class SingleChoice(val optionsStringRes: List<Pair<Int, Int>>) : PossibleAnswer()
    data class BioData(val biodata: ChildPayload,val user: User) : PossibleAnswer()
    class Referral : PossibleAnswer()
    data class SelectLocation(val locations: List<Location>) : PossibleAnswer()
    data class TextChoice(
        val inputType: TextFieldType,
        val validations: Validations = Validations.NONE,
        val isOptional: Boolean = false,
        val placeholder: Int = R.string.empty_hint,
    ) : PossibleAnswer()

    data class SingleChoiceIcon(val optionsStringIconRes: List<Pair<Int, LogicRunner>>) :
        PossibleAnswer()

    data class MultipleChoiceReferral(val referalReason: List<ReferalReason>) : PossibleAnswer()
    data class MultipleChoice(val optionsStringRes: List<Int>) : PossibleAnswer()
    data class MultipleChoiceIcon(val optionsStringIconRes: List<Pair<Int, Int>>) : PossibleAnswer()
    data class Action(
        @StringRes val label: Int,
        val actionType: SurveyActionType
    ) : PossibleAnswer()

    data class Slider(
        val range: ClosedFloatingPointRange<Float>,
        val steps: Int,
        @StringRes val startText: Int,
        @StringRes val endText: Int,
        @StringRes val neutralText: Int,
        val defaultValue: Float = 5.5f
    ) : PossibleAnswer()
}

sealed class Answer<T : PossibleAnswer> {
    object PermissionsDenied : Answer<Nothing>()
    data class SingleChoice(@StringRes val answer: Int) : Answer<PossibleAnswer.SingleChoice>()
    data class MultipleChoice(val answersStringRes: Set<Int>) :
        Answer<PossibleAnswer.MultipleChoice>()

    data class MultipleChoiceReferral(val reasons: Set<ReferalReason>) :
        Answer<PossibleAnswer.MultipleChoiceReferral>()

    data class SelectLoction(val location: Location, val searchText: String) :
        Answer<PossibleAnswer.SelectLocation>()

    data class Biodata(val basicInfo: ChildPayload,val user: User) : Answer<PossibleAnswer.BioData>()
    data class Referral(val reasons: List<Reasons>) : Answer<PossibleAnswer.Referral>()
    data class Action(val result: SurveyActionResult) : Answer<PossibleAnswer.Action>()
    data class Slider(val answerValue: Float) : Answer<PossibleAnswer.Slider>()
    data class Text(val answerValue: String) : Answer<PossibleAnswer.TextChoice>()
}

/**
 * Add or remove an answer from the list of selected answers depending on whether the answer was
 * selected or deselected.
 */
fun Answer.MultipleChoice.withAnswerSelected(
    @StringRes answer: Int,
    selected: Boolean
): Answer.MultipleChoice {
    val newStringRes = answersStringRes.toMutableSet()
    if (!selected) {
        newStringRes.remove(answer)
    } else {
        newStringRes.add(answer)
    }
    return Answer.MultipleChoice(newStringRes)
}

fun Answer.MultipleChoiceReferral.withReferralAnswerSelected(
    answer: ReferalReason,
    selected: Boolean
): Answer.MultipleChoiceReferral {
    val answerSet = reasons.toMutableSet()
    if (!selected) {
        answerSet.remove(answer)
    } else {
        answerSet.add(answer)
    }
    return Answer.MultipleChoiceReferral(answerSet)
}
