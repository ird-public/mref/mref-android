package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class Attendance(
    @SerializedName("attendanceId"      ) var attendanceId      : Int?    = null,
    @SerializedName("mappedId"          ) var mappedId          : Int?    = null,
    @SerializedName("checkIn"           ) var checkIn           : String? = null,
    @SerializedName("checkOut"          ) var checkOut          : String? = null,
    @SerializedName("description"       ) var description       : String? = null,
    @SerializedName("checkInLatitude"   ) var checkInLatitude   : String? = null,
    @SerializedName("checkInLongitude"  ) var checkInLongitude  : String? = null,
    @SerializedName("checkOutLatitude"  ) var checkOutLatitude  : String? = null,
    @SerializedName("checkOutLongitude" ) var checkOutLongitude : String? = null,
    @SerializedName("checkInAccuracy"   ) var checkInAccuracy   : String? = null,
    @SerializedName("checkOutAccuracy"  ) var checkOutAccuracy  : String? = null,
    @SerializedName("inStatus"          ) var inStatus          : String? = null,
    @SerializedName("outStatus"         ) var outStatus         : String? = null,
    @SerializedName("location"          ) var location          : String? = null,
    @SerializedName("dateCreated"       ) var dateCreated       : String? = null,
    @SerializedName("attendanceDate"    ) var attendanceDate    : String? = null
)
