package com.ird.compose.mref.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ird.compose.mref.model.*
import java.lang.reflect.Type


class Converter {

    @TypeConverter
    fun convertLocationToJson(list: List<Location>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Location>>() {}.getType()
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun convertFormToJson(form: ChildPayload): String {
        val gson = Gson()
        return gson.toJson(form, ChildPayload::class.java)
    }

    @TypeConverter
    fun convertJSONToForm(json: String): ChildPayload {
        val gson = Gson()
        return gson.fromJson(json, ChildPayload::class.java)
    }


    @TypeConverter
    fun convertJSONToLocation(json: String): List<Location> {
        val gson = Gson()
        val type = object :
            TypeToken<List<Location>>() {}.type
        return gson.fromJson(json, type)
    }

    @TypeConverter
    fun convertReferralToJson(list: List<ReferalReason>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<ReferalReason>>() {}.getType()
        return gson.toJson(list, type)
    }




    @TypeConverter
    fun convertJSONToReferral(json: String): List<ReferalReason> {
        val gson = Gson()
        val type = object :
            TypeToken<List<ReferalReason>>() {}.type
        return gson.fromJson(json, type)
    }

    @TypeConverter
    fun convertReasonsToJson(list: List<Reasons>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Reasons>>() {}.getType()
        return gson.toJson(list, type)
    }


    @TypeConverter
    fun convertJSONToReasons(json: String): List<Reasons> {
        val gson = Gson()
        val type = object :
            TypeToken<List<Reasons>>() {}.type
        return gson.fromJson(json, type)
    }


    @TypeConverter
    fun convertCheckInAttendanceToJson(list: List<AttendanceCheckIn>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<AttendanceCheckIn>>() {}.getType()
        return gson.toJson(list, type)
    }


    @TypeConverter
    fun convertJSONToCheckInAttendance(json: String): List<AttendanceCheckIn> {
        val gson = Gson()
        val type = object :
            TypeToken<List<AttendanceCheckIn>>() {}.type
        return gson.fromJson(json, type)
    }

    @TypeConverter
    fun convertCheckOutAttendanceToJson(list: List<AttendanceCheckOut>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<AttendanceCheckOut>>() {}.getType()
        return gson.toJson(list, type)
    }


    @TypeConverter
    fun convertJSONToCheckOutAttendance(json: String): List<AttendanceCheckOut> {
        val gson = Gson()
        val type = object :
            TypeToken<List<AttendanceCheckOut>>() {}.type
        return gson.fromJson(json, type)
    }




}