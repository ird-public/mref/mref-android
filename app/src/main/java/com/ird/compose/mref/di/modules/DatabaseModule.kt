package com.ird.compose.mref.di.modules

import android.app.Application
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.ird.compose.mref.db.AppDatabase
import com.ird.compose.mref.db.dao.AttendanceDao
import com.ird.compose.mref.db.dao.FormDao
import com.ird.compose.mref.db.dao.LocationDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
object DatabaseModule {


    var MIGRATION_1_2: Migration = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE 'form' ADD COLUMN 'nic' TEXT NOT NULL DEFAULT ''")
            database.execSQL("ALTER TABLE 'form' ADD COLUMN 'primaryNumber' TEXT NOT NULL DEFAULT ''")
            database.execSQL("ALTER TABLE 'form' ADD COLUMN 'secondaryNumber' TEXT NOT NULL DEFAULT ''")
        }
    }

    var MIGRATION_2_3: Migration = object : Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE 'offline_form' ('id' INT PRIMARY KEY AUTOINCREMENT, 'formType' TEXT NOT NULL DEFAULT '', 'payload' TEXT NOT NULL DEFAULT '')")
        }
    }

    var MIGRATION_3_4: Migration = object : Migration(3, 4) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE 'form' ADD COLUMN 'centerName' TEXT NOT NULL DEFAULT ''")
        }
    }

    var MIGRATION_4_5: Migration = object : Migration(4, 5) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE 'attendance_check_in' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'checkIn' TEXT NOT NULL, 'checkInLatitude' TEXT NOT NULL, 'checkInLongitude' TEXT NOT NULL, 'attendanceDate' TEXT NOT NULL, 'mappedId' INTEGER, 'status' INTEGER NOT NULL, 'IsUploadedSuccess' INTEGER NOT NULL)")
        }

    }

    var MIGRATION_5_6: Migration = object : Migration(5, 6) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE 'attendance_check_out' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'checkOut' TEXT NOT NULL, 'checkOutLatitude' TEXT NOT NULL, 'checkOutLongitude' TEXT NOT NULL, 'attendanceDate' TEXT NOT NULL, 'mappedId' INTEGER, 'status' INTEGER NOT NULL, 'IsUploadedSuccess' INTEGER NOT NULL)")
        }

    }

    var MIGRATION_6_7: Migration = object : Migration(6, 7) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE 'location' ADD COLUMN 'hierarchy' TEXT  DEFAULT ''")
        }

    }

    @Provides
    fun provideDatabaseModule(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "mref-db")
            .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5, MIGRATION_5_6,MIGRATION_6_7)
            .allowMainThreadQueries().build()
    }

    @Provides
    fun provideLocationDao(database: AppDatabase): LocationDao = database.LocationDao()

    @Provides
    fun provideFormDao(database: AppDatabase): FormDao = database.FormDao()

    @Provides
    fun provideAttendanceDao(database: AppDatabase): AttendanceDao = database.AttendanceDao()
}