/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.survey

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.NumberPicker
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.google.android.material.datepicker.MaterialDatePicker
import com.ird.compose.mref.App
import com.ird.compose.mref.R
import com.ird.compose.mref.db.dao.FormDao
import com.ird.compose.mref.db.dao.LocationDao
import com.ird.compose.mref.model.DevicePreference
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.theme.MRefTheme
import com.ird.compose.mref.util.DateTimeUtils
import com.journeyapps.barcodescanner.ScanContract
import com.journeyapps.barcodescanner.ScanIntentResult
import com.journeyapps.barcodescanner.ScanOptions
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import androidx.activity.OnBackPressedCallback
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.afollestad.materialdialogs.MaterialDialog
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.material.button.MaterialButton
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.ird.compose.mref.locationProvider.ILocationListener
import com.ird.compose.mref.locationProvider.LocationProvider
import com.ird.compose.mref.model.OfflineForm
import com.ird.compose.mref.util.PermissionUtils
import com.ird.compose.mref.util.PermissionUtils.askAccessFineLocationPermission
import com.ird.compose.mref.util.PermissionUtils.checkAccessFineLocationGranted
import com.ird.compose.mref.util.PermissionUtils.isLocationEnabled
import com.ird.compose.mref.util.PermissionUtils.showGPSNotEnabledDialog

class SurveyFragment : Fragment() {

    private val LOCATION_PERMISSION_REQUEST_CODE = 22
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    @Inject
    lateinit var locationDao: LocationDao

    @Inject
    lateinit var mainRepository: MainRepository

    @Inject
    lateinit var preference: DevicePreference

    @Inject
    lateinit var formDao: FormDao

    @Inject
    lateinit var locationProvider: LocationProvider

    val args: SurveyFragmentArgs by navArgs()

    var formType: FormType = FormType.REFERRAL_SLIP

    var isFormSubmitted: Boolean = false

    private val viewModel: SurveyViewModel by viewModels {

        formType = when (args.formType) {
            "referral" -> FormType.REFERRAL_SLIP
            "search" -> FormType.SEARCH
            "followup" -> FormType.FOLLOWUP
            else -> FormType.REFERRAL_SLIP
        }

        SurveyViewModelFactory(
            PhotoUriManager(requireContext().applicationContext),
            locationDao,
            formType,
            preference,
            requireContext().applicationContext
        )
    }

    private val barcodeLauncher = registerForActivityResult(
        ScanContract()
    ) { result: ScanIntentResult ->
        if (result.contents == null) {
            //    Toast.makeText(activity, "Cancelled", Toast.LENGTH_LONG).show()
        } else {
            viewModel.onQRScan(result.contents, mainRepository, formType)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!args.identifier.equals("")) {
            viewModel.onQRScan(args.identifier, mainRepository, formType)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (activity?.application as App).appComponent.inject(this)


        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    if (!isFormSubmitted)
                        showBackDialog()
                    else {
                        Navigation.findNavController(requireView()).popBackStack()
                    }
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        viewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        }

        return ComposeView(requireContext()).apply {
            // In order for savedState to work, the same ID needs to be used for all instances.
            id = R.id.sign_in_fragment

            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            viewModel.onSubmissionComplete.observe(viewLifecycleOwner, {


                formDao.insert(it)
                val forms = formDao.getForms()
                if (forms.size > 5) {
                    formDao.delete(forms.first())
                }

                preference.updateCounter(it.dateRefered)

                //  Log.d("DB", "saved successfully")
            })


            setContent {
                MRefTheme {
                    var resultStatus by remember { mutableStateOf("") }
                    ProvideWindowInsets(
                        //  consumeWindowInsets = false,
                        windowInsetsAnimationsEnabled = true
                    ){
                    viewModel.uiState.observeAsState().value?.let { surveyState ->
                        when (surveyState) {
                            is SurveyState.Questions -> SurveyQuestionsScreen(
                                formType = formType,
                                questions = surveyState,
                                shouldAskPermissions = viewModel.askForPermissions,
                                onAction = { id, action -> handleSurveyAction(id, action) },
                                onChange = { id, text -> handleChange(id, text) },
                                onDoNotAskForPermissions = { viewModel.doNotAskForPermissions() },
                                onDonePressed = {
                                    if (formType.name.equals("search", ignoreCase = true))
                                        viewModel.showFilters(surveyState)
                                    else
                                        viewModel.createPayload(
                                            surveyState,
                                            preference,
                                            latitude,
                                            longitude
                                        )
                                },
                                onBackPressed = {
                                    showBackDialog()
                                },
                                openSettings = {
                                    activity?.startActivity(
                                        Intent(
                                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                            Uri.fromParts("package", context.packageName, null)
                                        )
                                    )
                                }
                            )
                            is SurveyState.Result -> SurveyResultScreen(
                                result = surveyState,
                                status = resultStatus,
                                onPreviousPressed = {
                                    viewModel.goBacktoForms()
                                },
                                onUploadPressed = {
                                    viewModel.uploadForm(it, mainRepository, onStatusChanged = {
                                        resultStatus = it
                                        isFormSubmitted = it == "success"

                                    })
                                },
                                onSavedPressed = {
                                    formDao.insert(
                                        OfflineForm(
                                            id = 0,
                                            payload = it.surveyResult.payload,
                                            formType = formType.name
                                        )
                                    )
                                    isFormSubmitted = true
                                    resultStatus = "saved"
                                },
                                onClosedPressed = {
                                    Navigation.findNavController(requireView()).popBackStack()
                                }
                            )
                            is SurveyState.Search -> SearchFiltersScreen(
                                date = surveyState.date,
                                referralType = surveyState.type,
                                onAction = {
                                    handleSurveyAction(1, it)
                                },
                                onPreviousPressed = {
                                    viewModel.goBacktoForms()
                                },
                                onSearch = {
                                    viewModel.getSearchResults(
                                        it,
                                        mainRepository,
                                        surveyState.type,
                                        onStatusChanged = { text, searchResult ->
                                            if (searchResult.isEmpty()) {
                                                Toast.makeText(
                                                    requireContext(),
                                                    "No results found",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            } else {
                                                viewModel.computeSearchResults(searchResult)
                                            }
                                        })

                                }
                            )
                            is SurveyState.SearchResult -> SearchResultScreen(
                                result = surveyState.result,
                                onPreviousPressed = { viewModel.goBacktoForms() },
                                onItemClicked = {
                                    val args: Bundle = Bundle().apply {
                                        putString("formType", "followup")
                                        putString("identifier", it.identifier)
                                    }

                                    findNavController().navigate(R.id.survey_fragment, args)
                                }
                            )
                        }
                    }
                    }
                }
            }
        }
    }

    private fun showBackDialog() {

        val builder = AlertDialog.Builder(requireContext(), R.style.AppTheme_Dialog)
        builder.setTitle("Rukiye")
        builder.setMessage("Form submit nahi hua hy. Kiya aap phir bhi form band kerna chahte hein? ")
        builder.setPositiveButton("Yes") { dialog, which ->
            Navigation.findNavController(requireView()).popBackStack()
        }
        builder.setNegativeButton("No") { dialog, which ->
        }
        builder.show()
    }

    private fun handleChange(questionId: Int, text: String) {
        // onTextChange(questionId, text)
    }

    private fun handleSurveyAction(questionId: Int, actionType: SurveyActionType) {
        when (actionType) {
            is SurveyActionType.PICK_DATE -> showDatePicker(questionId)
            is SurveyActionType.TAKE_PHOTO -> scanQRCode()
            is SurveyActionType.SELECT_CONTACT -> selectContact(questionId)
            is SurveyActionType.SCAN_QRCODE -> scanQRCode()
            is SurveyActionType.FETCH_DATA -> fetchData(actionType.identifier)
            is SurveyActionType.REFERRAL_TYPE -> updateQuestions(actionType.referralType)
            is SurveyActionType.AGE_PICKER -> showAgeDialog(questionId)
            is SurveyActionType.AGE_PICKER_CHILD -> calculateAgeDialog(questionId)
            //    SurveyActionType.TEXT_CHANGE -> onTextChange(questionId)
        }
    }

    private fun updateQuestions(referralType: String) {
        viewModel.updateFilters(referralType)
    }

    private fun fetchData(identifier: String) {
        viewModel.onQRScan(identifier, mainRepository, formType)
    }

    private fun calculateAgeDialog(questionId: Int) {
        val npDays: NumberPicker?
        val npWeeks: NumberPicker?
        val npMonths: NumberPicker?
        val npYears: NumberPicker?

        val yrs: Int = 2;


        var nums: Array<String?>
        val year = IntArray(1)
        val month = IntArray(1)
        val week = IntArray(1)
        val day = IntArray(1)
        val dialog: AlertDialog = AlertDialog.Builder(requireActivity(), R.style.AppTheme_Dialog)
            .setTitle("Calculate Age")
            .setView(R.layout.child_age_calculator)
            .show()
        val view = dialog
        if (view != null) {
            npDays = view.findViewById<View>(R.id.numberPickerDays) as NumberPicker
            npWeeks = view.findViewById<View>(R.id.numberPickerWeeks) as NumberPicker
            npMonths = view.findViewById<View>(R.id.numberPickerMonths) as NumberPicker
            npYears = view.findViewById<View>(R.id.numberPickerYears) as NumberPicker
            // days
            nums = arrayOfNulls(31)
            for (i in nums.indices) nums[i] = Integer.toString(i)
            npDays.minValue = 0
            npDays.maxValue = 30
            npDays.wrapSelectorWheel = true
            npDays.displayedValues = nums
            npDays.value = 0
            // weeks
            nums = arrayOfNulls(5)
            for (i in nums.indices) nums[i] = Integer.toString(i)
            npWeeks.minValue = 0
            npWeeks.maxValue = 4
            npWeeks.wrapSelectorWheel = false
            npWeeks.displayedValues = nums
            npWeeks.value = 0
            // months
            nums = arrayOfNulls(13)
            for (i in nums.indices) nums[i] = Integer.toString(i)
            npMonths.minValue = 0
            npMonths.maxValue = 11
            npMonths.wrapSelectorWheel = false
            npMonths.displayedValues = nums
            npMonths.value = 0
            // years
            nums = arrayOfNulls(6)
            for (i in nums.indices) nums[i] = Integer.toString(i)
            npYears.minValue = 0
            npYears.maxValue = yrs
            npYears.wrapSelectorWheel = false
            npYears.displayedValues = nums
            npYears.value = 0
            val calculateAge = view.findViewById<View>(R.id.btnCalculateAge) as MaterialButton
            calculateAge.setOnClickListener {
                year[0] = npYears.getValue()
                month[0] = npMonths.getValue()
                week[0] = npWeeks.getValue()
                day[0] = npDays.getValue()
                //EpiUtils.validateCalculatorWidget(activity, npYears, npMonths, npWeeks, npDays)

                val cal = GregorianCalendar.getInstance()
                cal.time = Date()
                cal.add(Calendar.YEAR, -year[0])
                cal.add(Calendar.MONTH, -month[0])
                //calculate weeks and days together
                val daysFromWeeks = 7 * week[0]
                cal.add(Calendar.DAY_OF_MONTH, -(day[0] + daysFromWeeks))
                val dateOfBirth = cal.time

                val cal2 = cal
                cal2.add(Calendar.YEAR, yrs)
                val d1 = cal2.time

                val todayCal = Calendar.getInstance()
                todayCal.add(Calendar.MINUTE, -10)
                val todayTime = todayCal.time

                if (!d1.before(todayTime)) {

                    viewModel.onAgePicked(
                        formType,
                        questionId,
                        DateTimeUtils.getDateFormat().format(dateOfBirth)
                    )

                } else showDialog(
                    "Bacha " + yrs + " saal se bara nahi ho sakta.",
                    "Error"
                )
                dialog.dismiss()
            }

            val cancelButton = view.findViewById<View>(R.id.btnCancel) as MaterialButton
            cancelButton.setOnClickListener() {
                dialog.dismiss()
            }
        }

    }

    private fun showDialog(message: String, title: String) {
        val builder = AlertDialog.Builder(requireContext(), R.style.AppTheme_Dialog)
        builder.setTitle(title)
        builder.setCancelable(false)
        builder.setMessage(
            message
        )
        builder.setPositiveButton("Yes") { dialog, which ->

            //Navigation.findNavController(requireView()).popBackStack()
        }
        builder.show()

    }

    private fun showAgeDialog(questionId: Int) {
        val dialog: AlertDialog = AlertDialog.Builder(requireActivity(), R.style.AppTheme_Dialog)
            .setTitle("Calculate Age")
            .setView(R.layout.women_age_calculator)
            .show()
        dialog.setCancelable(false)

        val picker = dialog.findViewById<View>(R.id.numberPicker) as NumberPicker
        picker.maxValue = 55
        picker.minValue = 12
        val ageText = dialog.findViewById<View>(R.id.age_text) as TextView
        val done = dialog.findViewById<View>(R.id.btnCalculateAge) as MaterialButton
        done.setOnClickListener {
            if (picker.value > 0) {
                val calendar = Calendar.getInstance()
                calendar.time = Date()
                calendar[Calendar.YEAR] =
                    Calendar.getInstance()[Calendar.YEAR] - picker.value
                //isEstimatedDOB = true
                try {
                    //Date date = DateTimeUtils.getDateFormat().parse(c);
                    val dateFormat = SimpleDateFormat("dd-MM-yyyy")
                    val date = dateFormat.format(
                        calendar
                            .time
                    )
                    //editTxtBirthDate.setText(date)
                    viewModel.onAgePicked(formType, questionId, date)

                    //val mDate = DateTimeUtils.StringToDate(date,"dd/MM/yyyy")
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                dialog.dismiss()
            }
        }
        val cancelButton = dialog.findViewById<View>(R.id.btnCancel) as MaterialButton
        cancelButton.setOnClickListener() {
            dialog.dismiss()
        }
    }

    private fun showDatePicker(questionId: Int) {
        val date = viewModel.getCurrentDate(questionId = questionId)
        val constraints = viewModel.getConstraints(questionId )
              val picker = MaterialDatePicker.Builder.datePicker()
            .setSelection(date)
            .setTheme(R.style.MaterialCalendarTheme)
            .setCalendarConstraints(constraints)
            .build()

        activity?.let {
            picker.show(it.supportFragmentManager, picker.toString())
            picker.addOnPositiveButtonClickListener {
                viewModel.onDatePicked(formType, questionId, picker.selection)
            }
        }
    }

    private fun onTextChange(questionId: Int, text: String) {
        viewModel.onTextChange(questionId, text)
    }

    private fun scanQRCode() {

        val scanOptions = ScanOptions()
        scanOptions.setDesiredBarcodeFormats(ScanOptions.QR_CODE);
        scanOptions.setCameraId(0);  // Use a specific camera of the device
        scanOptions.setBeepEnabled(true);
        scanOptions.setOrientationLocked(false)
        scanOptions.setBarcodeImageEnabled(false);

        barcodeLauncher.launch(scanOptions)

        //takePicture.launch(viewModel.getUriToSaveImage())
    }

    @Suppress("UNUSED_PARAMETER")
    private fun selectContact(questionId: Int) {
        // TODO: unsupported for now
    }

    override fun onStart() {
        super.onStart()
        checkPermission()
    }

    private fun checkPermission() {
        if (checkAccessFineLocationGranted(requireContext())) {
            if (isLocationEnabled(requireActivity())) {
                locationProvider.getUserLocation(
                    object : ILocationListener {
                        override fun onLocationReceived(
                            locationResult: LocationResult?,
                            callback: LocationCallback?
                        ) {
                            locationProvider.stopUpdates(callback)
                            latitude = locationResult?.lastLocation?.latitude!!
                            longitude = locationResult.lastLocation.longitude
                        }
                    })
            } else {
                showGPSNotEnabledDialog(requireContext())
            }
        } else {
            askAccessFineLocationPermission(this, LOCATION_PERMISSION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationProvider.getUserLocation(
                    object : ILocationListener {
                        override fun onLocationReceived(
                            locationResult: LocationResult?,
                            callback: LocationCallback?
                        ) {
                            locationProvider.stopUpdates(callback)
                            latitude = locationResult?.lastLocation?.latitude!!
                            longitude = locationResult.lastLocation.longitude
                        }
                    })
                checkPermission()
            } else {
                showPermissionDialog()
            }
        }
    }

    private fun showPermissionDialog() {
        val builder = AlertDialog.Builder(requireContext(), R.style.AppTheme_Dialog)
        builder.setTitle("Location Access")
        builder.setCancelable(false)
        builder.setMessage(
            "This feature require your location to mark your attendance.\n\n" +
                    "Without giving the permission you are not allowed to fill any form. \n\n" +
                    "Please Allow this location permission" +
                    ""
        )
        builder.setPositiveButton("Yes") { dialog, which ->
            PermissionUtils.askAccessFineLocationPermission(
                this,
                LOCATION_PERMISSION_REQUEST_CODE
            )
            //Navigation.findNavController(requireView()).popBackStack()
        }
        builder.setNegativeButton("No") { dialog, which ->
            findNavController().navigate(
                R.id.welcome_fragment,
                null,
                NavOptions.Builder()
                    .setPopUpTo(R.id.home_fragment, true)
                    .build()
            )

            findNavController().navigate(R.id.welcome_fragment)

        }
        builder.show()
    }
}
