package com.ird.compose.mref.model


import com.google.gson.annotations.SerializedName


data class StatisticsResponse (

    @SerializedName("totalTodaySubmittedForms" ) var totalTodaySubmittedForms : Int                      =  0,
    @SerializedName("totalSubmittedForms"      ) var totalSubmittedForms      : Int                      = 0,
    @SerializedName("userRecentForm"           ) var userRecentForm           : ArrayList<UserRecentForm> = arrayListOf()

)