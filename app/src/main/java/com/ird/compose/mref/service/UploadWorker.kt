package com.ird.compose.mref.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import com.ird.compose.mref.App
import com.ird.compose.mref.R
import com.ird.compose.mref.db.dao.FormDao
import com.ird.compose.mref.db.dao.LocationDao
import com.ird.compose.mref.model.DevicePreference
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.survey.FormType
import retrofit2.Response
import javax.inject.Inject

class UploadWorker(context: Context, parameters: WorkerParameters) :
    CoroutineWorker(context, parameters) {

    private val CHANNEL_ID: String = "51231"

    @Inject
    lateinit var mainRespository: MainRepository

    @Inject
    lateinit var formDao: FormDao

    @Inject
    lateinit var preference: DevicePreference


    private val notificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as
                NotificationManager

    override suspend fun doWork(): Result {
        (applicationContext as App).appComponent.inject(this)

        val progress = "Starting Upload"
        setForeground(createForegroundInfo(progress))
        upload()
        return Result.success()
    }

    private suspend fun upload() {
        val offlineForms = formDao.getOfflineForms().filter {  form -> form.payload.userId.toInt() == preference.getUser().mappedId}
        offlineForms.forEach { form ->
            val response: Response<Any> = if (form.formType.equals(FormType.REFERRAL_SLIP.name)) {
                mainRespository.submitReferralSlipSync(form.payload).execute()
            } else { // followup
                mainRespository.submitFollowUpSync(form.payload).execute()
            }

            if (response.isSuccessful) {
                formDao.insert(form.payload)
                val historyForms = formDao.getForms()
                if (historyForms.size > 5) {
                    formDao.delete(historyForms.first())
                }

                formDao.delete(form)
                preference.updateCounter(form.payload.dateRefered)

            }
        }
    }

    private fun createForegroundInfo(progress: String): ForegroundInfo {
        val id = applicationContext.getString(R.string.notification_channel_id)
        val title = applicationContext.getString(R.string.notification_title)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }


        val notification = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setContentTitle(title)
            .setTicker(title)
            .setContentText(progress)
            .setSmallIcon(R.drawable.mref_logo)
            .setOngoing(true)
            .build()

        return ForegroundInfo(47123, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val name = applicationContext.getString(R.string.channel_name)
        val descriptionText = applicationContext.getString(R.string.channel_description)
        val importance = NotificationManager.IMPORTANCE_HIGH
        val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
        mChannel.description = descriptionText
        notificationManager.createNotificationChannel(mChannel)
    }

    companion object {
        const val KEY_FORMS = "KEY_FORMS"

    }
}