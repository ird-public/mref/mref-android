package com.ird.compose.mref.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.ird.compose.mref.db.Converter

@Entity(tableName = "attendance_check_in")
@TypeConverters(Converter::class)
data class AttendanceCheckIn(

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("checkIn") var checkIn: String = "",
    @SerializedName("checkInLatitude") var checkInLatitude: String = "",
    @SerializedName("checkInLongitude") var checkInLongitude: String = "",
    @SerializedName("attendanceDate") var attendanceDate: String = "",
    @SerializedName("mappedId") var mappedId: Int? = null,
    @SerializedName("status") var status: Int? = null,
    @SerializedName("IsUploadedSuccess") var IsUploadedSuccess: Int? = null

)


