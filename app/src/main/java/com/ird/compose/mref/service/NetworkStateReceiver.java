package com.ird.compose.mref.service;

import static com.ird.compose.mref.util.DateTimeUtils.isInternetAvailable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.ird.compose.mref.App;

import javax.inject.Inject;

public class NetworkStateReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        ((App) context.getApplicationContext()).appComponent.inject(this);
        if (intent.getExtras() != null) {
            if (isInternetAvailable(context)) {
                Constraints.Builder builder =
                        new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED);

                OneTimeWorkRequest syncWorkRequest =
                        new OneTimeWorkRequest.Builder(AttendanceService.class)
                                .addTag("Attendance")
                                .setConstraints(builder.build())
                                .build();

                WorkManager.getInstance(context).enqueue(syncWorkRequest);

            }
        } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
            Log.i("Network State Receiver", "There is no network connectivity");
        }
    }
}
