/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.survey

import com.ird.compose.mref.R
import com.ird.compose.mref.db.dao.LocationDao
import com.ird.compose.mref.model.ChildPayload
import com.ird.compose.mref.model.DevicePreference
import com.ird.compose.mref.model.Keys.*
import com.ird.compose.mref.model.Location
import com.ird.compose.mref.survey.PossibleAnswer.Action
import com.ird.compose.mref.survey.SurveyActionType.*


data class LogicRunner(val optionText: Int, val nextQuestionId: Int)
// HIGHEST QUESTION = 25

private fun searchQuestions(locationDao: LocationDao) = mutableListOf(


    Question(
        id = 1,
        questionText = R.string.referral_entity,
        answer = PossibleAnswer.SingleChoiceIcon(
            optionsStringIconRes = listOf(
                Pair(R.drawable.child_male, LogicRunner(R.string.child, -1)),
                Pair(R.drawable.woman, LogicRunner(R.string.woman, -1)),
            )
        ),
        description = R.string.select_one,
        nextQuestion = -1, // options will decide next question
        prevQuestion = 0,
        key = REFERRAL_ENTITY_SEARCH
    ),


/*    Question(
        id = 2,
        questionText = R.string.search,
        answer = Action(label = R.string.empty_hint, actionType = SEARCH_FILTER()),
        nextQuestion = 2,
        prevQuestion = 1,
        key = SEARCH_FILTERS
    )*/
)


private fun followupQuestions(locationDao: LocationDao, preference: DevicePreference) =
    mutableListOf(
        Question(
            id = 1,
            questionText = R.string.qr_code,
            answer = Action(label = R.string.add_photo, actionType = SCAN_QRCODE()),
            nextQuestion = 2,
            prevQuestion = 0,
            key = FOLLOWUP_ID
        ),

        Question(
            id = 2,
            questionText = R.string.biodata,
            answer = PossibleAnswer.BioData(ChildPayload(), preference.getUser()),
            description = R.string.empty_hint,
            nextQuestion = 3,
            prevQuestion = 1,
            key = BIODATA
        ),

        Question(
            id = 3,
            questionText = R.string.referral_reasons,
            answer = PossibleAnswer.Referral(),
            description = R.string.empty_hint,
            nextQuestion = -1,
            prevQuestion = 2,
            key = REFERRALS
        ),
    )


// Static data of questions
private fun mrefQuestions(locationDao: LocationDao) = mutableListOf(


    Question(
        id = 1,
        questionText = R.string.qr_code,
        answer = Action(label = R.string.add_photo, actionType = SCAN_QRCODE()),
        nextQuestion = 3,
        prevQuestion = 1,
        key = MREF_QRCODE
    ),


    Question(
        id = 3,
        questionText = R.string.uc_name,
        answer = PossibleAnswer.SelectLocation(
            locations = getMappedLocations(locationDao.getAllUC(), locationDao)
        ),
        description = R.string.select_one,
        nextQuestion = 4,
        prevQuestion = 1,
        key = UC
    ),


    Question(
        id = 4,
        questionText = R.string.referral_entity,
        answer = PossibleAnswer.SingleChoiceIcon(
            optionsStringIconRes = listOf(
                Pair(R.drawable.baby, LogicRunner(R.string.child, 5)),
                Pair(R.drawable.woman, LogicRunner(R.string.woman, 15)),
            )
        ),
        description = R.string.select_one,
        nextQuestion = -1, // options will decide next question
        prevQuestion = 3,
        key = REFERRAL_ENTITY
    ),


    /*************Women Questions******************/
    Question(
        id = 15,
        questionText = R.string.question_women_name,
        answer = PossibleAnswer.TextChoice(TextFieldType.TEXT, placeholder = R.string.enter_name),
        description = R.string.empty_hint,
        nextQuestion = 16,
        prevQuestion = 4,
        key = WOMAN_NAME
    ),

    Question(
        id = 16,
        questionText = R.string.question_women_husband_name,
        answer = PossibleAnswer.TextChoice(TextFieldType.TEXT, placeholder = R.string.enter_name),
        description = R.string.empty_hint,
        nextQuestion = 17,
        prevQuestion = 15,
        key = HUSBAND_NAME
    ),

    Question(
        id = 17,
        questionText = R.string.women_age,
        answer = Action(label = R.string.pick_date, actionType = AGE_PICKER()),
        description = R.string.select_age,
        nextQuestion = 25,
        prevQuestion = 16,
        key = WOMAN_AGE
    ),


    Question(
        id = 25,
        questionText = R.string.primary,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.PHONE,
            true,
            placeholder = R.string.phone_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 26,
        prevQuestion = 17,
        key = PHONE_PRIMARY_WOMAN
    ),

    Question(
        id = 26,
        questionText = R.string.secondary,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.PHONE,
            true,
            placeholder = R.string.phone_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 27,
        prevQuestion = 25,
        key = PHONE_SECONDARY_WOMAN
    ),


    Question(
        id = 27,
        questionText = R.string.cnic,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.CNIC,
            true,
            placeholder = R.string.cnic_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 18,
        prevQuestion = 26,
        key = CNIC_WOMAN
    ),


    Question(
        id = 18,
        questionText = R.string.question_area,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.ADDRESS,
            placeholder = R.string.address,
            isOptional = false
        ),
        description = R.string.empty_hint,
        nextQuestion = 19,
        prevQuestion = 27,
        key = ADDRESS
    ),

    Question(
        id = 19,
        questionText = R.string.epi_card,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.EPINUMBER,
            isOptional = true,
            placeholder = R.string.epino_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 20,
        prevQuestion = 18,
        key = EPI_NO_WOMAN

    ),

    Question(
        id = 20,
        questionText = R.string.is_refer,
        answer = PossibleAnswer.SingleChoiceIcon(


            optionsStringIconRes = listOf(
                Pair(R.drawable.ic_checked, LogicRunner(R.string.yes, 21)),
                Pair(R.drawable.ic_cancel, LogicRunner(R.string.no, -1)),
            )
        ),
        description = R.string.select_one,
        nextQuestion = -1, // check options .. They will decide next Question
        prevQuestion = 19,
        key = IS_REFERRED
    ),

    Question(
        id = 21,
        questionText = R.string.referral_center,
        answer = PossibleAnswer.SelectLocation(
            locations = getMappedLocations(locationDao.getAllVC(), locationDao)
        ),
        description = R.string.select_one,
        nextQuestion = 22,
        prevQuestion = 20,
        key = CENTER
    ),


    Question(
        id = 22,
        questionText = R.string.isPregnant,
        answer = PossibleAnswer.SingleChoiceIcon(
            optionsStringIconRes = listOf(
                Pair(R.drawable.ic_checked, LogicRunner(R.string.yes, 23)),
                Pair(R.drawable.ic_cancel, LogicRunner(R.string.no, 24)),
            )
        ),
        description = R.string.select_one,
        nextQuestion = -1, // options will decide next Question
        prevQuestion = 21,
        key = IS_PREGNANT
    ),

    Question(
        id = 23,
        questionText = R.string.refer_for_woman,
        answer = PossibleAnswer.MultipleChoiceReferral(
            locationDao.getPregnantWomanReasons()
        ),
        description = R.string.select_all,
        nextQuestion = -1,
        prevQuestion = 22,
        key = REFER_FOR_PREGNANT
    ),

    Question(
        id = 24,
        questionText = R.string.refer_for_woman,
        answer = PossibleAnswer.MultipleChoiceReferral(
            locationDao.getWomanReasons()
        ),
        description = R.string.select_all,
        nextQuestion = -1,
        prevQuestion = 22,
        key = REFER_FOR_WOMAN
    ),

    /********   CHILD QUESTIONS     ******/

    Question(
        id = 5,
        questionText = R.string.question_child_name,
        answer = PossibleAnswer.TextChoice(TextFieldType.TEXT, placeholder = R.string.enter_name),
        description = R.string.empty_hint,
        nextQuestion = 6,
        prevQuestion = 4,
        key = CHILD_NAME

    ),

    Question(
        id = 6,
        questionText = R.string.question_father_name,
        answer = PossibleAnswer.TextChoice(TextFieldType.TEXT, placeholder = R.string.enter_name),
        description = R.string.empty_hint,
        nextQuestion = 7,
        prevQuestion = 5,
        key = FATHER_NAME
    ),


    Question(
        id = 7,
        questionText = R.string.epi_card,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.EPINUMBER,
            isOptional = true,
            placeholder = R.string.epino_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 8,
        prevQuestion = 6,
        key = EPI_NO

    ),

    Question(
        id = 8,
        questionText = R.string.gender,
        answer = PossibleAnswer.SingleChoiceIcon(
            optionsStringIconRes = listOf(
                Pair(R.drawable.child_male, LogicRunner(R.string.male, 9)),
                Pair(R.drawable.child_female, LogicRunner(R.string.female, 9)),
            )
        ),
        description = R.string.select_one,
        nextQuestion = -1, // options will decide next question
        prevQuestion = 7,

        key = CHILD_GENDER
    ),


    Question(
        id = 9,
        questionText = R.string.child_age,
        answer = Action(label = R.string.pick_date, actionType = AGE_PICKER_CHILD()),
        description = R.string.select_age,
        nextQuestion = 28,
        prevQuestion = 8,
        key = CHILD_AGE
    ),

    Question(
        id = 28,
        questionText = R.string.primary,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.PHONE,
            true,
            placeholder = R.string.phone_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 29,
        prevQuestion = 9,
        key = PHONE_PRIMARY_CHILD
    ),

    Question(
        id = 29,
        questionText = R.string.secondary,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.PHONE,
            true,
            placeholder = R.string.phone_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 30,
        prevQuestion = 28,
        key = PHONE_SECONDARY_CHILD
    ),


    Question(
        id = 30,
        questionText = R.string.cnic,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.NUMERIC,
            Validations.CNIC,
            true,
            placeholder = R.string.cnic_placeholder
        ),
        description = R.string.optional,
        nextQuestion = 10,
        prevQuestion = 29,
        key = CNIC_CHILD
    ),


    Question(
        id = 10,
        questionText = R.string.question_area,
        answer = PossibleAnswer.TextChoice(
            TextFieldType.ADDRESS,
            placeholder = R.string.address,
            isOptional = false
        ),
        description = R.string.empty_hint,
        nextQuestion = 12,
        prevQuestion = 30,
        key = AREA_NAME

    ),

/*    Question(
        id = 11,
        questionText = R.string.house_no,
        answer = PossibleAnswer.TextChoice(TextFieldType.ADDRESS),
        description = R.string.empty_hint,
        nextQuestion = 12,
        prevQuestion = 10,
        key = HOUSE_NO

    ),*/


    Question(
        id = 12,
        questionText = R.string.is_refer,
        answer = PossibleAnswer.SingleChoiceIcon(
            optionsStringIconRes = listOf(
                Pair(R.drawable.ic_checked, LogicRunner(R.string.yes, 13)),
                Pair(R.drawable.ic_cancel, LogicRunner(R.string.no, -1)),
            )
        ),
        description = R.string.select_one,
        nextQuestion = -1, // check options .. They will decide next Question
        prevQuestion = 10,
        key = IS_REFERRED_CHILD

    ),

    Question(
        id = 13,
        questionText = R.string.referral_center,
        answer = PossibleAnswer.SelectLocation(
            locations = getMappedLocations(locationDao.getAllVC(), locationDao)
        ),
        description = R.string.select_one,
        nextQuestion = 14,
        prevQuestion = 12,
        key = REFERRAL_CENTER_CHILD
    ),

    Question(
        id = 14,
        questionText = R.string.refer_for,
        answer = PossibleAnswer.MultipleChoiceReferral(
            locationDao.getChildReasons()
        ),
        description = R.string.select_all,
        nextQuestion = -1, // check options .. They will decide next Question
        prevQuestion = 13,
        key = REFER_FOR_CHILD
    ),

    ).toList()

fun getMappedLocations(
    filteredLocations: List<Location>,
    locationDao: LocationDao
): List<Location> {

    /*filteredLocations.forEach {
        it.hierarchy = getLocationHeirarchy(it,locationDao)
    }*/
    return filteredLocations
}

fun getLocationHeirarchy(currentLocation: Location, locationDao: LocationDao): String {
    var hierarchy = ""
    var mLocation = currentLocation
    var hierarchyList: MutableList<Location> = mutableListOf()

    var stopLocationType = 9 //if (currentLocation.locationType == 3) 1 else 9

    while (mLocation.locationType != stopLocationType) {
        if (mLocation.parentLocation != null) {
            val location =
                locationDao.getLocation(mLocation.parentLocation!!)//getParentLocation(mLocation, locations)
            if (location != null) {
                mLocation = location
                hierarchyList.add(mLocation)
            } else {
                break
            }
        } else {
            break
        }
    }


    hierarchyList.asReversed().forEach { location ->
        hierarchy += location.name
        hierarchy += if (location.equals(hierarchyList.first())) "" else " > "
    }

    return hierarchy

}

fun getParentLocation(currentLocation: Location, locations: List<Location>): Location? {

    var location = locations.filter { it.locationId == currentLocation.parentLocation }

    return if (location.isNotEmpty()) location.first() else null
}


private fun mrefForm(locationDao: LocationDao) = Survey(
    title = R.string.referral_form,
    questions = mrefQuestions(locationDao)
)


private fun followupForm(locationDao: LocationDao, preference: DevicePreference) = Survey(
    title = R.string.followup,
    questions = followupQuestions(locationDao, preference)
)

private fun searchForm(locationDao: LocationDao) = Survey(
    title = R.string.search,
    questions = searchQuestions(locationDao)
)


object SurveyRepository {

    suspend fun getSurvey(
        locationDao: LocationDao,
        formType: FormType,
        preference: DevicePreference
    ): Survey {
        val form: Survey = when (formType) {
            FormType.REFERRAL_SLIP ->
                mrefForm(locationDao)
            FormType.FOLLOWUP ->
                followupForm(locationDao, preference)
            FormType.SEARCH ->
                searchForm(locationDao)
            else ->
                mrefForm(locationDao)
        }
        return form
    }


    @Suppress("UNUSED_PARAMETER")
    fun getSurveyResult(
        formType: FormType,
        patientDetails: ChildPayload,
        locationDao: LocationDao
    ): SurveyResult {
        var results = ""

        if (patientDetails.referalEntity.equals("child")) {
            results +=
                "Bache ka naam : " + patientDetails.name + "\n" +
                        "Walid ka naam : " + patientDetails.fatherName + "\n" +
                        "Tareekh-e-paidaish : " + patientDetails.dob + "\n" +
                        "Jins : " + patientDetails.gender.lowercase() + "\n" +
                        "Refer krne ki tareekh : " + patientDetails.dateRefered + "\n"
            "Visit ka natija : " + patientDetails.visitOutcome.lowercase() + "\n"
        } else {
            results += "Maa ka naam : " + patientDetails.name + "\n" +
                    "Shohar ka naam : " + patientDetails.husbandName + "\n" +
                    "Tareekh-e-paidaish : " + patientDetails.dob + "\n" +
                    "Refer krne ki tareekh : " + patientDetails.dateRefered + "\n"
            "Visit ka natija : " + patientDetails.visitOutcome.lowercase() + "\n"
        }


        if (!patientDetails.reasons.isEmpty()) {
            val location = locationDao.getLocation(patientDetails.reasons.first().referedTo.toInt())
            if (!patientDetails.reasons.first().referedTo.equals("") && patientDetails.reasons.first().referedTo.toInt() != -1) {
                results += "Center : " + location.name + "\n"
                // + "Tareekh : " + patientDetails.reasons.first().dateRefered + "\n"
                patientDetails.centerName = location.name!!

            }

            results +=
                "\nWajohaat \n"
            patientDetails.reasons.forEach {

                val mReasons = locationDao.getReasonByName(it.displayText)
                results += "Wajah : " +  if(mReasons!= null) mReasons.displayText + "\n"  else it.displayText + "\n"
                 results += "Status : " + it.status + "\n\n"


            }
        }

        var title = ""
        if (formType.name.equals("REFERRAL_SLIP")) {
            title = "Referral Slip Summary"
        }
        if (formType.name.equals("FOLLOWUP")) {
            title = "Referral Follow Up Summary"
        }

        return SurveyResult(
            formName = title,
            result = results,
            payload = patientDetails,
            description = R.string.survey_result_description
        )
    }
}
