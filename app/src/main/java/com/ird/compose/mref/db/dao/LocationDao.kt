package com.ird.compose.mref.db.dao

import androidx.room.Dao
import androidx.room.FtsOptions.Order
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ird.compose.mref.model.Location
import com.ird.compose.mref.model.ReferalReason






@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(Location: List<Location>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(location: Location)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllReferralReasons(referalReason: List<ReferalReason>)

    @Query("SELECT * FROM location")
    fun getAll(): List<Location>

    @Query("SELECT * FROM referalreason where entityType = 'WOMEN' ")
    fun getWomanReasons(): List<ReferalReason>

    @Query("SELECT * FROM referalreason where entityType = 'CHILD' ")
    fun getChildReasons(): List<ReferalReason>

    @Query("SELECT * FROM referalreason where name = :reason ")
    fun getReasonByName(reason: String): ReferalReason

    @Query("SELECT * FROM referalreason where entityType = 'WOMEN_PREGNANT' ")
    fun getPregnantWomanReasons(): List<ReferalReason>

    @Query("SELECT * FROM location where locationType = 3") //3
    fun getAllUC(): List<Location>

    @Query("SELECT * FROM location where locationType = 12") //12
    fun getAllVC(): List<Location>

    @Query("SELECT * FROM location WHERE locationId = :locationId")
    fun getLocation(locationId: Int): Location

    @Query("DELETE from location")
    fun deleteAllLocations()

}