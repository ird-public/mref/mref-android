package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName


data class Reasons(

    @SerializedName("name") var name: String = "",
    @SerializedName("displayText") var displayText: String = "",
    @SerializedName("dateClosed") var dateClosed: String = "",
    @SerializedName("referedTo") var referedTo: String = "",
    @SerializedName("status") var status: String = "",
    @SerializedName("dateRefered") var dateRefered: String = "",
    @SerializedName("referedBy") var referedBy: Int = -1,
    @SerializedName("closedBy") var closedBy: String = "",


    )