package com.ird.compose.mref.home

import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.History
import androidx.compose.material.icons.filled.Logout
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.*
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.insets.statusBarsPadding
import com.ird.compose.mref.R
import com.ird.compose.mref.model.ChildPayload
import com.ird.compose.mref.model.FormCount
import com.ird.compose.mref.model.Permission
import com.ird.compose.mref.model.StatisticsResponse

import com.ird.compose.mref.theme.MRefTheme
import com.ird.compose.mref.util.DateTimeUtils

sealed class HomeScreenEvent {
    object StartSurvey : HomeScreenEvent()
    object StartFollowup : HomeScreenEvent()
    object StartSavedForm : HomeScreenEvent()
    object StartSearch : HomeScreenEvent()
}


@ExperimentalMaterialApi
@Composable
fun Home(

    stats: StatisticsResponse,
    permissions: List<Permission>,
    onLogout: () -> Unit,
    checkedIn: () -> Unit,
    attendanceUIStatus: AttendanceUIStatus,
    onEvent: (HomeScreenEvent) -> Unit
) {
    Scaffold(
        modifier = Modifier
            .statusBarsPadding()
            .navigationBarsPadding(),
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Home",
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .wrapContentSize(Alignment.Center)
                    )
                },
                navigationIcon = {
                    Spacer(modifier = Modifier.width(20.dp))
                    Image(
                        modifier = Modifier
                            .height(30.dp)
                            .width(30.dp),
                        painter = painterResource(id = R.drawable.mref_logo),
                        contentDescription = "form",
                    )
                },

                actions = {
                    IconButton(onClick = { onLogout() }) {
                        Icon(
                            imageVector = Icons.Filled.Logout,
                            contentDescription = stringResource(id = R.string.back)
                        )
                    }
                },
                backgroundColor = MaterialTheme.colors.surface,
                elevation = 0.dp
            )
        },
        content = {
            Column(
                modifier = Modifier.fillMaxHeight()

            ) {
                HomeBody(

                    stats,
                    permissions,
                    onEvent,
                    attendanceUIStatus,
                    checkedIn
                )
            }
        }

    )


}

enum class PERMISSION {
    ADD_REFERAL_SLIP,
    ADD_FOLLOWUP_SLIP
}


@ExperimentalMaterialApi
@Composable
fun HomeBody(

    stats: StatisticsResponse,
    permissions: List<Permission>,
    onEvent: (HomeScreenEvent) -> Unit,
    attendanceUIStatus: AttendanceUIStatus,
    checkedIn: () -> Unit
) {
    val scrollState = rememberScrollState()
    var btnText by remember { mutableStateOf("") }
    var backgroundColor by remember { mutableStateOf( if(attendanceUIStatus != AttendanceUIStatus.CHECKED_IN ) R.color.red_light_image else  R.color.gradient_first ) }
    var backgroundColor2 by remember { mutableStateOf(if(attendanceUIStatus != AttendanceUIStatus.CHECKED_IN ) R.color.red_light_image_2 else  R.color.gradient_second) }
    var buttonColor by remember { mutableStateOf(R.color.red_image) }
    var buttonTextColor by remember { mutableStateOf(R.color.white) }

    Column(
        modifier = Modifier
            .padding(20.dp)
            .verticalScroll(scrollState)
    ) {

        val referralPermission =
            permissions.filter { permission -> permission.permissionName == PERMISSION.ADD_REFERAL_SLIP.name }
        val followupPermission =
            permissions.filter { permission -> permission.permissionName == PERMISSION.ADD_FOLLOWUP_SLIP.name }




        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            shape = RoundedCornerShape(7.dp),
            //backgroundColor = colorResource(backgroundColor),  //MaterialTheme.colors.onPrimary  //,
            contentColor = colorResource(id = R.color.white),
            content = {
                Row(
                    modifier = Modifier
                        .background(
                            Brush.linearGradient(
                                listOf(
                                    colorResource(backgroundColor),
                                    colorResource(backgroundColor2),

                                    )
                            )
                        ),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.aligned(Alignment.CenterHorizontally)
                ) {
                    if (attendanceUIStatus == AttendanceUIStatus.CHECKED_OUT) {
                        backgroundColor = R.color.red_light_image
                        backgroundColor2 = R.color.red_light_image_2
                        buttonTextColor = R.color.white


                        Text(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 10.dp, vertical = 10.dp),
                            text = "Attendance Completed For " + DateTimeUtils.getCurrentDate(),
                            textAlign = TextAlign.Center,
                            style = LocalTextStyle
                                .current.copy(color = colorResource(id = buttonTextColor))
                        )
                    } else {
                        if (attendanceUIStatus == AttendanceUIStatus.CHECKED_IN) {
                            backgroundColor = R.color.gradient_first
                            backgroundColor2 = R.color.gradient_second
                            buttonTextColor = R.color.matt_black
                            btnText = "Check Out"
                            buttonColor = R.color.blue
                        } else if (attendanceUIStatus == AttendanceUIStatus.NO_ATTENDANCE) {
                            btnText = "Check In"
                            buttonTextColor = R.color.white
                            buttonColor = R.color.red_image
                        }
                        Text(
                            modifier = Modifier
                                .padding(top = 10.dp, bottom = 10.dp, end = 10.dp),
                            text = "Mark Your Attendance",
                            textAlign = TextAlign.Center,
                            style = LocalTextStyle
                                .current.copy(color = colorResource(id = buttonTextColor))
                        )
                        Button(
                            modifier = Modifier
                                //.align(alignment = Alignment.CenterHorizontally)
                                .padding(vertical = 10.dp),
                            shape = RoundedCornerShape(10.dp),
                            contentPadding = PaddingValues(
                                start = 12.dp,
                                end = 12.dp,
                                top = 5.dp,
                                bottom = 5.dp
                            ),
                            enabled = true,
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = colorResource(id = buttonColor),
                                contentColor = Color.White
                            ),
                            onClick = {
                                checkedIn()
                            }
                        ) {
                            Text(
                                modifier = Modifier.padding(5.dp),
                                text = btnText,
                                color = colorResource(id = R.color.white)
                            )
                        }
                    }
                }
            }
        )
        val context = LocalContext.current

        Row() {
            if (referralPermission.isNotEmpty()) {
                ButtonHeaderItem(
                    Modifier.weight(0.8f, true),
                    R.drawable.icon_doctor,
                    "Referral Slip",
                    "The electronic referral form in a format of the current UNICEF integrated survey referral slip",
                    MaterialTheme.colors.primary,
                ) {
                    if (attendanceUIStatus == AttendanceUIStatus.CHECKED_IN) {
                        onEvent(HomeScreenEvent.StartSurvey)
                    } else {
                        Toast.makeText(
                            context,
                            "Pehle check in kren",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
        Row() {
            if (followupPermission.isNotEmpty()) {
                ButtonItem(
                    Modifier.weight(0.8f, true),
                    R.drawable.ic_followup,
                    "Follow Up",
                    "Some Heading",
                    MaterialTheme.colors.primary
                ) {
                    if (attendanceUIStatus == AttendanceUIStatus.CHECKED_IN) {
                        onEvent(HomeScreenEvent.StartFollowup)
                    } else {
                        Toast.makeText(
                            context,
                            "Pehle check in kren",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            ButtonItem(
                Modifier.weight(0.8f, true),
                R.drawable.ic_saved_forms,
                "Saved \nForms",
                "Some Heading",
                MaterialTheme.colors.primary
            ) {
                if (attendanceUIStatus == AttendanceUIStatus.CHECKED_IN) {
                    onEvent(HomeScreenEvent.StartSavedForm)
                } else {
                    Toast.makeText(
                        context,
                        "Pehle check in kren",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            // Spacer(modifier = Modifier.padding(10.dp))
            ButtonItem(
                Modifier.weight(0.8f, true),
                R.drawable.ic_search,
                "Search\n",
                "Some Heading",
                MaterialTheme.colors.primary
            ) {
                if (attendanceUIStatus == AttendanceUIStatus.CHECKED_IN) {
                    onEvent(HomeScreenEvent.StartSearch)
                } else {
                    Toast.makeText(
                        context,
                        "Pehle check in kren",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        Heading(label = "Statistics")
        Row() {
            StatItem(
                Modifier.weight(0.8f, true),
                "Total Form(s) Submitted Today",
                stats.totalTodaySubmittedForms.toString()
            )
            Spacer(modifier = Modifier.padding(10.dp))
            StatItem(
                Modifier.weight(0.8f, true),
                "Total Form(s) Submitted",
                stats.totalSubmittedForms.toString(),
            )
        }

        Heading(label = "Recent Forms")

        //

        if (stats.userRecentForm == null || stats.userRecentForm.isEmpty()) {

            Box(modifier = Modifier.fillMaxHeight(), contentAlignment = Alignment.Center) {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Icon(imageVector = Icons.Default.History, contentDescription = "hisory")

                    Text(
                        "No History Found",
                        modifier = Modifier
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                }
            }

        } else {
            Column(verticalArrangement = Arrangement.spacedBy(4.dp))
            {


                stats.userRecentForm.forEach { form ->
                    HistoryItem(
                        modifier = Modifier.fillMaxWidth(),
                        iconId = if (form.referredType != "child") R.drawable.woman else if (form.gender == "MALE") R.drawable.child_male else R.drawable.child_female,
                        name = form.name,
                        id = form.id,
                        referTo = form.referTo,
                        date = form.dob
                    )
                }


            }
        }


    }
}


@Composable
fun StatItem(
    modifier: Modifier,
    label: String,
    value: String
) {

    Card(
        modifier = modifier

            .fillMaxWidth()
            .padding(5.dp),

        contentColor = colorResource(id = R.color.white),
        backgroundColor = Color.Transparent,
        shape = RoundedCornerShape(7.dp),
        content = {
            Column(
                Modifier
                    .fillMaxWidth()
                    .background(
                        Brush.linearGradient(
                            listOf(
                                Color(0xFFEDF4F7),
                                Color(0xFFBEE4F3),
                            )
                        )
                    )
                    .padding(10.dp),
                Arrangement.Center

            ) {

                Text(
                    text = label,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(5.dp),
                    textAlign = TextAlign.Start,
                    style = MaterialTheme.typography.caption,
                    fontWeight = FontWeight.Bold,
                    color = colorResource(id = R.color.matt_black)

                )


                Text(
                    text = value,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(0.dp),
                    textAlign = TextAlign.End,
                    style = MaterialTheme.typography.h5,
                    color = colorResource(id = R.color.white)

                )


            }


        }
    )

}

@Composable
fun UserHeading(label: String) {

    Row(modifier = Modifier.padding(horizontal = 15.dp))
    {
        Image(
            modifier = Modifier

                .height(30.dp)
                .width(30.dp)
                .align(Alignment.CenterVertically),
            painter = painterResource(id = R.drawable.ic_avatar),
            contentDescription = "form",
        )
        Spacer(modifier = Modifier.width(10.dp))

        Text(
            text = "Hey, ",
            modifier = Modifier
                .padding(vertical = 3.dp),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h6,
            color = colorResource(id = R.color.matt_black)
        )

        Text(
            text = label,
            modifier = Modifier
                .padding(vertical = 3.dp),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h6,
            color = colorResource(id = R.color.matt_black)
        )
    }
}

@Composable
fun Heading(label: String) {

    Text(
        text = label,
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 3.dp, horizontal = 5.dp),
        textAlign = TextAlign.Start,
        style = MaterialTheme.typography.h5,
        color = colorResource(id = R.color.matt_black)

    )
}


@Composable
fun HistoryItem(
    modifier: Modifier,
    iconId: Int,
    name: String,
    id: String,
    referTo: String,
    date: String,
) {

    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(5.dp),

        contentColor = colorResource(id = R.color.blue_dark),
        backgroundColor = Color.Transparent,
        shape = RoundedCornerShape(7.dp),
        content = {
            Row(
                Modifier
                    .fillMaxWidth()
                    .background(
                        Brush.linearGradient(
                            listOf(
                                Color(0xFFEDF4F7),
                                Color(0xFFBEE4F3),

                                )
                        )
                    )
                    .padding(10.dp),
                //Arrangement.Center

            ) {
                Image(
                    modifier = Modifier

                        .padding(10.dp)
                        .width(50.dp)
                        .height(50.dp)
                        .align(Alignment.CenterVertically),
                    painter = painterResource(id = iconId),

                    contentDescription = "form",


                    )

                Column() {
                    Text(
                        text = name,
                        modifier = Modifier
                            .fillMaxWidth(),
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.Bold,
                        style = MaterialTheme.typography.body1
                    )


                    Text(
                        text = "ID # $id",
                        modifier = Modifier
                            .fillMaxWidth(),
                        color = colorResource(id = R.color.grey_dark),
                        textAlign = TextAlign.Justify,
                        style = MaterialTheme.typography.body2
                    )

                    Text(
                        text = "Refer to : $referTo",
                        modifier = Modifier
                            .fillMaxWidth(),
                        color = colorResource(id = R.color.grey_dark),
                        textAlign = TextAlign.Justify,
                        style = MaterialTheme.typography.body2
                    )
                    Text(
                        text = "Date : $date",
                        modifier = Modifier
                            .fillMaxWidth(),
                        color = colorResource(id = R.color.grey_dark),
                        textAlign = TextAlign.Justify,
                        style = MaterialTheme.typography.body2
                    )

                }

            }

        }

    )


}


@ExperimentalMaterialApi
@Composable
fun ButtonItem(
    modifier: Modifier,
    iconId: Int,
    heading: String,
    subHeading: String,
    color: Color,
    callback: () -> Unit
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(5.dp),
        contentColor = colorResource(id = R.color.blue_dark),
        backgroundColor = Color.Transparent,
        shape = RoundedCornerShape(7.dp),
        onClick = callback,
        content = {
            Column(
                Modifier
                    .fillMaxWidth()
                    .background(
                        Brush.linearGradient(
                            listOf(
                                Color(0xFFEDF4F7),
                                Color(0xFFBEE4F3),

                                )
                        )
                    )
                    .padding(10.dp),
                Arrangement.Center

            ) {

                Text(
                    text = heading,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(5.dp),
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.body1,
                    color = colorResource(id = R.color.matt_black)

                )


                Icon(
                    modifier = Modifier
                        .padding(15.dp)
                        .width(50.dp)
                        .height(50.dp)
                        .align(Alignment.End),
                    painter = painterResource(id = iconId),
                    contentDescription = "form",
                )


            }


        }
    )
}

@ExperimentalMaterialApi
@Composable
fun ButtonHeaderItem(
    modifier: Modifier,
    iconId: Int,
    heading: String,
    subHeading: String,
    color: Color,
    callback: () -> Unit
) {
    Card(
        modifier = modifier

            .fillMaxWidth()
            .padding(5.dp),
        contentColor = colorResource(id = R.color.white),
        backgroundColor = Color.Transparent,
        shape = RoundedCornerShape(7.dp),
        onClick = callback,
        content = {
            Row(
                modifier = Modifier
                    .background(
                        Brush.linearGradient(
                            listOf(
                                Color(0xFFEDF4F7),
                                Color(0xFFBEE4F3),

                                )
                        )
                    )
                    .padding(10.dp),
                Arrangement.Center

            ) {

                Column(
                    modifier = Modifier
                        .weight(1.0f)
                        .padding(5.dp),

                    )
                {
                    Text(
                        text = heading,
                        textAlign = TextAlign.Start,
                        style = MaterialTheme.typography.h6,
                        color = colorResource(id = R.color.matt_black)
                    )
                    Text(
                        text = subHeading,
                        textAlign = TextAlign.Start,
                        style = MaterialTheme.typography.caption,
                        color = colorResource(id = R.color.grey_dark)
                    )
                }

                Image(
                    modifier = Modifier
                        .padding(15.dp)
                        .weight(1.0f)
                        .align(Alignment.CenterVertically),
                    painter = painterResource(id = iconId),
                    contentDescription = "form",
                )


            }


        }
    )
}


@Composable
fun HeaderIcon() {

    Column(
        modifier = Modifier
            .fillMaxHeight(fraction = 0.3f)
            .background(MaterialTheme.colors.secondary)


    ) {


        /*     Card(Modifier.fillMaxSize(1.2f).offset(y= -83.dp,x = 0.dp).clip(CircleShape),
                 backgroundColor = MaterialTheme.colors.secondary,

                 content = {

                     Text(
                         text = stringResource(id = R.string.home_quotes),
                         color = MaterialTheme.colors.onPrimary,
                     )

                 }
             )*/
        FaceIcon()


    }
}

@Composable
fun HeaderAnimatedIcon() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.user))
    val progress by animateLottieCompositionAsState(
        composition,
        iterations = LottieConstants.IterateForever,
        clipSpec = LottieClipSpec.Progress(0.1f, 0.9f),
    )

    LottieAnimation(
        composition,
        progress,
    )
}


@Composable
fun FaceIcon() {
    Image(
        painterResource(R.drawable.ic_home_b),
        contentDescription = "",
        contentScale = ContentScale.Fit,
        modifier = Modifier
            .fillMaxSize()
            .padding(50.dp)
    )
}


@ExperimentalMaterialApi
@Preview(name = "Homelight theme")
@Composable
fun SignInPreview() {
    MRefTheme {
        Home(
            StatisticsResponse(),
            listOf(),
            {},
            {},
            attendanceUIStatus = AttendanceUIStatus.CHECKED_IN
        ) {}
    }
}

