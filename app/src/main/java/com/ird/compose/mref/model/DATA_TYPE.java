package com.ird.compose.mref.model;

public enum DATA_TYPE {
    checkInEvent,
    checkOutEvent,
    schedule
}
