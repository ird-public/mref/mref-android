package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class User (
    val name : String,
    val mappedId : Int,
    val username : String,
    val status : String,
    var permission : List<Permission> = arrayListOf(),
)