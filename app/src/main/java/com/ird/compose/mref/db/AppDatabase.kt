package com.ird.compose.mref.db

import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.RoomDatabase
import com.ird.compose.mref.db.dao.FormDao
import com.ird.compose.mref.db.dao.LocationDao
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.migration.Migration
import com.ird.compose.mref.db.dao.AttendanceDao
import com.ird.compose.mref.model.*


@Database(
    entities = [Location::class, ReferalReason::class, ChildPayload::class,OfflineForm::class,AttendanceCheckIn::class,AttendanceCheckOut::class],
    version = 7,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun LocationDao(): LocationDao
    abstract fun FormDao(): FormDao
    abstract fun AttendanceDao(): AttendanceDao
}


