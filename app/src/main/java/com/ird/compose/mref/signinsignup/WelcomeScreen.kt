/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.signinsignup

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.layout.boundsInParent
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.ird.compose.mref.R
import com.ird.compose.mref.theme.LightThemeColors
import com.ird.compose.mref.theme.MRefTheme
import com.ird.compose.mref.util.supportWideScreen
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.google.accompanist.insets.statusBarsPadding


sealed class WelcomeEvent {
    /*data class SignInSignUp(val email: String) : WelcomeEvent()
    object SignInAsGuest : WelcomeEvent()*/
    object CallMetaData : WelcomeEvent()
    object onError : WelcomeEvent()
    data class SignInUser(val username: String, val password: String) : WelcomeEvent()
}


@OptIn(ExperimentalAnimationApi::class)
@Composable
fun WelcomeScreen(
    loadingText: String,
    isSignInVisible: Boolean,
    isFirstTime: Boolean,
    isError: Boolean,
    isKeyboardVisible: Boolean,
    onEvent: (WelcomeEvent) -> Unit,

    ) {


    Surface(modifier = Modifier.supportWideScreen()) {


        Column(
            modifier = Modifier
                .fillMaxWidth()
                .statusBarsPadding()
                .navigationBarsWithImePadding()
                .verticalScroll(rememberScrollState())


        ) {

            if (!isKeyboardVisible) {

                Branding(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(0.4f, true)
                ) {
                    onEvent(
                        WelcomeEvent.CallMetaData
                    )
                }
            }



            if (isFirstTime) {
                SyncButton(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(0.5f, true)
                        .padding(horizontal = 40.dp),
                    onEvent
                )
            } else {
                if (isSignInVisible) {
                    SignInContent(
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(0.5f, true)
                            .padding(horizontal = 60.dp),
                        visible = isSignInVisible,
                        onSignInSubmitted = { email, password ->
                            onEvent(
                                WelcomeEvent.SignInUser(
                                    email, password
                                )
                            )
                        }
                    )
                } else {
                    Loading(

                        onEvent = onEvent,
                        isSignInVisible = isSignInVisible,
                        loadingText = loadingText,
                        isError = isError,
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(0.5f, true)
                            .padding(horizontal = 40.dp)
                    )
                }
            }

            Footer(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.2f, true)
            )

        }
    }

}


@Composable
private fun Footer(modifier: Modifier) {

  /*  Box(modifier = Modifier.fillMaxWidth().wrapContentHeight(align = Alignment.Top),
        contentAlignment = Alignment.Center

    ) {

    }*/







        Box(
        modifier = modifier.background(
            Brush.verticalGradient(
                listOf(
                    Color(0xFFFFFFFF),
                    Color(0xFFEDF4F7),
                    Color(0xFFBEE4F3),
                ),
            )
        ),
    ) {

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
                .align(Alignment.BottomCenter),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            Text(
                text = "Powered By",
                textAlign = TextAlign.Start,
                style = MaterialTheme.typography.h6
            )
            Spacer(Modifier.width(10.dp))
            Image(
                modifier = Modifier
                    .height(70.dp)
                    .width(40.dp),
                painter = painterResource(id = R.drawable.ic_zm),
                contentDescription = null,
                contentScale = ContentScale.Fit
            )
        }

    }

}


private fun Modifier.brandingPreferredHeight(
    showBranding: Boolean,
    heightDp: Dp
): Modifier {
    return if (!showBranding) {
        this
            .wrapContentHeight(unbounded = true)
            .height(heightDp)
    } else {
        this
    }
}

@Composable

private fun Branding(modifier: Modifier = Modifier, onSyncClicked: () -> Unit) {

    Box(modifier = modifier) {

        Image(
            painter = painterResource(id = R.drawable.login_bg_3),
            modifier = modifier,
            contentDescription = null,
            contentScale = ContentScale.FillBounds
        )

        IconButton(
            modifier = Modifier.align(Alignment.TopEnd),
            onClick = {
                onSyncClicked()
            }) {
            Icon(
                modifier = Modifier
                    .height(70.dp)
                    .width(70.dp)
                    .padding(10.dp),
                imageVector = Icons.Filled.Sync,
                contentDescription = stringResource(id = R.string.back),
                tint = colorResource(id = R.color.white)
            )
        }

        Box(modifier = modifier, contentAlignment = Alignment.Center)
        {
            Logo(
                modifier = Modifier
                    .padding(vertical = 50.dp)
                    .align(alignment = Alignment.Center)

            )

        }

    }


}

@Composable
private fun Logo(
    modifier: Modifier = Modifier,
    lightTheme: Boolean = MaterialTheme.colors.isLight
) {

    val assetId = R.drawable.mref_logo
    Icon(
        painter = painterResource(id = assetId),
        modifier = modifier,
        contentDescription = null,
        tint = colorResource(id = R.color.white)
    )
}


@ExperimentalAnimationApi
@Composable
private fun Loading(
    onEvent: (WelcomeEvent) -> Unit,
    isSignInVisible: Boolean,
    loadingText: String,
    isError: Boolean,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {

        if (isError) {

            IconButton(

                onClick = {
                    onEvent(WelcomeEvent.CallMetaData)
                }) {
                Icon(
                    modifier = Modifier
                        .height(50.dp)
                        .width(50.dp),
                    imageVector = Icons.Filled.Refresh,
                    contentDescription = stringResource(id = R.string.back),
                    tint = colorResource(id = R.color.blue)
                )
            }

        } else {
            CircularProgressIndicator(
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp),
                LightThemeColors.primary
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = loadingText,
            textAlign = TextAlign.Center
        )
    }
}


@ExperimentalAnimationApi
@Composable
fun SignInContent(
    modifier: Modifier = Modifier,
    visible: Boolean,
    onSignInSubmitted: (email: String, password: String) -> Unit,
) {

    Column(
        modifier = modifier.height(LocalConfiguration.current.screenHeightDp.dp),
        verticalArrangement = Arrangement.Center,

        ) {
        val focusRequester = remember { FocusRequester() }
        val emailState = remember {
            EmailState()
        }

        Text(
            text = "Sign In",
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 0.dp),
            style = MaterialTheme.typography.h5,
            textAlign = TextAlign.Left
        )
        Spacer(modifier = Modifier.height(10.dp))

        Email(emailState, onImeAction = { /*focusRequester.requestFocus() */ })

        Spacer(modifier = Modifier.height(10.dp))

        val passwordState = remember { PasswordState() }
        Password(
            label = stringResource(id = R.string.password),
            passwordState = passwordState,
            //modifier = Modifier.focusRequester(focusRequester),
            onImeAction = { onSignInSubmitted(emailState.text, passwordState.text) }
        )
        Spacer(modifier = Modifier.height(5.dp))

        GradientButton(
            text = stringResource(id = R.string.sign_in),
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            shape = RoundedCornerShape(40.dp),
            enabled = emailState.isValid && passwordState.isValid,

            onClick = { onSignInSubmitted(emailState.text, passwordState.text) }
        )

        Spacer(modifier = Modifier.height(5.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
                ,
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.Top

        )
        {


            Image(
                modifier = Modifier
                    .height(60.dp)
                    .width(100.dp),
                painter = painterResource(id = R.drawable.logo_unicef_full),
                contentDescription = null,
                contentScale = ContentScale.Fit
            )

            Spacer(modifier = Modifier.padding(10.dp))
            Image(
                modifier = Modifier
                    .height(60.dp)
                    .width(70.dp),
                painter = painterResource(id = R.drawable.ird_logo),
                contentDescription = null,
                contentScale = ContentScale.Fit
            )

        }


        /*    Button(
                shape = RoundedCornerShape(40.dp),
                onClick = { onSignInSubmitted(emailState.text, passwordState.text) },
                modifier = Modifier
                    .fillMaxWidth(),
                enabled = false,
            ) {
                Text(
                    modifier = Modifier.padding(5.dp),
                    text = stringResource(id = R.string.sign_in)
                )
            }*/
    }


}

@Composable
private fun SyncButton(
    modifier: Modifier = Modifier,
    onEvent: (WelcomeEvent) -> Unit,
) {

    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {


        Text(
            text = "Please click on SYNC NOW for onetime settings",
            textAlign = TextAlign.Center
        )
        GradientButton(
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp),
            onClick = { onEvent(WelcomeEvent.CallMetaData) },
            text = stringResource(id = R.string.sync_now)
        )


    }


}


@Preview(name = "Welcome light theme")
@Composable
fun WelcomeScreenPreview() {
    MRefTheme {
        WelcomeScreen("loadingText", true, true, isError = true, false) {}
    }
}

@Composable
fun WelcomeScreenPreviewDark() {
    MRefTheme(darkTheme = true) {
        WelcomeScreen("loadingText", false, false, isError = true, false) {}
    }
}

@Composable
fun GradientButton(
    text: String,
    modifier: Modifier = Modifier,
    shape: Shape = RoundedCornerShape(10.dp),
    enabled: Boolean = true,
    onClick: () -> Unit = { },
) {
    val inactiveGradient =
        Brush.verticalGradient(
            listOf(
                Color(0xFFeef2f3),
                Color(0xFFE1E2E2)
            )
        )
    val gradient =
        Brush.verticalGradient(
            listOf(
                Color(0xFF11B0E6),
                Color(0xFF007FAA)
            )
        )

    Button(
        modifier = modifier,
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent),
        contentPadding = PaddingValues(),
        enabled = enabled,
        shape = shape,
        onClick = { onClick() },
    ) {
        Box(
            modifier = Modifier
                .background(if (enabled) gradient else inactiveGradient)
                .then(modifier),
            contentAlignment = Alignment.Center,
        ) {
            Text(
                modifier = Modifier.padding(5.dp),
                text = text,
                color = colorResource(id = if (enabled) R.color.white else R.color.grey_dark)
            )
        }
    }
}
