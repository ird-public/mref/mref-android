package com.ird.compose.mref.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.ird.compose.mref.db.Converter


@Entity(tableName = "form")
@TypeConverters(Converter::class)
data class ChildPayload(

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("referalEntity") var referalEntity: String = "",
    @SerializedName("identifier") var identifier: String = "",
    @SerializedName("fatherName") var fatherName: String = "",
    @SerializedName("husbandName") var husbandName: String = "",
    @SerializedName("gender") var gender: String = "",
    @SerializedName("dob") var dob: String = "",
    @SerializedName("epiNo") var epiNo: String = "",
    @SerializedName("dateRefered") var dateRefered: String = "",
    @SerializedName("name") var name: String = "",
    @SerializedName("visitOutcome") var visitOutcome: String = "UNKNOWN",
    @SerializedName("address") var address: String = "",
    @SerializedName("userId") var userId: String = "",
    @SerializedName("ucId") var ucId: String = "" ,
    @SerializedName("uc") var uc: String = "",
    @SerializedName("town") var town: String = "",
    @SerializedName("district") var district: String = "",
    @SerializedName("townId") var townId: String = "",
    @SerializedName("districtId") var districtId: String = "",
    @SerializedName("latitude") var latitude: String = "",
    @SerializedName("longitude") var longitude: String = "",
    @SerializedName("reasons") var reasons: List<Reasons> = arrayListOf(),
    @SerializedName("nic") var nic: String = "",
    @SerializedName("primaryNumber") var primaryNumber: String = "",
    @SerializedName("secondaryNumber") var secondaryNumber: String = "",
    @SerializedName("centerName") var centerName: String = ""


)

