package com.ird.compose.mref.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.ird.compose.mref.db.Converter


@Entity(tableName = "offline_form")
@TypeConverters(Converter::class)
data class OfflineForm(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var formType: String = "",
    var payload: ChildPayload = ChildPayload()
)