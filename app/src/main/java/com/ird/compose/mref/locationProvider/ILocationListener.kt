package com.ird.compose.mref.locationProvider

import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult

interface ILocationListener {
    fun onLocationReceived(locationResult: LocationResult?, callback: LocationCallback?)
}