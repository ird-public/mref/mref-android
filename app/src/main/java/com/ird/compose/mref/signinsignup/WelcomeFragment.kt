/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.signinsignup

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.ComposeView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.accompanist.insets.ProvideWindowInsets
import com.ird.compose.mref.App
import com.ird.compose.mref.Screen
import com.ird.compose.mref.db.dao.LocationDao
import com.ird.compose.mref.model.DevicePreference
import com.ird.compose.mref.navigate
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.survey.getLocationHeirarchy
import com.ird.compose.mref.theme.MRefTheme
import com.ird.compose.mref.util.DateTimeUtils
import java.lang.NullPointerException
import java.time.Duration
import javax.inject.Inject

/**
 * Fragment containing the welcome UI.
 */
class WelcomeFragment : Fragment() {

    @Inject
    lateinit var mainRespository: MainRepository

    @Inject
    lateinit var locationDao: LocationDao

    @Inject
    lateinit var preference: DevicePreference


    private val viewModel: WelcomeViewModel by viewModels { ViewModelFactory(mainRespository) }

    private val isSignInVisible =
        mutableStateOf<Boolean>(false)
    private val isError = mutableStateOf<Boolean>(false)
    private val isFirstTime = mutableStateOf<Boolean>(false)
    private val loadingText = mutableStateOf<String>(
        "We are setting up the app for you. \n" +
                "Please bear with us"
    )

    private val isKeyboardVisible = mutableStateOf<Boolean>(
        false
    )


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity?.application as App).appComponent.inject(this)

        viewModel.navigateTo.observe(viewLifecycleOwner) { navigateToEvent ->
            navigateToEvent.getContentIfNotHandled()?.let { navigateTo ->
                navigate(navigateTo, Screen.Welcome)
            }
        }

        viewModel.onSignIn.observe(viewLifecycleOwner) {
            preference.saveUser(it)
            isSignInVisible.value = true
            viewModel.NavigateHomeScreen()

        }

        viewModel.errorMessage.observe(viewLifecycleOwner) {
            isSignInVisible.value = true
            if (it.contains("User not found") || it.contains("Not Acceptable"))
                Toast.makeText(context, "Username is incorrect", Toast.LENGTH_SHORT)
                    .show()
            else if (it.contains("Bad Request"))
                Toast.makeText(context, "Password is incorrect", Toast.LENGTH_SHORT)
                    .show()
            else
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()

        }






        isFirstTime.value = DevicePreference.isFirstTime()



        viewModel.onLocationRecieved.observe(viewLifecycleOwner) {
            if (it.location.size > 0) {
                locationDao.insertAll(it.location)
                locationDao.insertAllReferralReasons(it.referalReason)
                it.location.forEach {
                    it.hierarchy = getLocationHeirarchy(it, locationDao)
                }
                locationDao.insertAll(it.location)

                isSignInVisible.value = true
                DevicePreference.invalidateFirstTimeFlag()
            }
        }


        viewModel.isError.observe(viewLifecycleOwner) {
            if (true) {
                isFirstTime.value = true
            }

            isError.value = it
            loadingText.value = "Something went wrong.\n Please try again"
        }

        val locations = locationDao.getAll()
        if (locations != null && locations.size > 0)
            isSignInVisible.value = true
        else
            isSignInVisible.value = false

        val view: View = ComposeView(requireContext()).apply {


            setContent {
                MRefTheme {
                    ProvideWindowInsets(
                        //  consumeWindowInsets = false,
                        windowInsetsAnimationsEnabled = true
                    ) {
                        WelcomeScreen(
                            loadingText = loadingText.value,
                            isSignInVisible = isSignInVisible.value,
                            isFirstTime = isFirstTime.value,
                            isError = isError.value,
                            isKeyboardVisible = isKeyboardVisible.value,
                            onEvent = { event ->
                                when (event) {
                                    is WelcomeEvent.CallMetaData -> {
                                        isFirstTime.value = false
                                        isSignInVisible.value = false

                                        loadingText.value =
                                            "We are setting up the app for you. \n" +
                                                    "Please bear with us"
                                        isError.value = false

                                        viewModel.getLocations()
                                    }
                                    is WelcomeEvent.SignInUser -> {

                                        if (DateTimeUtils.isInternetAvailable(requireContext())) {
                                            if(!event.username.trim().equals("") && !event.password.trim().equals("")) {
                                                viewModel.signInUser(event.username, event.password)
                                                preference.savePassword(event.password)
                                                isSignInVisible.value = false
                                                loadingText.value = "Fetching user information..."
                                            }else{
                                                Toast.makeText(requireContext(), "username and password can't be empty", Toast.LENGTH_SHORT).show()
                                            }
                                        } else {
                                            try {
                                                val user = preference.getUser()
                                                if (user.username.equals(event.username)
                                                    && preference.getPassword()
                                                        .equals(event.password)
                                                ) {
                                                    isSignInVisible.value = true
                                                    viewModel.NavigateHomeScreen()
                                                } else {
                                                    Toast.makeText(
                                                        requireContext(),
                                                        "User device pr maujood nahin hai. Kya aap internet ke sath login karna chahen ge?",
                                                        Toast.LENGTH_LONG
                                                    ).show()
                                                }
                                            } catch (exception: NullPointerException) {
                                                Toast.makeText(
                                                    requireContext(),
                                                    "User device pr maujood nahin hai. Kya aap internet ke sath login karna chahen ge?",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        }
                                    }
                                }
                            }

                        )
                    }
                }
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(view) { v, insets ->
            isKeyboardVisible.value = insets.isVisible(WindowInsetsCompat.Type.ime())
            insets
        }

        return view
    }
}
