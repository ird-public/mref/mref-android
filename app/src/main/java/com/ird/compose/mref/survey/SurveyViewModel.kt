/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.survey

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.text.format.DateUtils
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.gson.Gson
import com.ird.compose.mref.R
import com.ird.compose.mref.db.dao.LocationDao
import com.ird.compose.mref.model.*
import com.ird.compose.mref.model.Keys.*
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.util.DateTimeUtils
import com.ird.compose.mref.util.Regex.zmIDRegex
import kotlinx.coroutines.*
import okhttp3.ResponseBody
import java.text.SimpleDateFormat
import java.util.*

const val simpleDateFormatPattern = "dd-MM-yyyy"


enum class FormType {
    REFERRAL_SLIP, FOLLOWUP, SEARCH
}

class SurveyViewModel(
    private val context: Context,
    private val surveyRepository: SurveyRepository,
    private val photoUriManager: PhotoUriManager,
    private val locationDao: LocationDao,
    private val formType: FormType,
    private val preference: DevicePreference,
) : ViewModel() {


    val errorMessage = MutableLiveData<String>()

    private val _uiState = MutableLiveData<SurveyState>()
    val uiState: LiveData<SurveyState>
        get() = _uiState

    private val _onSubmission = MutableLiveData<ChildPayload>()
    val onSubmissionComplete: LiveData<ChildPayload>
        get() = _onSubmission

    var askForPermissions by mutableStateOf(true)
        private set

    private lateinit var surveyInitialState: SurveyState


    // Uri used to save photos taken with the camera
    private var uri: Uri? = null

    init {
        viewModelScope.launch {
            val survey = surveyRepository.getSurvey(locationDao, formType, preference)

            // Create the default questions state based on the survey questions
            val questions: List<QuestionState> = survey.questions.mapIndexed { index, question ->
                val showPrevious = index > 0
                val showDone = index == survey.questions.size - 1
                QuestionState(
                    question = question,
                    questionIndex = index,
                    totalQuestionsCount = survey.questions.size,
                    showPrevious = showPrevious,
                    showDone = showDone
                )
            }
            surveyInitialState = SurveyState.Questions(survey.title, questions)
            _uiState.value = surveyInitialState
        }
    }


    fun onDatePicked(formType: FormType, questionId: Int, pickerSelection: Long?) {
        val selectedDate = Date().apply {
            time = pickerSelection ?: getCurrentDate(questionId)
        }
        val formattedDate =
            SimpleDateFormat(simpleDateFormatPattern, Locale.getDefault()).format(selectedDate)

        if (formType == FormType.SEARCH) {
            updateDateOnFilters(formattedDate)
        } else if (formType == FormType.FOLLOWUP) {
            updateBiodataWithActionResult(questionId, formattedDate)
        } else {
            updateStateWithActionResult(questionId, SurveyActionResult.Date(formattedDate))
        }
    }


    fun onAgePicked(formType: FormType, questionId: Int, date: String) {
        if (formType == FormType.SEARCH) {
            updateDateOnFilters(date)
        } else if (formType == FormType.FOLLOWUP) {
            updateBiodataWithActionResult(questionId, date)
        } else {
            updateStateWithActionResult(questionId, SurveyActionResult.Date(date))
        }


    }

    fun onTextChange(questionId: Int, text: String) {
        updateStateWithTextResult(questionId, text)
    }

    fun getCurrentDate(questionId: Int): Long {
        if (formType == FormType.FOLLOWUP)
            return getBiodataSelectedDate(questionId)
        else return getSelectedDate(questionId)
    }


    fun getUriToSaveImage(): Uri? {
        uri = photoUriManager.buildNewUri()
        return uri
    }

    fun onImageSaved() {
        uri?.let { uri ->
            getLatestQuestionId()?.let { questionId ->
                updateStateWithActionResult(questionId, SurveyActionResult.Photo(uri))
            }
        }
    }

    // TODO: Ideally this should be stored in the database
    fun doNotAskForPermissions() {
        askForPermissions = false
    }

    private fun updateStateWithActionResult(questionId: Int, result: SurveyActionResult) {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {
            val question =
                latestState.questionsState.first { questionState ->
                    questionState.question.id == questionId
                }
            question.answer = Answer.Action(result)
            question.enableNext = true
        }
    }

    private fun updateBiodataWithActionResult(questionId: Int, dateOfBirth: String) {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {


            latestState.questionsState.forEach {
                when (it.question.key) {

                    BIODATA -> {
                        val biodata = it.answer as Answer.Biodata
                        biodata.basicInfo.dob = dateOfBirth
                        it.answer = null
                        it.answer = Answer.Biodata(biodata.basicInfo, preference.getUser())
                        it.enableNext = true
                    }
                }
            }


        }

        /*      if (latestState != null && latestState is SurveyState.Questions) {
                  val question =
                      latestState.questionsState.first { questionState ->
                          questionState.question.id == questionId
                      }

                  question.enableNext = true
              }*/
    }


    private fun updateDateOnFilters(date: String) {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Search) {
            latestState.date = date
        }
    }


    fun goBacktoForms() {
        _uiState.value = surveyInitialState
    }


    fun computeResult(
        formType: FormType,
        patientDetails: ChildPayload,
        locationDao: LocationDao,
    ) {
        val result = surveyRepository.getSurveyResult(formType, patientDetails, locationDao)
        _uiState.value = SurveyState.Result(R.string.empty_hint, result)
    }

    fun getStringRes(resId: Int): String {
        return context.getString(resId)
    }

    fun createPayload(
        surveyQuestions: SurveyState.Questions,
        preference: DevicePreference,
        latitude: Double, longitude: Double
    ) {

        var childPayload = ChildPayload()
        var referTo = -1
        var isPregnant = false

        if (formType == FormType.REFERRAL_SLIP) {
            val entityQuestion =
                surveyQuestions.questionsState.filter { it.question.key == REFERRAL_ENTITY }.first()

            val entity = context.getString((entityQuestion.answer as Answer.SingleChoice).answer)
                .lowercase()

            childPayload.referalEntity = entity

            surveyQuestions.questionsState.forEach { questionState ->


                questionState.answer?.let {
                    when (questionState.question.key) {
                        REFERRAL_ENTITY -> {
                            childPayload.referalEntity =
                                context.getString((questionState.answer as Answer.SingleChoice).answer)
                                    .lowercase()
                        }
                        MREF_QRCODE -> {
                            childPayload.identifier =
                                ((questionState.answer as Answer.Action).result as SurveyActionResult.QRCode).id
                        }
                        FATHER_NAME -> {
                            childPayload.fatherName =
                                (questionState.answer as Answer.Text).answerValue
                        }

                        HUSBAND_NAME -> {
                            if (!entity.equals("child")) {
                                childPayload.husbandName =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }


                        CHILD_GENDER -> {

                            if (entity.equals("child")) {
                                childPayload.gender =
                                    context.getString((questionState.answer as Answer.SingleChoice).answer)
                                        .uppercase()
                            }
                        }


                        WOMAN_AGE -> {

                            if (!entity.equals("child")) {
                                childPayload.dob =
                                    ((questionState.answer as Answer.Action).result as SurveyActionResult.Date).date
                            }

                        }

                        CHILD_AGE -> {

                            if (entity.equals("child")) {
                                childPayload.dob =
                                    ((questionState.answer as Answer.Action).result as SurveyActionResult.Date).date
                            }

                        }


                        CHILD_NAME -> {

                            if (entity.equals("child")) {
                                childPayload.name =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }

                        WOMAN_NAME -> {

                            if (!entity.equals("child")) {
                                childPayload.name =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }

                        CNIC_WOMAN -> {

                            if (!entity.equals("child")) {
                                childPayload.nic =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }


                        PHONE_PRIMARY_WOMAN -> {

                            if (!entity.equals("child")) {
                                childPayload.primaryNumber =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }

                        PHONE_SECONDARY_WOMAN -> {

                            if (!entity.equals("child")) {
                                childPayload.secondaryNumber =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }


                        CNIC_CHILD -> {

                            if (entity.equals("child")) {
                                childPayload.nic =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }


                        PHONE_PRIMARY_CHILD -> {

                            if (entity.equals("child")) {
                                childPayload.primaryNumber =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }

                        PHONE_SECONDARY_CHILD -> {

                            if (entity.equals("child")) {
                                childPayload.secondaryNumber =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }


                        EPI_NO -> {
                            if (entity.equals("child")) {
                                childPayload.epiNo =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }


                        EPI_NO_WOMAN -> {
                            if (!entity.equals("child")) {
                                childPayload.epiNo =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }

                        IS_REFERRED_CHILD -> {

                            if (entity.equals("child")) {
                                val outcome =
                                    context.getString((questionState.answer as Answer.SingleChoice).answer)

                                if (outcome.equals("Yes", true))
                                    childPayload.visitOutcome = "REFERRED"
                                else
                                    childPayload.visitOutcome = "NOT_REFERRED"

                            }
                        }


                        IS_PREGNANT -> {

                            if (!entity.equals("child")) {
                                val outcome =
                                    context.getString((questionState.answer as Answer.SingleChoice).answer)

                                isPregnant = outcome.equals("Yes", true)

                            }
                        }


                        IS_REFERRED -> {

                            if (!entity.equals("child")) {
                                val outcome =
                                    context.getString((questionState.answer as Answer.SingleChoice).answer)

                                if (outcome.equals("Yes", true))
                                    childPayload.visitOutcome = "REFERRED"
                                else
                                    childPayload.visitOutcome = "NOT_REFERRED"

                            }
                        }

                        ADDRESS -> {
                            if (!entity.equals("child")) {
                                childPayload.address =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }
                        AREA_NAME -> {
                            if (entity.equals("child")) {
                                childPayload.address =
                                    (questionState.answer as Answer.Text).answerValue
                            }
                        }


                        UC -> {
                            if (questionState.answer is Answer.SelectLoction) {
                                val location =
                                    (questionState.answer as Answer.SelectLoction).location
                                val town = locationDao.getLocation(location.parentLocation!!)
                                childPayload.ucId = location.locationId.toString()
                                childPayload.townId = town.locationId.toString()
                                childPayload.districtId = town.parentLocation!!.toString()
                            }
                        }


                        REFERRAL_CENTER_CHILD -> {
                            if (entity.equals("child")) {
                                val location =
                                    (questionState.answer as Answer.SelectLoction).location
                                referTo = location.locationId
                                childPayload.centerName = location.name!!
                            }
                        }

                        CENTER -> {
                            if (!entity.equals("child")) {
                                val location =
                                    (questionState.answer as Answer.SelectLoction).location
                                referTo = location.locationId
                                childPayload.centerName = location.name!!
                            }
                        }


                        REFER_FOR_CHILD,

                        -> {
                            if (entity.equals("child")) {
                                val selectedValues =
                                    (questionState.answer as Answer.MultipleChoiceReferral).reasons

                                val reasons: MutableList<Reasons> = mutableListOf()

                                selectedValues.forEach { reason ->
                                    val reason: Reasons = Reasons().apply {
                                        this.name = reason.shortName;
                                        this.displayText = reason.name
                                        this.status = "ACTIVE"
                                        this.dateRefered = getCurrentDate()
                                        this.referedBy = preference.getUser().mappedId

                                    }
                                    reasons.add(reason)
                                }
                                childPayload.reasons = reasons
                            }
                        }


                        REFER_FOR_WOMAN,
                        -> {
                            if (!entity.equals("child") && !isPregnant) {
                                val selectedValues =
                                    (questionState.answer as Answer.MultipleChoiceReferral).reasons

                                val reasons: MutableList<Reasons> = mutableListOf()

                                selectedValues.forEach { reason ->
                                    val reason: Reasons = Reasons().apply {
                                        this.name = reason.shortName;
                                        this.displayText = reason.name
                                        this.status = "ACTIVE"
                                        this.dateRefered = getCurrentDate()
                                        this.referedBy = preference.getUser().mappedId

                                    }
                                    reasons.add(reason)
                                }
                                childPayload.reasons = reasons
                            }
                        }


                        REFER_FOR_PREGNANT,

                        -> {
                            if (!entity.equals("child") && isPregnant) {
                                val selectedValues =
                                    (questionState.answer as Answer.MultipleChoiceReferral).reasons

                                val reasons: MutableList<Reasons> = mutableListOf()

                                selectedValues.forEach { reason ->
                                    val reason: Reasons = Reasons().apply {
                                        this.name = reason.shortName;
                                        this.displayText = reason.name
                                        this.status = "ACTIVE"
                                        this.dateRefered = getCurrentDate()
                                        this.referedBy = preference.getUser().mappedId

                                    }
                                    reasons.add(reason)
                                }
                                childPayload.reasons = reasons
                            }
                        }

                    }
                }
            }

        } else {
            surveyQuestions.questionsState.forEach { questionState ->


                questionState.answer?.let {
                    when (questionState.question.key) {

                        BIODATA -> {
                            childPayload = (questionState.answer as Answer.Biodata).basicInfo
                        }
                        REFERRALS -> {
                            childPayload.reasons = (questionState.answer as Answer.Referral).reasons
                        }
                    }
                }
            }

        }

        childPayload.reasons.forEach {
            it.referedTo = referTo.toString()
            if (it.status == "CLOSED" && it.dateClosed == "") {
                it.closedBy = preference.getUser().mappedId.toString()
                it.dateClosed = getCurrentDate()
            }
        }


        childPayload.dateRefered = getCurrentDate()
        childPayload.userId = preference.getUser().mappedId.toString()
        childPayload.latitude = latitude.toString()
        childPayload.longitude = longitude.toString()


        computeResult(formType = formType, childPayload, locationDao)


    }

    private fun resetAnswers() {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {


            latestState.questionsState.forEach {
                it.answer = null
                it.enableNext = false
            }
        }
    }


    private fun updateStateOfEligibleQuestion(patient: Patient) {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {


            latestState.questionsState.forEach {
                when (it.question.key) {

                    CHILD_NAME -> {
                        if (patient.referalEntity.equals("child") && checkEmpty(patient.data.name)) {
                            it.answer = Answer.Text(patient.data.name)
                            it.enableNext = true
                        }
                    }

                    HUSBAND_NAME -> {
                        if (!patient.referalEntity.equals("child") && (patient.data.husbandFirstName != null) && checkEmpty(
                                patient.data.husbandFirstName!!
                            )
                        ) {
                            it.answer = Answer.Text(patient.data.husbandFirstName!!)
                            it.enableNext = true
                        }
                    }

                    CNIC_CHILD -> {
                        if (patient.referalEntity.equals("child") && checkEmpty(patient.data.nic)) {
                            it.answer = Answer.Text(patient.data.nic)
                            it.enableNext = true
                        }
                    }
                    CNIC_WOMAN -> {
                        if (!patient.referalEntity.equals("child") && checkEmpty(patient.data.nic)) {
                            it.answer = Answer.Text(patient.data.nic)
                            it.enableNext = true
                        }
                    }

                    PHONE_PRIMARY_CHILD -> {
                        if (patient.referalEntity.equals("child") && checkEmpty(patient.data.primaryNumber)) {
                            it.answer = Answer.Text(patient.data.primaryNumber)
                            it.enableNext = true
                        }
                    }

                    PHONE_SECONDARY_CHILD -> {
                        if (patient.referalEntity.equals("child") && checkEmpty(patient.data.secondaryNumber)) {
                            it.answer = Answer.Text(patient.data.secondaryNumber)
                            it.enableNext = true
                        }
                    }

                    PHONE_PRIMARY_WOMAN -> {
                        if (!patient.referalEntity.equals("child") && checkEmpty(patient.data.primaryNumber)) {
                            it.answer = Answer.Text(patient.data.primaryNumber)
                            it.enableNext = true
                        }
                    }

                    PHONE_SECONDARY_WOMAN -> {
                        if (!patient.referalEntity.equals("child") && checkEmpty(patient.data.secondaryNumber)) {
                            it.answer = Answer.Text(patient.data.secondaryNumber)
                            it.enableNext = true
                        }
                    }


                    UC -> {
                        if (patient.data.ucId != -1) {
                            val location = locationDao.getLocation(patient.data.ucId!!.toInt())
                            it.answer = Answer.SelectLoction(location, location.name!!)
                            it.enableNext = true
                        }
                    }
                    REFERRAL_ENTITY -> {
                        if (patient.referalEntity.equals(context.getString(R.string.child), true)) {
                            it.answer = Answer.SingleChoice(R.string.child)
                            it.enableNext = true
                            it.question.nextQuestion = 5


                        } else if (patient.referalEntity.equals(
                                context.getString(R.string.woman),
                                true
                            )
                        ) {
                            it.answer = Answer.SingleChoice(R.string.women)
                            it.enableNext = true
                            it.question.nextQuestion = 15
                        }
                    }
                    WOMAN_NAME -> {

                        if (!patient.referalEntity.equals("child") && checkEmpty(patient.data.name)) {

                            it.answer = Answer.Text(patient.data.name)
                            it.enableNext = true
                        }
                    }


                    CHILD_AGE -> {
                        if (patient.referalEntity.equals("child") && checkEmpty(patient.data.dob)) {

                            it.answer = Answer.Action(SurveyActionResult.Date(patient.data.dob))
                            it.enableNext = true
                        }
                    }

                    WOMAN_AGE -> {
                        if (!patient.referalEntity.equals("child") && checkEmpty(patient.data.dob)) {

                            it.answer = Answer.Action(SurveyActionResult.Date(patient.data.dob))
                            it.enableNext = true
                        }
                    }

                    ADDRESS -> {
                        if (checkEmpty(patient.data.address)) {
                            it.answer = Answer.Text(patient.data.address)
                            it.enableNext = true
                        }
                    }
                    FATHER_NAME -> {
                        if (patient.data.fatherName != null && checkEmpty(patient.data.fatherName!!)) {
                            it.answer = Answer.Text(patient.data.fatherName!!)
                            it.enableNext = true
                        }
                    }
                    EPI_NO_WOMAN,
                    -> {
                        if (!patient.referalEntity.equals("child") && checkEmpty(patient.data.epiNo)) {
                            it.answer = Answer.Text(patient.data.epiNo)
                            it.enableNext = true
                        }
                    }

                    EPI_NO -> {
                        if (patient.referalEntity.equals("child") && checkEmpty(patient.data.epiNo)) {
                            it.answer = Answer.Text(patient.data.epiNo)
                            it.enableNext = true
                        }
                    }


                    CHILD_GENDER -> {
                        //it.answer = Answer.SingleChoice(patient.data.epiNo)

                        if (patient.data.gender.equals(context.getString(R.string.male), true)) {
                            it.answer = Answer.SingleChoice(R.string.male)
                            it.enableNext = true
                            it.question.nextQuestion = 9
                        } else if (patient.data.gender.equals(
                                context.getString(R.string.female),
                                true
                            )
                        ) {
                            it.answer = Answer.SingleChoice(R.string.female)
                            it.enableNext = true
                            it.question.nextQuestion = 9
                        }

                    }


                }
            }


        }


    }

    private fun checkEmpty(value: String): Boolean {
        return value != null && !value.equals("")
    }


    fun updateFilters(referralType: String) {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {
            latestState.questionsState.forEach {
                when (it.question.key) {

                    REFERRAL_ENTITY_SEARCH -> {
                        it.answer = Answer.Action(SurveyActionResult.Text(referralType))
                        it.enableNext = true
                    }

                    SEARCH_FILTERS -> {
                        it.answer = Answer.Action(SurveyActionResult.Text(referralType))
                        //it.enableNext = true
                    }
                }
            }
        }
    }

    private fun updateStateOfFollowupQuestion(patient: ChildPayload) {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {


            latestState.questionsState.forEach {
                when (it.question.key) {

                    BIODATA -> {
                        it.answer = Answer.Biodata(patient, preference.getUser())
                        it.enableNext = true
                    }
                    REFERRALS -> {
                        it.answer = Answer.Referral(patient.reasons)
                        it.enableNext = true
                    }
                }
            }


        }


    }


    private fun updateStateWithTextResult(questionId: Int, text: String) {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {
            val question =
                latestState.questionsState.first { questionState ->
                    questionState.question.id == questionId
                }
            question.answer = Answer.Text(text)
            question.enableNext = true
        }
    }

    private fun getLatestQuestionId(): Int? {
        val latestState = _uiState.value
        if (latestState != null && latestState is SurveyState.Questions) {
            return latestState.questionsState[latestState.currentQuestionIndex].question.id
        }
        return null
    }


    private fun getBiodataSelectedDate(questionId: Int): Long {
        val latestState = _uiState.value
        var ret = Date().time
        if (latestState != null && latestState is SurveyState.Questions) {
            val question =
                latestState.questionsState.first { questionState ->
                    questionState.question.id == questionId
                }
            val answer: Answer.Biodata = question.answer as Answer.Biodata
            if (answer != null) {

                val mDate = if (answer.basicInfo.dob.equals("")) {
                    getCurrentDate()
                } else {
                    answer.basicInfo.dob
                }

                ret = DateTimeUtils.addOneday(DateTimeUtils.getDate(mDate)).time
            }
        }
        return ret
    }


    private fun getSelectedDate(questionId: Int): Long {
        val latestState = _uiState.value
        var ret = Date().time
        if (latestState != null && latestState is SurveyState.Questions) {
            val question =
                latestState.questionsState.first { questionState ->
                    questionState.question.id == questionId
                }
            val answer: Answer.Action? = question.answer as Answer.Action?
            if (answer != null && answer.result is SurveyActionResult.Date) {
                //val formatter = SimpleDateFormat(simpleDateFormatPattern)
                val mDate =
                    if (answer.result.date.equals("") || answer.result.date.equals("DD-MM-YYYY")) {
                        getCurrentDate()
                    } else {
                        answer.result.date
                    }

                ret = DateTimeUtils.addOneday(DateTimeUtils.getDate(mDate)).time

            } else {
                if (question.question.key == WOMAN_AGE) {
                    ret = DateTimeUtils.addOneday(
                        DateTimeUtils.reduceDatetoTwelveYears(
                            Calendar.getInstance().time
                        )
                    ).time
                } else
                    ret = DateTimeUtils.addOneday(Calendar.getInstance().time).time
            }


        }


        return ret
    }

    private fun getCurrentDate(): String {
        return DateTimeUtils.DateToString(
            DateTimeUtils.getTodaysDateWithoutTime(),
            simpleDateFormatPattern
        )
    }


    var job: Job? = null


    private fun onError(message: String) {

        errorMessage.postValue(message)

    }

    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Something went wrong")
    }

    fun onQRScan(identifier: String, mainRepository: MainRepository, formType: FormType) {

        if (identifier.matches(zmIDRegex)) {

            if (formType == FormType.REFERRAL_SLIP) {

                if (DateTimeUtils.isInternetAvailable(context)) {

                    job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
                        val response = mainRepository.getPatientDetails(identifier)
                        withContext(Dispatchers.Main) {
                            if (response.isSuccessful && response.body()?.status != 0) {

                                updateStateOfEligibleQuestion(response.body()!!)

                                getLatestQuestionId()?.let { questionId ->
                                    updateStateWithActionResult(
                                        questionId,
                                        SurveyActionResult.QRCode(
                                            identifier,
                                            "Details mosool hogayin"
                                        )
                                    )
                                }
                            } else {
                                resetAnswers()
                                getLatestQuestionId()?.let { questionId ->
                                    updateStateWithActionResult(
                                        questionId,
                                        SurveyActionResult.QRCode(
                                            identifier,
                                            "QR code assign kerdiya gaya hai"
                                        )
                                    )
                                } //FIXME Remove me on the error

                                // onError("Error : ${response.message()} ")
                            }
                        }

                    }
                } else {
                    resetAnswers()

                    getLatestQuestionId()?.let { questionId ->
                        updateStateWithActionResult(
                            questionId,
                            SurveyActionResult.QRCode(
                                identifier,
                                "QR code assign kerdiya gaya hai"
                            )
                        )
                    }
                }

            } else {

                job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
                    val response = mainRepository.getFollowupDetails(identifier)
                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful && response.body()!!.identifier != "") {
                            updateStateOfFollowupQuestion(response.body()!!)

                            getLatestQuestionId()?.let { questionId ->
                                updateStateWithActionResult(
                                    questionId,
                                    SurveyActionResult.QRCode(
                                        identifier,
                                        "Details mosool hogayin"
                                    )
                                )
                            }
                        } else {
                            Toast.makeText(context, "No Data found", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }


        }

    }


    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

    fun getValueByKey(surveyQuestions: SurveyState.Questions, key: Keys): String {
        val question =
            surveyQuestions.questionsState.filter { it.question.key == key }.first()
        val answer =
            ((question.answer as Answer.Action).result as SurveyActionResult.QRCode).id
        return answer

    }

    fun uploadForm(
        results: SurveyState.Result,
        mainRepository: MainRepository,
        onStatusChanged: (String) -> Unit
    ) {

        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            if (formType == FormType.REFERRAL_SLIP) {

                if (results.surveyResult.payload.visitOutcome.equals("REFERRED") && results.surveyResult.payload.reasons.isEmpty()) {


                    errorMessage.postValue("Please select atleast one referral Reason")


                } else {
                    val response = mainRepository.submitReferralSlip(results.surveyResult.payload)
                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            _onSubmission.value = results.surveyResult.payload
                            onStatusChanged("success")
                            //computeResult(results.surveyResult.payload, locationDao)
                        } else {
                            onStatusChanged("failure")
                            val error =
                                convertMessage((response.errorBody() as ResponseBody).string())
                            onError("${error.message}")
                            // errorMessage.postValue("Something Went Wrong..")

                        }
                    }
                }

            } else {
                val response = mainRepository.submitFollowup(results.surveyResult.payload)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        _onSubmission.value = results.surveyResult.payload
                        onStatusChanged("success")
                    } else {
                        onStatusChanged("failure")
                        Toast.makeText(context, "Something Went Wrong..", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    fun convertMessage(json: String): ErrorMessage {
        val gson = Gson()
        return gson.fromJson(json, ErrorMessage::class.java)
    }

    fun showFilters(surveyQuestions: SurveyState.Questions) {
        val entityQuestion =
            surveyQuestions.questionsState.filter { it.question.key == REFERRAL_ENTITY_SEARCH }
                .first()
        val entity = context.getString((entityQuestion.answer as Answer.SingleChoice).answer)
            .lowercase()
        _uiState.value = SurveyState.Search(entity)

    }

    fun getSearchResults(
        searchPayload: SearchPayload,
        mainRepository: MainRepository,
        type: String,
        onStatusChanged: (String, List<SearchResults>) -> Unit,
    ) {
        // if (type.equals("child", ignoreCase = true)) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {

            val response = mainRepository.search(type, searchPayload)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {

                    onStatusChanged("success", response.body()!!)
                } else {
                    onStatusChanged("No Data Found", emptyList())
                }
            }
        }

    }

    fun computeSearchResults(searchResult: List<SearchResults>) {
        _uiState.value = SurveyState.SearchResult(searchResult)
    }

    fun getConstraints(questionId: Int): CalendarConstraints {
        val latestState = _uiState.value
        var ret = Date().time
        var endDate = Date().time
        if (latestState != null && latestState is SurveyState.Questions) {
            val question =
                latestState.questionsState.first { questionState ->
                    questionState.question.id == questionId
                }


            if (question.answer is Answer.Biodata) {
                val answer: Answer.Biodata = question.answer as Answer.Biodata
                if (answer != null) {



                    val childDate = if (answer.basicInfo.dob.equals("")) {
                        getCurrentDate()
                    } else {
                        answer.basicInfo.dob
                    }
                    ret = DateTimeUtils.addOneday(DateTimeUtils.getDate(childDate)).time

                    if(!answer.basicInfo.referalEntity.equals("child")){
                        endDate =  DateTimeUtils.addOneday(
                            DateTimeUtils.reduceDatetoTwelveYears(
                                Calendar.getInstance().time
                            )
                        ).time
                    }

                }
            } else {
                val answer: Answer.Action? = question.answer as Answer.Action?

                if (question.question.key == WOMAN_AGE) {
                    endDate = DateTimeUtils.addOneday(
                        DateTimeUtils.reduceDatetoTwelveYears(
                            Calendar.getInstance().time
                        )
                    ).time

                }


                if (answer != null && answer.result is SurveyActionResult.Date) {
                    //val formatter = SimpleDateFormat(simpleDateFormatPattern)
                    val mDate =
                        if (answer.result.date.equals("") || answer.result.date.equals("DD-MM-YYYY")) {
                            getCurrentDate()
                        } else {
                            answer.result.date
                        }



                    ret = DateTimeUtils.addOneday(DateTimeUtils.getDate(mDate)).time
                } else {

                    if (question.question.key == WOMAN_AGE) {
                        ret = DateTimeUtils.addOneday(
                            DateTimeUtils.reduceDatetoTwelveYears(
                                Calendar.getInstance().time
                            )
                        ).time
                        endDate = ret
                    } else
                        ret = DateTimeUtils.addOneday(Calendar.getInstance().time).time

                }
            }
        }

        val upTo = ret

        val constraints = CalendarConstraints.Builder()
            .setOpenAt(upTo)
            .setEnd(endDate)
            .setValidator(DateValidatorPointBackward.now())
            .build()
        return constraints


    }
}

class SurveyViewModelFactory(
    private val photoUriManager: PhotoUriManager,
    private val locationDao: LocationDao,
    private val formType: FormType,
    private val preference: DevicePreference,
    private val context: Context

) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SurveyViewModel::class.java)) {
            return SurveyViewModel(
                context,
                SurveyRepository,
                photoUriManager,
                locationDao,
                formType,
                preference
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
