package com.ird.compose.mref

import android.app.Application
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Handler
import com.ird.compose.mref.di.components.AppComponent
import com.ird.compose.mref.di.components.DaggerAppComponent
import com.ird.compose.mref.di.modules.ApplicationModule
import com.ird.compose.mref.service.NetworkStateReceiver
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

class App : Application() {

    lateinit var appComponent: AppComponent
    private var mReceiver: BroadcastReceiver? = null

    override fun onCreate() {
        super.onCreate()
        AppCenter.start(
            this, "9d0f026c-1790-426d-a9a1-9948831b67b0",
            Analytics::class.java, Crashes::class.java
        )
        appComponent = initDagger(this)

        val handler = Handler()
        handler.postDelayed({
            mReceiver = NetworkStateReceiver()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                registerReceiver(mReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
            }
        }, 5000)

    }

    private fun initDagger(app: App): AppComponent =
        DaggerAppComponent.builder()
            .applicationModule(ApplicationModule(app))
            .build()
}
