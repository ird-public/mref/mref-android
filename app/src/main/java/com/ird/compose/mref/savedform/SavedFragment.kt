package com.ird.compose.mref.savedform

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.ui.platform.ComposeView
import androidx.navigation.Navigation
import androidx.work.Constraints
import com.ird.compose.mref.App
import com.ird.compose.mref.R
import com.ird.compose.mref.db.dao.FormDao
import com.ird.compose.mref.theme.MRefTheme
import javax.inject.Inject
import androidx.work.WorkManager

import androidx.work.OneTimeWorkRequest

import androidx.work.NetworkType
import com.google.accompanist.insets.ProvideWindowInsets
import com.ird.compose.mref.model.DevicePreference
import com.ird.compose.mref.service.UploadWorker


class SavedFragment : Fragment() {

    @Inject
    lateinit var formDao: FormDao

    @Inject
    lateinit var preference: DevicePreference


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity?.application as App).appComponent.inject(this)

        val offlineForms = formDao.getOfflineForms().filter {  form -> form.payload.userId.toInt() == preference.getUser().mappedId}

        return ComposeView(requireContext()).apply {
            setContent {
                MRefTheme {
                    ProvideWindowInsets(
                        //  consumeWindowInsets = false,
                        windowInsetsAnimationsEnabled = true
                    ) {
                        SavedForm(offlineForms,
                            onUpload = {
                                val builder: Constraints.Builder =
                                    Constraints.Builder()
                                        .setRequiredNetworkType(NetworkType.CONNECTED)


                                val syncWorkRequest =
                                    OneTimeWorkRequest.Builder(UploadWorker::class.java)
                                        .addTag("Sync")
                                        .setConstraints(builder.build())
                                        .build()

                                WorkManager.getInstance(requireContext()).enqueue(syncWorkRequest)
                                Toast.makeText(
                                    requireContext(),
                                    "Synchronizing...",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                Navigation.findNavController(requireView()).popBackStack()

                            },
                            onBackPressed = {
                                Navigation.findNavController(requireView()).popBackStack()
                            }
                        )
                    }
                }
            }
        }
    }

}