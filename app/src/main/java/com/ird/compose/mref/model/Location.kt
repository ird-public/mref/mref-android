package com.ird.compose.mref.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.ird.compose.mref.db.Converter

@Entity
@TypeConverters(Converter::class)
data class Location(
    @PrimaryKey
    @SerializedName("locationId") val locationId: Int,
    @SerializedName("name") val name: String?,
//    @SerializedName("shortName") val shortName : String?,
//    @SerializedName("fullName") val fullName : String?,
    @SerializedName("parentLocation") val parentLocation: Int?,
//    @SerializedName("description") val description : String?,
//    @SerializedName("otherIdentifier") val otherIdentifier : Int?,
    @SerializedName("locationType") val locationType: Int?,
//    @SerializedName("latitude") val latitude : String?,
//    @SerializedName("longitude") val longitude : String?,
//    @SerializedName("voided") val voided : Int
    @SerializedName("hierarchy")
    var hierarchy: String? = ""
) {

}