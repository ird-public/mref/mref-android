package com.ird.compose.mref.util

object Constants {
    const val TODAY_DATE = "todayDate"
    const val DELETE_KEY = "deleteKey"
    const val CHECK_IN_EVENT = "checkInEvent"
    const val CHECK_OUT_EVENT = "checkOutEvent"
}