package com.ird.compose.mref.util

object Regex {

    val zmIDRegex = Regex("^[1-5]{1}([0-9]{13})$")
    val NUMERIC = Regex("[0-9]+")
    val REPETITIVE = Regex("(.)\\1+")
    val CNIC = Regex("\\d{13}")
    val PHONE = Regex("03[0-9]{9}")
    val EPINUMBER = Regex("^20([0-9]{6})$")

}