/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.signinsignup


import com.ird.compose.mref.model.MRefMetadata
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.internal.LinkedTreeMap
import com.ird.compose.mref.Screen
import com.ird.compose.mref.home.AttendanceUIStatus
import com.ird.compose.mref.model.ErrorMessage
import com.ird.compose.mref.model.StatisticsResponse

import com.ird.compose.mref.model.User
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.util.Event
import kotlinx.coroutines.*
import okhttp3.ResponseBody


class WelcomeViewModel(private val mainRepository: MainRepository) : ViewModel() {

    private val _navigateTo = MutableLiveData<Event<Screen>>()
    val navigateTo: LiveData<Event<Screen>> = _navigateTo
    val onLocationRecieved = MutableLiveData<MRefMetadata>()
    val onStatsRecieved = MutableLiveData<StatisticsResponse>()
    val onSignIn = MutableLiveData<User>()


    val loading = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()
    val isError = MutableLiveData<Boolean>()
    val attendanceStatus = MutableLiveData<AttendanceUIStatus>()
    var job: Job? = null

    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        //onError("Exception handled: ${throwable.localizedMessage}")
    }

    fun getLocations() {
        val syncExceptional = CoroutineExceptionHandler { _, throwable ->
            isError.postValue(true)
            onError("Sync failed")
        }

        job = CoroutineScope(Dispatchers.IO + syncExceptional).launch {
            val response = mainRepository.getLocations()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    onLocationRecieved.postValue(response.body())
                    loading.postValue(false)
                } else {
                    val error = convertMessage((response.errorBody() as ResponseBody).string())
                    onError("${error.message}")
                    isError.postValue(true)


                }
            }
        }
    }


    fun getStatistics(userId: Int) {
        val statsExceptional = CoroutineExceptionHandler { _, throwable ->
            // isError.postValue(true)

        }

        job = CoroutineScope(Dispatchers.IO + statsExceptional).launch {
            val response = mainRepository.getStatistics(userId)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    onStatsRecieved.postValue(response.body())
                    // loading.postValue(false)
                } else {
                    //isError.postValue(true)
                    //onError("Error : ${response.message()} ")

                }
            }
        }
    }


    fun signInUser(username: String, password: String) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.getUser(username, password)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    onSignIn.postValue(response.body())
                    //loading.postValue(false)
                } else {
                    val error = convertMessage((response.errorBody() as ResponseBody).string())
                    onError("${error.message}")
                }
            }
        }
    }

    fun convertMessage(json : String): ErrorMessage {
        val gson = Gson()
        return gson.fromJson(json, ErrorMessage::class.java)
    }


    fun updateAttendanceStatus(attendanceUIStatus: AttendanceUIStatus) {
        attendanceStatus.postValue(attendanceUIStatus)
    }

    fun NavigateReferralScreen() {
        _navigateTo.value = Event(Screen.Survey)
    }


    fun NavigateHomeScreen() {
        _navigateTo.value = Event(Screen.Home)
    }

    private fun onError(message: String) {

        errorMessage.postValue(message)
        loading.postValue(false)

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}

class ViewModelFactory(val mainRepository: MainRepository) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WelcomeViewModel::class.java)) {
            return WelcomeViewModel(mainRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
