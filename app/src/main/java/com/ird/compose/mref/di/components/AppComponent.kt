package com.ird.compose.mref.di.components

import com.ird.compose.mref.App
import com.ird.compose.mref.di.modules.*
import com.ird.compose.mref.home.HomeFragment
import com.ird.compose.mref.savedform.SavedFragment
import com.ird.compose.mref.service.AttendanceService
import com.ird.compose.mref.service.NetworkStateReceiver
import com.ird.compose.mref.service.UploadWorker
import com.ird.compose.mref.signinsignup.WelcomeFragment
import com.ird.compose.mref.survey.SurveyFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class,NetworkModule::class, DatabaseModule::class, RepositoryModule::class, LocationModule::class ])
interface AppComponent {
    fun inject(app: App)
    fun inject(welcomeFragment: WelcomeFragment)
    fun inject(homeFragment: HomeFragment)
    fun inject(surveyFragment: SurveyFragment)
    fun inject(savedFragment: SavedFragment)
    fun inject(uploadWorker: UploadWorker)
    fun inject(attendanceService: AttendanceService)
    fun inject(networkStateReceiver: NetworkStateReceiver) {

    }
}