package com.ird.compose.mref.model;

public abstract class BasePayload {
    public abstract Object getPayload();

    protected abstract void setPayload(Object payload);
}
