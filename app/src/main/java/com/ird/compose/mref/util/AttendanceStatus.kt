package com.ird.compose.mref.util

interface AttendanceStatus {

    fun checkInStatus()
    fun checkOutStatus()
    fun noStatus()
}