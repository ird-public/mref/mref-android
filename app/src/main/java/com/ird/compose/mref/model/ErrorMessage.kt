package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class ErrorMessage (
    @SerializedName("message" ) var message : String = "",
    @SerializedName("details" ) var details : String = ""
)