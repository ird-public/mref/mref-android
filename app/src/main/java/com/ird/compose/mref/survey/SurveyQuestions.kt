/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.survey

import androidx.annotation.StringRes
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.annotation.ExperimentalCoilApi
import com.ird.compose.mref.R
import com.ird.compose.mref.theme.MRefTheme
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.ird.compose.mref.model.*
import com.ird.compose.mref.util.Regex
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun Question(
    question: Question,
    answer: Answer<*>?,
    shouldAskPermissions: Boolean,
    onAnswer: (Answer<*>) -> Unit,
    onAction: (Int, SurveyActionType) -> Unit,
    onInvalidAnswer: () -> Unit,
    onDoNotAskForPermissions: () -> Unit,
    openSettings: () -> Unit,
    modifier: Modifier = Modifier
) {
    if (question.permissionsRequired.isEmpty()) {
        QuestionContent(question, answer, onAnswer, onInvalidAnswer, onAction, modifier)
    } else {
        val permissionsContentModifier = modifier.padding(horizontal = 20.dp)

        val multiplePermissionsState =
            rememberMultiplePermissionsState(question.permissionsRequired)

        when {
            // If all permissions are granted, then show the question
            multiplePermissionsState.allPermissionsGranted -> {
                QuestionContent(question, answer, onAnswer, onInvalidAnswer, onAction, modifier)
            }
            // If user denied some permissions but a rationale should be shown or the user
            // is going to be presented with the permission for the first time. Let's explain
            // why we need the permission
            multiplePermissionsState.shouldShowRationale ||
                    !multiplePermissionsState.permissionRequested -> {
                if (!shouldAskPermissions) {
                    PermissionsDenied(
                        question.questionText,
                        openSettings,
                        permissionsContentModifier
                    )
                } else {
                    PermissionsRationale(
                        question,
                        multiplePermissionsState,
                        onDoNotAskForPermissions,
                        permissionsContentModifier
                    )
                }
            }
            // If the criteria above hasn't been met, the user denied some permission.
            else -> {
                PermissionsDenied(question.questionText, openSettings, permissionsContentModifier)
                // Trigger side-effect to not ask for permissions
                LaunchedEffect(true) {
                    onDoNotAskForPermissions()
                }
            }
        }

        // If permissions are denied, inform the caller that can move to the next question
        if (!shouldAskPermissions) {
            LaunchedEffect(true) {
                onAnswer(Answer.PermissionsDenied)
            }
        }
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
private fun PermissionsRationale(
    question: Question,
    multiplePermissionsState: MultiplePermissionsState,
    onDoNotAskForPermissions: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier) {
        Spacer(modifier = Modifier.height(32.dp))
        QuestionTitle(question.questionText)
        Spacer(modifier = Modifier.height(32.dp))
        val rationaleId =
            question.permissionsRationaleText ?: R.string.permissions_rationale
        Text(stringResource(id = rationaleId))
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedButton(
            onClick = {
                multiplePermissionsState.launchMultiplePermissionRequest()
            }
        ) {
            Text(stringResource(R.string.request_permissions))
        }
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedButton(onClick = onDoNotAskForPermissions) {
            Text(stringResource(R.string.do_not_ask_permissions))
        }
    }
}

@Composable
private fun PermissionsDenied(
    @StringRes title: Int,
    openSettings: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier) {
        Spacer(modifier = Modifier.height(32.dp))
        QuestionTitle(title)
        Spacer(modifier = Modifier.height(32.dp))
        Text(stringResource(R.string.permissions_denied))
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedButton(onClick = openSettings) {
            Text(stringResource(R.string.open_settings))
        }
    }
}

@Composable
private fun QuestionContent(
    question: Question,
    answer: Answer<*>?,
    onAnswer: (Answer<*>) -> Unit,
    onInvalidAnswer: () -> Unit,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.padding(horizontal = 20.dp, vertical = 0.dp),
        // contentPadding = PaddingValues(start = 20.dp, end = 20.dp)
    ) {

        Spacer(modifier = Modifier.height(16.dp))
        QuestionTitle(question.questionText)
        Spacer(modifier = Modifier.height(16.dp))
        if (question.description != null && question.description != R.string.empty_hint) {
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                Text(
                    text = stringResource(id = question.description),
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 18.dp, start = 8.dp, end = 8.dp)
                )
            }
        }
        when (question.answer) {
            is PossibleAnswer.SingleChoice -> SingleChoiceQuestion(
                possibleAnswer = question.answer,
                answer = answer as Answer.SingleChoice?,
                onAnswerSelected = { answer, nextQuestion ->
                    onAnswer(Answer.SingleChoice(answer))
                    question.nextQuestion = nextQuestion
                },
                modifier = Modifier.fillMaxWidth()
            )
            is PossibleAnswer.SelectLocation -> SelectListQuestion(
                possibleAnswer = question.answer,
                answer = if (answer is Answer.SelectLoction) answer as Answer.SelectLoction? else null,
                onAnswerSelected = { answer, text ->
                    onAnswer(Answer.SelectLoction(answer, text))
                },
                modifier = Modifier
                    .fillMaxWidth(),
                onInvalidAnswer = onInvalidAnswer

            )

            is PossibleAnswer.BioData -> BiodataQuestion(
                questionId = question.id,
                answer = answer as Answer.Biodata?,
                modifier = Modifier
                    .fillMaxWidth(),
                onAction = onAction,
            )

            is PossibleAnswer.Referral -> ReferralReasonsQuestion(
                answer = answer as Answer.Referral?,
                modifier = Modifier
                    .fillMaxWidth()
            )


            is PossibleAnswer.SingleChoiceIcon -> SingleChoiceIconQuestion(
                possibleAnswer = question.answer,
                answer = answer as Answer.SingleChoice?,
                onAnswerSelected = { answer, nextQuestionId ->
                    onAnswer(Answer.SingleChoice(answer))
                    question.nextQuestion = nextQuestionId
                },
                modifier = Modifier.fillMaxWidth()
            )
            is PossibleAnswer.TextChoice -> TextQuestion(
                questionId = question.id,
                fieldType = question.answer.inputType,
                validation = question.answer.validations,
                isOptional = question.answer.isOptional,
                placeholder = question.answer.placeholder,
                answer = answer as Answer.Text?,
                onInvalidAnswer = onInvalidAnswer,
                onAnswer = { answer -> onAnswer(Answer.Text(answer)) },
                modifier = Modifier.fillMaxWidth()
            )
            is PossibleAnswer.MultipleChoice -> MultipleChoiceQuestion(
                possibleAnswer = question.answer,
                answer = answer as Answer.MultipleChoice?,
                onAnswerSelected = { newAnswer, selected ->
                    if (answer == null) {
                        onAnswer(Answer.MultipleChoice(setOf(newAnswer)))
                    } else {
                        onAnswer(answer.withAnswerSelected(newAnswer, selected))
                    }
                },
                modifier = Modifier.fillMaxWidth()
            )

            is PossibleAnswer.MultipleChoiceReferral -> ReferralChoiceQuestion(
                possibleAnswer = question.answer,
                answer = answer as Answer.MultipleChoiceReferral?,
                onAnswerSelected = { newAnswer, selected ->
                    if (answer == null) {
                        onAnswer(Answer.MultipleChoiceReferral(setOf(newAnswer)))
                    } else {
                        onAnswer(answer.withReferralAnswerSelected(newAnswer, selected))
                    }
                },
                modifier = Modifier.fillMaxWidth()
            )
            is PossibleAnswer.MultipleChoiceIcon -> MultipleChoiceIconQuestion(
                possibleAnswer = question.answer,
                answer = answer as Answer.MultipleChoice?,
                onAnswerSelected = { newAnswer, selected ->
                    // create the answer if it doesn't exist or
                    // update it based on the user's selection
                    if (answer == null) {
                        onAnswer(Answer.MultipleChoice(setOf(newAnswer)))
                    } else {
                        onAnswer(answer.withAnswerSelected(newAnswer, selected))
                    }
                },
                modifier = Modifier.fillMaxWidth()
            )
            is PossibleAnswer.Action -> ActionQuestion(
                questionId = question.id,
                possibleAnswer = question.answer,
                answer = answer as Answer.Action?,
                onAction = onAction,
                onInvalidAnswer = onInvalidAnswer,
                modifier = Modifier.fillMaxWidth()
            )
            is PossibleAnswer.Slider -> SliderQuestion(
                possibleAnswer = question.answer,
                answer = answer as Answer.Slider?,
                onAnswerSelected = { onAnswer(Answer.Slider(it)) },
                modifier = Modifier.fillMaxWidth()
            )
        }

    }
}


@Composable
private fun QuestionTitle(@StringRes title: Int) {
    val backgroundColor = if (MaterialTheme.colors.isLight) {
        MaterialTheme.colors.onSurface.copy(alpha = 0.04f)
    } else {
        MaterialTheme.colors.onSurface.copy(alpha = 0.06f)
    }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                color = backgroundColor,
                shape = MaterialTheme.shapes.small
            )
    ) {
        Text(
            text = stringResource(id = title),
            style = MaterialTheme.typography.subtitle1,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 20.dp, horizontal = 16.dp)
        )
    }
}

@Composable
private fun SingleChoiceQuestion(
    possibleAnswer: PossibleAnswer.SingleChoice,
    answer: Answer.SingleChoice?,
    onAnswerSelected: (Int, Int) -> Unit,
    modifier: Modifier = Modifier
) {
    val options = possibleAnswer.optionsStringRes.associateBy {
        stringResource(id = it.first)
    }

    val radioOptions = options.keys.toList()

    val selected = if (answer != null) {
        stringResource(id = answer.answer)
    } else {
        null
    }

    val (selectedOption, onOptionSelected) = remember(answer) { mutableStateOf(selected) }

    Column(modifier = modifier) {
        radioOptions.forEach { text ->
            val onClickHandle = {
                onOptionSelected(text)
                options[text]?.let { onAnswerSelected(it.first, it.second) }
                Unit
            }
            val optionSelected = text == selectedOption

            val answerBorderColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.5f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
            }
            val answerBackgroundColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.12f)
            } else {
                MaterialTheme.colors.background
            }
            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier.padding(vertical = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = optionSelected,
                            onClick = onClickHandle
                        )
                        .background(answerBackgroundColor)
                        .padding(vertical = 16.dp, horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = text
                    )

                    RadioButton(
                        selected = optionSelected,
                        onClick = onClickHandle,
                        colors = RadioButtonDefaults.colors(
                            selectedColor = MaterialTheme.colors.primary
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun SearchView(state: MutableState<TextFieldValue>) {


    Surface(
        shape = MaterialTheme.shapes.small,
        border = BorderStroke(
            width = 1.dp,
            color = MaterialTheme.colors.primary.copy(alpha = 0.5f)
        ),
        modifier = Modifier.padding(vertical = 8.dp)
    ) {

        TextField(
            value = state.value,
            onValueChange = { value ->
                state.value = value
            },
            modifier = Modifier
                .fillMaxWidth(),
            textStyle = TextStyle(color = Color.Black, fontSize = 18.sp),
            leadingIcon = {
                Icon(
                    Icons.Default.Search,
                    contentDescription = "",
                    modifier = Modifier
                        .padding(15.dp)
                        .size(24.dp)
                )
            },
            trailingIcon = {
                if (state.value != TextFieldValue("")) {
                    IconButton(
                        onClick = {
                            state.value =
                                TextFieldValue("") // Remove text from TextField when you press the 'X' icon
                        }
                    ) {
                        Icon(
                            Icons.Default.Close,
                            contentDescription = "",
                            modifier = Modifier
                                .padding(15.dp)
                                .size(24.dp)
                        )
                    }
                }
            },
            singleLine = true,
            shape = RectangleShape, // The TextFiled has rounded corners top left and right by default
            colors = TextFieldDefaults.textFieldColors(
                textColor = colorResource(id = R.color.matt_black),
                cursorColor = colorResource(id = R.color.matt_black),
                leadingIconColor = colorResource(id = R.color.matt_black),
                trailingIconColor = colorResource(id = R.color.matt_black),
                backgroundColor = colorResource(id = R.color.white),
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            )
        )
    }
}


@Composable
fun SelectListQuestion(
    possibleAnswer: PossibleAnswer.SelectLocation,
    answer: Answer.SelectLoction?,
    onAnswerSelected: (Location, String) -> Unit,
    onInvalidAnswer: () -> Unit,
    modifier: Modifier
) {


    var selected = if (answer != null) {
        answer
    } else {
        null
    }

    var selectedOption by remember(answer) { mutableStateOf(selected) }

    val textState = remember { mutableStateOf(TextFieldValue("")) }


    if (selectedOption == null)
        SearchView(textState)

    var searchedText = textState.value.text
    if (!selectedOption?.searchText.equals("") && selectedOption != null) {
        searchedText = selectedOption!!.searchText
    }


    val locations = if (searchedText.isEmpty()) {
        possibleAnswer.locations
    } else {
        val resultList = ArrayList<Location>()
        for (location in possibleAnswer.locations) {
            if (location.name?.lowercase(Locale.getDefault())
                !!.contains(searchedText.lowercase(Locale.getDefault()))
                || location.hierarchy
                    ?.lowercase(Locale.getDefault())
                    ?.contains(searchedText.lowercase(Locale.getDefault()))!!
            ) {
                resultList.add(location)
            }
        }
        resultList
    }


    LazyColumn(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(4.dp),
    ) {


        items(locations) { location ->

            val onClickHandle = {
                searchedText = location.name!!
                selectedOption = Answer.SelectLoction(location, location.name)
                onAnswerSelected(location, searchedText)
            }

            var optionSelected = location == selectedOption?.location

            val onCloseHandle = {
                selectedOption = null
                onInvalidAnswer()

            }


            val answerBorderColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.5f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
            }
            val answerBackgroundColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.12f)
            } else {
                MaterialTheme.colors.background
            }


            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier.padding(vertical = 8.dp)
            ) {

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = optionSelected,
                            onClick = onClickHandle
                        )
                        .background(answerBackgroundColor)
                        .padding(vertical = 16.dp, horizontal = 16.dp),

                ) {
                    Column(
                        modifier = Modifier.weight(weight = 0.8f,true),
                        horizontalAlignment = Alignment.Start,
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(location.name!!)

                        Text(
                            if (location.hierarchy != null) location.hierarchy!! else "",
                            style = MaterialTheme.typography.caption,
                            color = colorResource(id = R.color.grey_dark)
                        )

                    }


                    if(selectedOption !=null) {

                        IconButton(onClick = onCloseHandle) {
                            Icon(
                                Icons.Default.Close,
                                contentDescription = "",
                                modifier = Modifier
                                    .padding(15.dp)
                                    .size(24.dp),
                            )
                        }
                    }
                }
            }
        }
    }

}


@Composable
fun ReferralReasonsQuestion(
    answer: Answer.Referral?,
    modifier: Modifier
) {

    val reasons = if (answer != null) {
        answer.reasons
    } else {
        arrayListOf(
            Reasons(
                stringResource(id = R.string.option12),
                "",
                "",
                "Open",
                "12-2-2012",
                "",
                -1,
                ""
            ),
        )
    }



    LazyColumn(verticalArrangement = Arrangement.spacedBy(4.dp))
    {
        items(reasons) { item: Reasons ->

            var checked by remember { mutableStateOf(item.status.equals("Closed", true)) }

            //checked = true

            val backgroundColor by animateColorAsState(
                if (checked) colorResource(R.color.green) else colorResource(id = R.color.red_image)
            )

            val headingColor by animateColorAsState(
                if (checked) colorResource(R.color.matt_black) else colorResource(id = R.color.white)
            )

            val subHeadingColor by animateColorAsState(
                if (checked) colorResource(R.color.grey_dark) else colorResource(id = R.color.grey)
            )




            Card(
                modifier = modifier
                    .fillMaxWidth()
                    .padding(5.dp),
                contentColor = colorResource(id = R.color.white),
                backgroundColor = backgroundColor,
                shape = RoundedCornerShape(15.dp),
                content = {
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .padding(10.dp),


                        ) {


                        Column(modifier = Modifier.weight(0.6f)) {
                            Text(
                                text = item.displayText,
                                modifier = Modifier
                                    .fillMaxWidth(),
                                color = headingColor,
                                textAlign = TextAlign.Start,
                                style = MaterialTheme.typography.body1
                            )


                            Text(
                                text = "Status : ${item.status}",
                                modifier = Modifier
                                    .fillMaxWidth(),
                                color = subHeadingColor,
                                textAlign = TextAlign.Justify,
                                style = MaterialTheme.typography.body2
                            )

                            /*         Text(
                                         text = "Referred To : ${item.referedTo}",
                                         modifier = Modifier
                                             .fillMaxWidth(),
                                         color = colorResource(id = R.color.matt_black),
                                         textAlign = TextAlign.Justify,
                                         style = MaterialTheme.typography.body2
                                     )*/
                            Text(
                                text = "Refer ki tareekh : ${item.dateRefered}",
                                modifier = Modifier
                                    .fillMaxWidth(),
                                color = subHeadingColor,
                                textAlign = TextAlign.Justify,
                                style = MaterialTheme.typography.body2
                            )


                        }

                        if (item.dateClosed.equals("")) {
                            IconToggleButton(
                                modifier = Modifier
                                    .fillMaxHeight()
                                    .weight(0.1f)
                                    .align(Alignment.CenterVertically),

                                checked = checked,
                                onCheckedChange = {
                                    checked = it
                                    item.status = if (checked) "CLOSED" else "ACTIVE"
                                }) {
                                val tint by animateColorAsState(
                                    if (checked) colorResource(R.color.blue) else Color(
                                        0xFFB0BEC5
                                    )
                                )
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_checked),
                                    contentDescription = "Localized description",
                                    tint = tint
                                )
                            }
                        }

                    }

                }

            )


        }
    }


}


@Composable
fun BiodataQuestion(
    questionId: Int,
    answer: Answer.Biodata?,
    modifier: Modifier,
    onAction: (Int, SurveyActionType) -> Unit
) {

    val answerValue: ChildPayload = if (answer != null) {
        answer.basicInfo
    } else {
        ChildPayload()
    }

    val userValue: User = if (answer != null) {
        answer.user
    } else {
        User("", -1, "", "")
    }

    var name by remember(answer) { mutableStateOf(answerValue.name) }
    var username by remember(answer) { mutableStateOf(userValue.name) }
    var fatherName by remember(answer) { mutableStateOf(answerValue.fatherName) }
    var husbandName by remember(answer) { mutableStateOf(answerValue.husbandName) }
    var address by remember(answer) { mutableStateOf(answerValue.address) }
    var epiNo by remember(answer) { mutableStateOf(answerValue.epiNo) }
    val selectedGender = remember { mutableStateOf(answerValue.gender) }
    val town = remember { mutableStateOf(answerValue.town) }
    val uc = remember { mutableStateOf(answerValue.uc) }
    val district = remember { mutableStateOf(answerValue.district) }
    val dob = remember { mutableStateOf(answerValue.dob) }

    dob.value = answerValue.dob

    val backgroundColor = if (MaterialTheme.colors.isLight) {
        MaterialTheme.colors.primary.copy(alpha = 0.04f)
    } else {
        MaterialTheme.colors.primary.copy(alpha = 0.06f)
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
    ) {

        Text(
            text = "Health Worker Name",
            style = MaterialTheme.typography.caption,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 10.dp, horizontal = 0.dp)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = backgroundColor,
                    shape = MaterialTheme.shapes.small
                )
        ) {
            Text(
                text = if (username != null) username else "",
                style = MaterialTheme.typography.caption,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp)
            )
        }


        Text(
            text = "District",
            style = MaterialTheme.typography.caption,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 10.dp, horizontal = 0.dp)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = backgroundColor,
                    shape = MaterialTheme.shapes.small
                )
        ) {
            Text(
                text = district.value,
                style = MaterialTheme.typography.caption,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp)
            )
        }

        Text(
            text = "Town",
            style = MaterialTheme.typography.caption,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 10.dp, horizontal = 0.dp)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = backgroundColor,
                    shape = MaterialTheme.shapes.small
                )
        ) {
            Text(
                text = town.value,
                style = MaterialTheme.typography.caption,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp)
            )
        }


        Text(
            text = "Union Council",
            style = MaterialTheme.typography.caption,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 10.dp, horizontal = 0.dp)
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = backgroundColor,
                    shape = MaterialTheme.shapes.small
                )
        ) {
            Text(
                text = uc.value,
                style = MaterialTheme.typography.caption,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp)
            )
        }

        OutlinedTextField(
            value = name,

            onValueChange = {
                name = it
                answerValue.name = it
            },
            label = { Text(text = stringResource(id = R.string.name)) },
            maxLines = 1,
            textStyle = MaterialTheme.typography.caption,
            modifier = modifier

                .padding(vertical = 5.dp)
                .height(54.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done,
                keyboardType = KeyboardType.Text
            )
        )

        if (answerValue.referalEntity != "child") {

            OutlinedTextField(
                value = husbandName,

                onValueChange = {
                    fatherName = it
                    answerValue.husbandName = it
                },
                label = { Text(text = stringResource(id = R.string.father_husband_name)) },
                maxLines = 1,
                textStyle = MaterialTheme.typography.caption,
                modifier = modifier
                    .padding(vertical = 5.dp)
                    .height(54.dp),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Text
                )
            )
        } else {
            OutlinedTextField(
                value = fatherName,

                onValueChange = {
                    fatherName = it
                    answerValue.fatherName = it
                },
                label = { Text(text = stringResource(id = R.string.father_name)) },
                maxLines = 1,
                textStyle = MaterialTheme.typography.caption,
                modifier = modifier
                    .padding(vertical = 5.dp)
                    .height(54.dp),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Text
                )
            )
        }

        Text(
            text = "Date of birth",
            style = MaterialTheme.typography.caption,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 10.dp, horizontal = 0.dp)
                .align(Alignment.End)
        )

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {


            Surface(
                //onClick = { onAction(questionId, SurveyActionType.PICK_DATE) },

                shape = MaterialTheme.shapes.small,
                modifier = modifier
                    .height(54.dp)
                    .weight(0.6f),
                border = BorderStroke(1.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.12f))

            ) {
                Text(
                    text = dob.value,
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight(align = Alignment.CenterVertically)
                        .wrapContentWidth(align = Alignment.CenterHorizontally)

                )

            }

            androidx.compose.foundation.layout.Spacer(
                modifier = androidx.compose.ui.Modifier.padding(
                    horizontal = 10.dp
                )
            )

            IconButton(
                modifier = Modifier.weight(0.2f),
                onClick = { onAction(questionId, SurveyActionType.PICK_DATE()) },
            ) {
                Column() {


                    Icon(
                        painter = painterResource(id = R.drawable.ic_calendar),
                        contentDescription = null,
                        modifier = Modifier
                            .height(25.dp)
                            .width(25.dp)
                            .align(Alignment.CenterHorizontally)

                    )
                    Text(
                        "Calendar",
                        style = MaterialTheme.typography.caption,
                        textAlign = TextAlign.Center
                    )

                }
            }

            IconButton(
                modifier = Modifier.weight(0.2f),
                onClick = {

                    if (answerValue.referalEntity == "child")
                        onAction(questionId, SurveyActionType.AGE_PICKER_CHILD())
                    else onAction(questionId, SurveyActionType.AGE_PICKER())
                },
            ) {
                Column() {

                    Icon(
                        painter = painterResource(id = R.drawable.ic_calculator),
                        contentDescription = null,
                        modifier = Modifier
                            .height(25.dp)
                            .width(25.dp)
                            .align(Alignment.CenterHorizontally)
                    )

                    Text(
                        "Age",
                        // modifier = modifier.weight(0.5f),
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.caption
                    )


                }
            }

        }

        if (answerValue.referalEntity == "child") {
            Spacer(modifier = Modifier.size(16.dp))
            Text(
                "Jins",
                style = MaterialTheme.typography.caption,
            )
            Spacer(modifier = Modifier.size(12.dp))
            Row(modifier = Modifier.height(32.dp)) {
                RadioButton(
                    selected = selectedGender.value == "MALE",
                    onClick = {
                        selectedGender.value = "MALE"
                        answerValue.gender = "MALE"
                    },
                    colors = RadioButtonDefaults.colors(colorResource(id = R.color.blue)),
                    modifier = Modifier.align(alignment = Alignment.CenterVertically)
                )
                // Spacer(modifier = Modifier.width(16.dp))
                Text(
                    stringResource(id = R.string.male),
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier.align(alignment = Alignment.CenterVertically),
                    textAlign = TextAlign.Center
                )
                Spacer(modifier = Modifier.width(16.dp))
                RadioButton(
                    selected = selectedGender.value == "FEMALE",
                    onClick = {
                        selectedGender.value = "FEMALE"
                        answerValue.gender = "FEMALE"
                    },
                    colors = RadioButtonDefaults.colors(colorResource(id = R.color.blue)),
                    modifier = Modifier.align(alignment = Alignment.CenterVertically)
                )
                // Spacer(modifier = Modifier.width(16.dp))
                Text(
                    stringResource(id = R.string.female),
                    textAlign = TextAlign.Center,
                    modifier = Modifier.align(alignment = Alignment.CenterVertically),
                    style = MaterialTheme.typography.caption,
                )
            }
        }

        OutlinedTextField(
            value = epiNo,
            onValueChange = {
                epiNo = it
                answerValue.epiNo = it
            },
            label = { Text(text = stringResource(id = R.string.epi)) },
            maxLines = 1,
            modifier = modifier
                .padding(vertical = 5.dp)
                .height(54.dp),

            textStyle = MaterialTheme.typography.caption,
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done,
                keyboardType = KeyboardType.Text
            )
        )



        OutlinedTextField(
            value = address,
            onValueChange = {
                address = it
                answerValue.address = it
            },
            label = { Text(text = stringResource(id = R.string.address)) },
            maxLines = 1,
            modifier = modifier
                .padding(vertical = 5.dp)
                .height(54.dp),
            textStyle = MaterialTheme.typography.caption,
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done,
                keyboardType = KeyboardType.Text
            )
        )

    }


}

@Composable
private fun TextQuestion(
    questionId: Int,
    fieldType: TextFieldType,
    validation: Validations,
    isOptional: Boolean,
    placeholder: Int,
    answer: Answer.Text?,
    onInvalidAnswer: () -> Unit,
    onAnswer: (String) -> Unit,
    modifier: Modifier = Modifier,
) {

    val answerValue: String = if (answer != null) {
        answer.answerValue
    } else {
        ""
    }

    var value by remember(answer) { mutableStateOf(answerValue) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }


    if (isOptional) {
        if (value.equals("")) {
            onAnswer("")
        } else {
            if (validateField(validation = validation, value).isError) {
                isError = false
            } else {
                onInvalidAnswer()
                isError = true
            }
        }
    }

    val keyboardOptions: KeyboardOptions = when (fieldType) {
        TextFieldType.NUMERIC -> KeyboardOptions.Default.copy(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Number
        )
        TextFieldType.TEXT -> KeyboardOptions.Default.copy(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Text
        )


        else -> KeyboardOptions.Default.copy(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Text
        )
    }



    OutlinedTextField(

        value = value,

        onValueChange = {


            //onChange(questionId, value)
            val validationResult = validateField(validation, it)
            if ((it.equals("") && isOptional) || validationResult.isError) {
                isError = false
                onAnswer(it)
            } else {
                onAnswer(it)
                onInvalidAnswer()
                errorMessage = validationResult.errorMessage
                isError = true

            }
            value = it
        },

        placeholder = {
            Text(
                text = stringResource(id = placeholder),
                style = TextStyle(
                    color = colorResource(id = R.color.grey),
                    textAlign = TextAlign.Center
                )
            )
        },

        isError = isError,
        //label = { Text(questionText) },
        maxLines = 1,
        modifier = modifier
            .padding(vertical = 30.dp)
            .height(54.dp),
        keyboardOptions = keyboardOptions
        /*keyboardActions = KeyboardActions(
            onDone = {
                onDone(value)
            }
        )*/

    )

    if (isError) {
        Text(
            text = errorMessage,
            color = MaterialTheme.colors.error,
            style = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp)
        )
    }

}

data class ValidateResult(var isError: Boolean = false, var errorMessage: String = "")


fun validateField(validation: Validations, value: String): ValidateResult {

    when (validation) {
        Validations.CNIC -> {
            return ValidateResult(value.matches(Regex.CNIC), "13 hindso ka CNIC darj kerein")
        }
        Validations.EPINUMBER -> {
            return ValidateResult(
                value.matches(Regex.EPINUMBER),
                "EPI No. 20 sey shuru hona chahiye or 8 digit ka hona chahiye"
            )
        }
        Validations.PHONE -> {
            return ValidateResult(value.matches(Regex.PHONE), "Mobile number durust nahi he")
        }
        Validations.IDENTIFIER -> {
            return ValidateResult(
                value.matches(Regex.zmIDRegex),
                "ID 14 digits ki honi chahiye. Pehla number 1 sy lykar 5 tak ka ho sakta hy"
            )
        }
        Validations.EMPTY -> {
            return ValidateResult(true, "")
        }
        else -> {
            return ValidateResult(true, "")
        }
    }
}

@Composable
private fun SingleChoiceIconQuestion(
    possibleAnswer: PossibleAnswer.SingleChoiceIcon,
    answer: Answer.SingleChoice?,
    onAnswerSelected: (Int, Int) -> Unit,
    modifier: Modifier = Modifier
) {
    val options =
        possibleAnswer.optionsStringIconRes.associateBy { stringResource(id = it.second.optionText) }

    val radioOptions = options.keys.toList()

    val selected = if (answer != null) {
        stringResource(id = answer.answer)
    } else {
        null
    }

    val (selectedOption, onOptionSelected) = remember(answer) { mutableStateOf(selected) }

    Column(modifier = modifier) {
        radioOptions.forEach { text ->
            val onClickHandle = {
                onOptionSelected(text)
                options[text]?.let {
                    onAnswerSelected(
                        it.second.optionText,
                        it.second.nextQuestionId
                    )
                }
                Unit
            }
            val optionSelected = text == selectedOption
            val answerBorderColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.5f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
            }
            val answerBackgroundColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.12f)
            } else {
                MaterialTheme.colors.background
            }
            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier.padding(vertical = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = optionSelected,
                            onClick = onClickHandle
                        )
                        .background(answerBackgroundColor)
                        .padding(vertical = 16.dp, horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    options[text]?.let {
                        Image(
                            painter = painterResource(
                                id = it.first
                            ),
                            contentDescription = null,
                            modifier = Modifier
                                .width(56.dp)
                                .height(56.dp)
                                .clip(MaterialTheme.shapes.medium)
                        )
                    }
                    Text(
                        text = text
                    )

                    RadioButton(
                        selected = optionSelected,
                        onClick = onClickHandle,
                        colors = RadioButtonDefaults.colors(
                            selectedColor = MaterialTheme.colors.primary
                        )
                    )
                }
            }
        }
    }
}

@Composable
private fun MultipleChoiceQuestion(
    possibleAnswer: PossibleAnswer.MultipleChoice,
    answer: Answer.MultipleChoice?,
    onAnswerSelected: (Int, Boolean) -> Unit,
    modifier: Modifier = Modifier
) {
    val options = possibleAnswer.optionsStringRes.associateBy { stringResource(id = it) }
    Column(modifier = modifier.verticalScroll(rememberScrollState())) {
        for (option in options) {
            var checkedState by remember(answer) {
                val selectedOption = answer?.answersStringRes?.contains(option.value)
                mutableStateOf(selectedOption ?: false)
            }
            val answerBorderColor = if (checkedState) {
                MaterialTheme.colors.primary.copy(alpha = 0.5f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
            }
            val answerBackgroundColor = if (checkedState) {
                MaterialTheme.colors.primary.copy(alpha = 0.12f)
            } else {
                MaterialTheme.colors.background
            }
            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier
                    .padding(vertical = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(answerBackgroundColor)
                        .clickable(
                            onClick = {
                                checkedState = !checkedState
                                onAnswerSelected(option.value, checkedState)
                            }
                        )
                        .padding(vertical = 16.dp, horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        modifier = Modifier.weight(0.8f),
                        text = option.key
                    )

                    Checkbox(
                        modifier = Modifier.weight(0.2f),
                        checked = checkedState,
                        onCheckedChange = { selected ->
                            checkedState = selected
                            onAnswerSelected(option.value, selected)
                        },
                        colors = CheckboxDefaults.colors(
                            checkedColor = MaterialTheme.colors.primary
                        ),
                    )
                }
            }
        }
    }
}


@Composable
private fun ReferralChoiceQuestion(
    possibleAnswer: PossibleAnswer.MultipleChoiceReferral,
    answer: Answer.MultipleChoiceReferral?,
    onAnswerSelected: (ReferalReason, Boolean) -> Unit,
    modifier: Modifier = Modifier
) {
    val options = possibleAnswer.referalReason.associateBy { it.displayText }
    Column(modifier = modifier.verticalScroll(rememberScrollState())) {
        for (option in options) {
            var checkedState by remember(answer) {
                val selectedOption = answer?.reasons?.contains(option.value)
                mutableStateOf(selectedOption ?: false)
            }
            val answerBorderColor = if (checkedState) {
                MaterialTheme.colors.primary.copy(alpha = 0.5f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
            }
            val answerBackgroundColor = if (checkedState) {
                MaterialTheme.colors.primary.copy(alpha = 0.12f)
            } else {
                MaterialTheme.colors.background
            }
            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier
                    .padding(vertical = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(answerBackgroundColor)
                        .clickable(
                            onClick = {
                                checkedState = !checkedState
                                onAnswerSelected(option.value, checkedState)
                            }
                        )
                        .padding(vertical = 16.dp, horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        modifier = Modifier.weight(0.8f),
                        text = option.key
                    )

                    Checkbox(
                        modifier = Modifier.weight(0.2f),
                        checked = checkedState,
                        onCheckedChange = { selected ->
                            checkedState = selected
                            onAnswerSelected(option.value, selected)
                        },
                        colors = CheckboxDefaults.colors(
                            checkedColor = MaterialTheme.colors.primary
                        ),
                    )
                }
            }
        }
    }
}


@Composable
private fun MultipleChoiceIconQuestion(
    possibleAnswer: PossibleAnswer.MultipleChoiceIcon,
    answer: Answer.MultipleChoice?,
    onAnswerSelected: (Int, Boolean) -> Unit,
    modifier: Modifier = Modifier
) {
    val options = possibleAnswer.optionsStringIconRes.associateBy { stringResource(id = it.second) }
    Column(modifier = modifier) {
        for (option in options) {
            var checkedState by remember(answer) {
                val selectedOption = answer?.answersStringRes?.contains(option.value.second)
                mutableStateOf(selectedOption ?: false)
            }
            val answerBorderColor = if (checkedState) {
                MaterialTheme.colors.primary.copy(alpha = 0.5f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
            }
            val answerBackgroundColor = if (checkedState) {
                MaterialTheme.colors.primary.copy(alpha = 0.12f)
            } else {
                MaterialTheme.colors.background
            }
            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier.padding(vertical = 4.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable(
                            onClick = {
                                checkedState = !checkedState
                                onAnswerSelected(option.value.second, checkedState)
                            }
                        )
                        .background(answerBackgroundColor)
                        .padding(vertical = 16.dp, horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Image(
                        painter = painterResource(id = option.value.first),
                        contentDescription = null,
                        modifier = Modifier
                            .width(56.dp)
                            .height(56.dp)
                            .clip(MaterialTheme.shapes.medium)
                    )
                    Text(text = option.key)

                    Checkbox(
                        checked = checkedState,
                        onCheckedChange = { selected ->
                            checkedState = selected
                            onAnswerSelected(option.value.second, selected)
                        },
                        colors = CheckboxDefaults.colors(
                            checkedColor = MaterialTheme.colors.primary
                        ),
                    )
                }
            }
        }
    }
}

@Composable
private fun ActionQuestion(
    questionId: Int,
    possibleAnswer: PossibleAnswer.Action,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    onInvalidAnswer: () -> Unit,
    modifier: Modifier = Modifier
) {
    when (possibleAnswer.actionType) {
        is SurveyActionType.PICK_DATE -> {
            DateQuestion(
                questionId = questionId,
                answer = answer,
                onAction = onAction,
                modifier = modifier
            )
        }
        is SurveyActionType.AGE_PICKER -> {
            AgeQuestion(
                questionId = questionId,
                answer = answer,
                onAction = onAction,
                modifier = modifier
            )
        }
        /* is SurveyActionType.SEARCH_FILTER -> {
             SearchFilterQuestion(
                 questionId = questionId,
                 answer = answer,
                 onAction = onAction,
                 modifier = modifier
             )
         }*/
        is SurveyActionType.AGE_PICKER_CHILD -> {
            ChildAgeQuestion(
                questionId = questionId,
                answer = answer,
                onAction = onAction,
                modifier = modifier
            )
        }
        is SurveyActionType.TAKE_PHOTO -> {
            PhotoQuestion(
                questionId = questionId,
                answer = answer,
                onAction = onAction,
                onInvalidAnswer = onInvalidAnswer,
                modifier = modifier
            )
        }
        is SurveyActionType.REFERRAL_TYPE -> {
            SelectReferralQuestion(
                questionId = questionId,
                answer = answer,
                onAction = onAction,
                modifier = modifier
            )
        }
        is SurveyActionType.SCAN_QRCODE -> {
            PhotoQuestion(
                questionId = questionId,
                answer = answer,
                onAction = onAction,
                onInvalidAnswer = onInvalidAnswer,
                modifier = modifier
            )
        }
        /*  SurveyActionType.TEXT_CHANGE -> {
              TextQuestion(
                  questionId = questionId,
                  possibleAnswer = possibleAnswer,
                  answer = answer,
                  onAction = onAction,
                  modifier = modifier
              )
          }*/


        SurveyActionType.SELECT_CONTACT() -> TODO()
    }
}


@Composable
fun SelectReferralQuestion(
    questionId: Int,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier
) {


    val radioOptions = listOf(
        stringResource(id = R.string.child),
        stringResource(id = R.string.woman),
    )

    val iconOptions = listOf(
        painterResource(id = R.drawable.child_male),
        painterResource(id = R.drawable.child_female),
    )

    val selected = if (answer != null && answer.result is SurveyActionResult.Text) {
        answer.result.text
    } else {
        ""
    }

    /* val selected = if (answer != null) {
         stringResource(id = answer.answer)
     } else {
         null
     }*/

    val (selectedOption, onOptionSelected) = remember(answer) { mutableStateOf(selected) }

    Column(modifier = modifier) {


        radioOptions.forEachIndexed { index, text ->
            val onClickHandle = {
                onOptionSelected(text)
                onAction(questionId, SurveyActionType.REFERRAL_TYPE(text))

            }
            val optionSelected = text == selectedOption
            val answerBorderColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.5f)
            } else {
                MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
            }
            val answerBackgroundColor = if (optionSelected) {
                MaterialTheme.colors.primary.copy(alpha = 0.12f)
            } else {
                MaterialTheme.colors.background
            }
            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier.padding(vertical = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = optionSelected,
                            onClick = onClickHandle
                        )
                        .background(answerBackgroundColor)
                        .padding(vertical = 16.dp, horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {

                    Image(
                        painter = iconOptions[index],
                        contentDescription = null,
                        modifier = Modifier
                            .width(56.dp)
                            .height(56.dp)
                            .clip(MaterialTheme.shapes.medium)
                    )
                    Text(
                        text = text
                    )

                    RadioButton(
                        selected = optionSelected,
                        onClick = onClickHandle,
                        colors = RadioButtonDefaults.colors(
                            selectedColor = MaterialTheme.colors.primary
                        )
                    )
                }
            }
        }
    }

}


@OptIn(ExperimentalCoilApi::class)
@Composable
private fun PhotoQuestion(
    questionId: Int,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    onInvalidAnswer: () -> Unit,
    modifier: Modifier = Modifier
) {
    val resource = if (answer != null) {
        Icons.Filled.SwapHoriz
    } else {
        Icons.Filled.AddAPhoto
    }

    val answerValue: SurveyActionResult.QRCode =
        if (answer != null && answer.result is SurveyActionResult.QRCode) {
            answer.result
        } else {
            SurveyActionResult.QRCode("", "")
        }


    var identifier by remember(answer) { mutableStateOf(answerValue.id) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }

    val scrollState = rememberScrollState()
    Column(Modifier.verticalScroll(scrollState)) {

        Card(
            modifier = modifier
                .fillMaxWidth()
                .padding(5.dp),
            contentColor = colorResource(id = R.color.white),
            backgroundColor = MaterialTheme.colors.onPrimary,
            shape = RoundedCornerShape(15.dp),
            content = {

                val focusManager = LocalFocusManager.current

                Column(
                    modifier = Modifier.padding(10.dp)
                ) {

                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 10.dp, vertical = 20.dp),
                        text = "QR code type kerein",
                        textAlign = TextAlign.Center,
                        style = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSecondary)

                    )


                    OutlinedTextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(10.dp),
                        value = identifier,
                        onValueChange = {
                            if (it.length <= 14 &&
                                !it.contains(" ") &&
                                !it.contains(",") &&
                                !it.contains("-") &&
                                !it.contains(".")
                            ) {


                                identifier = it
                                val validationResult = validateField(Validations.IDENTIFIER, it)
                                if (it.equals("")) {
                                    isError = false
                                } else {
                                    if (validationResult.isError) {
                                        isError = false
                                        onAction(
                                            questionId,
                                            SurveyActionType.FETCH_DATA(identifier)
                                        )
                                        focusManager.clearFocus()
                                    } else {
                                        onInvalidAnswer()
                                        errorMessage = validationResult.errorMessage
                                        isError = true

                                    }
                                }

                            }
                        },
                        isError = isError,
                        placeholder = {
                            Text(
                                text = stringResource(id = R.string.id_placeholder),
                                style = TextStyle(
                                    color = colorResource(id = R.color.grey),
                                    textAlign = TextAlign.Center
                                )
                            )
                        },
                        singleLine = true,
                        keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                        keyboardOptions = KeyboardOptions.Default.copy(
                            imeAction = ImeAction.Done,
                            keyboardType = KeyboardType.Number
                        ),

                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            cursorColor = MaterialTheme.colors.primaryVariant,
                            textColor = MaterialTheme.colors.primary
                        ),

                        )

                    if (isError) {
                        Text(
                            text = errorMessage,
                            color = MaterialTheme.colors.error,
                            style = MaterialTheme.typography.caption,
                            modifier = Modifier.padding(start = 16.dp)
                        )
                    }
                }
            })



        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp, vertical = 20.dp),
            text = "OR ",
            textAlign = TextAlign.Center,
            style = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSecondary)

        )



        OutlinedButton(
            onClick = { onAction(questionId, SurveyActionType.SCAN_QRCODE()) },
            modifier = modifier,
            //contentPadding = PaddingValues()
        ) {
            Column {


                if (answer != null && answer.result is SurveyActionResult.QRCode && !isError && !identifier.equals(
                        ""
                    )
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_checked),
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(40.dp)
                            .width(80.dp)
                    )
                } else {
                    PhotoDefaultImage(
                        modifier = Modifier.padding(
                            horizontal = 10.dp,
                            vertical = 10.dp
                        )
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentSize(Alignment.TopCenter)
                        .padding(vertical = 10.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(imageVector = resource, contentDescription = null)
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(
                        text = stringResource(
                            id = if (answer != null) {
                                R.string.rescan_qrcode
                            } else {
                                R.string.add_photo
                            }
                        )
                    )
                }
            }
        }

        if (answer != null && answer.result is SurveyActionResult.QRCode && !isError && !identifier.equals(
                ""
            )
        ) {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp, vertical = 20.dp),
                text = "${answer.result.message}",
                textAlign = TextAlign.Center,
                style = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSecondary)
            )
        }
    }
}

@Composable
private fun DateQuestion(
    questionId: Int,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier = Modifier
) {
    val date = if (answer != null && answer.result is SurveyActionResult.Date) {
        answer.result.date
    } else {
        //SimpleDateFormat(simpleDateFormatPattern, Locale.getDefault()).format(Date())
        "DD-MM-YYYY"
    }
    Button(
        onClick = { onAction(questionId, SurveyActionType.PICK_DATE()) },
        colors = ButtonDefaults.buttonColors(
            backgroundColor = MaterialTheme.colors.onPrimary,
            contentColor = MaterialTheme.colors.onSecondary
        ),
        shape = MaterialTheme.shapes.small,
        modifier = modifier
            .padding(vertical = 20.dp)
            .height(54.dp),
        elevation = ButtonDefaults.elevation(0.dp),
        border = BorderStroke(1.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.12f))

    ) {
        Text(
            text = date,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1.8f)
        )
        /*  Icon(
              imageVector = Icons.Filled.ArrowDropDown,
              contentDescription = null,
              modifier = Modifier
                  .fillMaxWidth()
                  .weight(0.2f)
          )*/
    }
}


@Composable
private fun AgeQuestion(
    questionId: Int,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier = Modifier
) {
    val date = if (answer != null && answer.result is SurveyActionResult.Date) {
        answer.result.date
    } else {
        "DD-MM-YYYY"
        //SimpleDateFormat(simpleDateFormatPattern, Locale.getDefault()).format(Date())
    }



    Row(modifier = modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {

        IconButton(
            //modifier = modifier,
            onClick = { onAction(questionId, SurveyActionType.PICK_DATE()) },
        ) {
            Column() {


                Icon(
                    painter = painterResource(id = R.drawable.ic_calendar),
                    contentDescription = null,
                    modifier = Modifier
                        .height(50.dp)
                        .width(50.dp)
                        .align(Alignment.CenterHorizontally)

                )
                Text(
                    "Calendar",
                    // modifier = modifier.weight(0.5f),
                    textAlign = TextAlign.Center
                )

            }
        }

        IconButton(

            onClick = { onAction(questionId, SurveyActionType.AGE_PICKER()) },
        ) {
            Column() {

                Icon(
                    painter = painterResource(id = R.drawable.ic_calculator),
                    contentDescription = null,
                    modifier = Modifier
                        .height(50.dp)
                        .width(50.dp)
                        .align(Alignment.CenterHorizontally)
                )

                Text(
                    "Age Calculator",
                    // modifier = modifier.weight(0.5f),
                    textAlign = TextAlign.Center
                )


            }
        }

    }


    Surface(
        //onClick = { onAction(questionId, SurveyActionType.PICK_DATE) },

        shape = MaterialTheme.shapes.small,
        modifier = modifier
            .padding(vertical = 20.dp)
            .height(54.dp),
        border = BorderStroke(1.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.12f))

    ) {
        Text(
            text = date,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(align = Alignment.CenterVertically)
                .wrapContentWidth(align = Alignment.CenterHorizontally),
            textAlign = TextAlign.Center

        )

    }
}


@Composable
private fun ChildAgeQuestion(
    questionId: Int,
    answer: Answer.Action?,
    onAction: (Int, SurveyActionType) -> Unit,
    modifier: Modifier = androidx.compose.ui.Modifier
) {
    val date = if (answer != null && answer.result is SurveyActionResult.Date) {
        answer.result.date
    } else {

        "DD-MM-YYYY"
        //SimpleDateFormat(simpleDateFormatPattern, Locale.getDefault()).format(Date())
    }



    Row(modifier = modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {

        IconButton(
            // modifier = modifier,
            onClick = {
                onAction(
                    questionId,
                    SurveyActionType.PICK_DATE()
                )
            },
        ) {
            Column() {


                Icon(
                    painter = painterResource(id = com.ird.compose.mref.R.drawable.ic_calendar),
                    contentDescription = null,
                    modifier = Modifier
                        .height(50.dp)
                        .width(50.dp)
                        .align(Alignment.CenterHorizontally)

                )
                Text(
                    "Calendar",
                    //  modifier = modifier,
                    textAlign = androidx.compose.ui.text.style.TextAlign.Center
                )

            }
        }

        androidx.compose.material.IconButton(
            //modifier = modifier,
            onClick = {
                onAction(
                    questionId,
                    com.ird.compose.mref.survey.SurveyActionType.AGE_PICKER_CHILD()
                )
            },
        ) {
            androidx.compose.foundation.layout.Column() {

                androidx.compose.material.Icon(
                    painter = androidx.compose.ui.res.painterResource(id = com.ird.compose.mref.R.drawable.ic_calculator),
                    contentDescription = null,
                    modifier = Modifier
                        .height(50.dp)
                        .width(50.dp)
                        .align(Alignment.CenterHorizontally)
                )

                androidx.compose.material.Text(
                    "Age Calculator",
                    // modifier = modifier,
                    textAlign = androidx.compose.ui.text.style.TextAlign.Center
                )


            }
        }

    }


    androidx.compose.material.Surface(
        //onClick = { onAction(questionId, SurveyActionType.PICK_DATE) },

        shape = androidx.compose.material.MaterialTheme.shapes.small,
        modifier = modifier
            .padding(vertical = 20.dp)
            .height(54.dp),
        border = androidx.compose.foundation.BorderStroke(
            1.dp,
            androidx.compose.material.MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
        )

    ) {
        Text(
            text = date,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(align = Alignment.CenterVertically)
                .wrapContentWidth(align = Alignment.CenterHorizontally),

            textAlign = TextAlign.Center

        )

    }
}


@Composable
private fun PhotoDefaultImage(
    modifier: Modifier = Modifier,
    lightTheme: Boolean = MaterialTheme.colors.isLight
) {
    val assetId = R.drawable.ic_qr_code
    Image(
        painter = painterResource(id = assetId),
        modifier = modifier
            .fillMaxWidth()
            .height(100.dp),
        contentDescription = null
    )
}

@Composable
private fun SliderQuestion(
    possibleAnswer: PossibleAnswer.Slider,
    answer: Answer.Slider?,
    onAnswerSelected: (Float) -> Unit,
    modifier: Modifier = Modifier
) {
    var sliderPosition by remember {
        mutableStateOf(answer?.answerValue ?: possibleAnswer.defaultValue)
    }
    Row(modifier = modifier) {

        Slider(
            value = sliderPosition,
            onValueChange = {
                sliderPosition = it
                onAnswerSelected(it)
            },
            valueRange = possibleAnswer.range,
            steps = possibleAnswer.steps,
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 16.dp)
        )
    }
    Row {
        Text(
            text = stringResource(id = possibleAnswer.startText),
            style = MaterialTheme.typography.caption,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1.8f)
        )
        Text(
            text = stringResource(id = possibleAnswer.neutralText),
            style = MaterialTheme.typography.caption,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1.8f)
        )
        Text(
            text = stringResource(id = possibleAnswer.endText),
            style = MaterialTheme.typography.caption,
            textAlign = TextAlign.End,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1.8f)
        )
    }
}

@Preview
@Composable
fun QuestionPreview() {
    val question = Question(
        id = 2,
        questionText = R.string.pick_superhero,
        answer = PossibleAnswer.SelectLocation(

            listOf(

                Location(name = "Azam Town", locationId = 2, parentLocation = 5, locationType = 12),
                Location(
                    name = "Kamran Town",
                    locationId = 5,
                    parentLocation = 3,
                    locationType = 6
                ),
                Location(
                    name = "Abbas Town",
                    locationId = 3,
                    parentLocation = -1,
                    locationType = 4
                ),


                )
        ),
        description = R.string.empty_hint,
        nextQuestion = 1,
        prevQuestion = 0,
        key = Keys.NO_KEY

    )
    MRefTheme {
        Question(
            question = question,
            shouldAskPermissions = true,
            answer = null,
            onAnswer = {},
            onInvalidAnswer = { },
            onAction = { _, _ -> },
            onDoNotAskForPermissions = {},
            openSettings = {}
        )
    }
}
