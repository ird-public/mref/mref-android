package com.ird.compose.mref.home

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.work.*
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.ird.compose.mref.App
import com.ird.compose.mref.R
import com.ird.compose.mref.Screen
import com.ird.compose.mref.db.dao.AttendanceDao
import com.ird.compose.mref.db.dao.FormDao
import com.ird.compose.mref.locationProvider.ILocationListener
import com.ird.compose.mref.locationProvider.LocationProvider
import com.ird.compose.mref.model.*
import com.ird.compose.mref.navigate
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.service.AttendanceService
import com.ird.compose.mref.service.UploadWorker

import com.ird.compose.mref.signinsignup.ViewModelFactory
import com.ird.compose.mref.signinsignup.WelcomeViewModel
import com.ird.compose.mref.theme.MRefTheme
import com.ird.compose.mref.util.DateTimeUtils
import com.ird.compose.mref.util.DateTimeUtils.isInternetAvailable
import com.ird.compose.mref.util.PermissionUtils
import kotlinx.coroutines.*
import javax.inject.Inject
import android.content.ComponentName
import com.google.accompanist.insets.ProvideWindowInsets


class HomeFragment : Fragment() {

    private val LOCATION_PERMISSION_REQUEST_CODE = 99
    var latitude: Double = 0.0
    var longitude: Double = 0.0

    //var isCheckedIn = false
    var job: Job? = null

    @Inject
    lateinit var attendanceDao: AttendanceDao

    @Inject
    lateinit var locationProvider: LocationProvider

    @Inject
    lateinit var mainRespository: MainRepository

    @Inject
    lateinit var preference: DevicePreference

    @Inject
    lateinit var formDao: FormDao


    private val viewModel: WelcomeViewModel by viewModels { ViewModelFactory(mainRespository) }

    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        //onError("Exception handled: ${throwable.localizedMessage}")
    }

    private val statistics =
        mutableStateOf<StatisticsResponse>(StatisticsResponse())

    @OptIn(ExperimentalMaterialApi::class)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        (activity?.application as App).appComponent.inject(this)

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    showBackDialog()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        viewModel.navigateTo.observe(viewLifecycleOwner) { navigateToEvent ->
            navigateToEvent.getContentIfNotHandled()?.let { navigateTo ->
                navigate(navigateTo, Screen.Home)
            }
        }

        viewModel.onStatsRecieved.observe(viewLifecycleOwner) {
            if (it != null)
                statistics.value = it
        }

        return ComposeView(requireContext()).apply {

            setContent {
                MRefTheme {

                    var attendanceUIStatus by remember { mutableStateOf(AttendanceUIStatus.NO_ATTENDANCE) }
                    viewModel.attendanceStatus.observe(viewLifecycleOwner) {
                        attendanceUIStatus = it
                    }
                    ProvideWindowInsets(
                        //  consumeWindowInsets = false,
                        windowInsetsAnimationsEnabled = true
                    )
                    {
                        Home(

                            statistics.value,
                            preference.getUser().permission,
                            onLogout = {
                                showBackDialog()
                            },
                            checkedIn = {
                                if (attendanceUIStatus == AttendanceUIStatus.NO_ATTENDANCE) {
                                    attendanceUIStatus = AttendanceUIStatus.CHECKED_IN
                                    viewModel.updateAttendanceStatus(AttendanceUIStatus.CHECKED_IN)
                                } else if (attendanceUIStatus == AttendanceUIStatus.CHECKED_IN) {

                                    showConfirmationDialog() {
                                        attendanceUIStatus = AttendanceUIStatus.CHECKED_OUT
                                        viewModel.updateAttendanceStatus(AttendanceUIStatus.CHECKED_OUT)

                                    }
                                }
                                setCheckInOut(attendanceUIStatus)
                            },
                            attendanceUIStatus
                        ) { event ->
                            when (event) {
                                is HomeScreenEvent.StartSurvey -> {
                                    val bundle: Bundle = Bundle().apply {
                                        putString("formType", "referral")
                                    }
                                    navigate(Screen.Survey, Screen.Home, bundle)
                                    //viewModel.NavigateReferralScreen()
                                }
                                is HomeScreenEvent.StartFollowup -> {
                                    val bundle: Bundle = Bundle().apply {
                                        putString("formType", "followup")
                                    }
                                    navigate(Screen.Survey, Screen.Home, bundle)
                                }

                                is HomeScreenEvent.StartSearch -> {
                                    val bundle: Bundle = Bundle().apply {
                                        putString("formType", "search")
                                    }
                                    navigate(Screen.Survey, Screen.Home, bundle)
                                }

                                is HomeScreenEvent.StartSavedForm -> {
                                    navigate(Screen.SavedForms, Screen.Home)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        viewModel.getStatistics(preference.getUser().mappedId)
    }

    private fun showConfirmationDialog(onConfirmation: () -> Unit) {
        val builder = AlertDialog.Builder(requireContext(), R.style.AppTheme_Dialog)
        builder.setTitle("Rukiye")
        builder.setMessage("Kia aap Check out krna chahte hen?")
        builder.setPositiveButton("Yes") { dialog, which ->
            onConfirmation()
        }
        builder.setNegativeButton("No") { dialog, which ->
        }
        builder.show()

    }

    private fun showBackDialog() {
        val builder = AlertDialog.Builder(requireContext(), R.style.AppTheme_Dialog)
        builder.setTitle("Rukiye")
        builder.setMessage("Kia aap Log out krna chahte hen? ")
        builder.setPositiveButton("Yes") { dialog, which ->
            findNavController().navigate(
                R.id.welcome_fragment,
                null,
                NavOptions.Builder()
                    .setPopUpTo(R.id.home_fragment, true)
                    .build()
            )

            findNavController().navigate(R.id.welcome_fragment)
            //Navigation.findNavController(requireView()).popBackStack()
        }
        builder.setNegativeButton("No") { dialog, which ->
        }
        builder.show()
    }


    private fun showPermissionDialog() {
        val builder = AlertDialog.Builder(requireContext(), R.style.AppTheme_Dialog)
        builder.setTitle("Location Access")
        builder.setCancelable(false)
        builder.setMessage(
            "Location is required to mark your attendance.\n\n" +
                    "You can not fill any forms without giving permission access. \n\n" +
                    "Kindly allow location permission after pressing Yes"
        )





        builder.setPositiveButton("Yes") { dialog, which ->
            PermissionUtils.askAccessFineLocationPermission(
                this,
                LOCATION_PERMISSION_REQUEST_CODE
            )
            //Navigation.findNavController(requireView()).popBackStack()
        }
        builder.setNegativeButton("No") { dialog, which ->
            findNavController().navigate(
                R.id.welcome_fragment,
                null,
                NavOptions.Builder()
                    .setPopUpTo(R.id.home_fragment, true)
                    .build()
            )

            findNavController().navigate(R.id.welcome_fragment)

        }

        /* builder.setNeutralButton("Enable from settings") { dialog, which ->
             val intent = Intent(Intent.ACTION_MAIN)
             intent.component = ComponentName(
                 requireActivity().application.packageName,
                 "com.android.packageinstaller.permission.ui.ManagePermissionsActivity"
             )
             startActivity(intent)
             //Navigation.findNavController(requireView()).popBackStack()
         }*/



        builder.show()
    }


    private fun setCheckInOut(attendanceUIStatus: AttendanceUIStatus) {

        if (attendanceUIStatus == AttendanceUIStatus.CHECKED_IN) { //do check in
            doCheckIn()
        } else if (attendanceUIStatus == AttendanceUIStatus.CHECKED_OUT)//do check out
            doCheckOut()
    }

    private fun doCheckIn() {

        saveCheckInAttendance(getTime(), getDate())
        uploadAttendance()
        //uploadCheckInAttendance()
    }

    private fun doCheckOut() {

        saveCheckOutAttendance(getTime(), getDate())
        uploadAttendance()

        //uploadCheckOutAttendance()
    }


    private fun saveCheckInAttendance(checkInTime: String, checkInDate: String) {
        attendanceDao.insert(
            AttendanceCheckIn(
                id = 0,
                checkInTime,
                latitude.toString(),
                longitude.toString(),
                checkInDate,
                DevicePreference.getUser().mappedId,
                1,
                0
            )
        )
    }

    private fun uploadCheckInAttendance() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {

            val attendanceCheckIn = AttendanceCheckIn(
                checkIn = getTime(),
                checkInLatitude = latitude.toString(),
                checkInLongitude = longitude.toString(),
                attendanceDate = getDate(),
                mappedId = DevicePreference.getUser().mappedId
            )

            val response = mainRespository.attendanceCheckIn(attendanceCheckIn)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    Toast.makeText(
                        requireContext(),
                        "Success: " + response.body().toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Error: " + response.body().toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun uploadCheckOutAttendance() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {

            val dao = attendanceDao.getCheckOutAttendance().last()
            val attendanceCheckOut = AttendanceCheckOut(
                checkOut = dao.checkOut,
                checkOutLatitude = dao.checkOutLatitude,
                checkOutLongitude = dao.checkOutLongitude,
                attendanceDate = dao.attendanceDate,
                mappedId = dao.mappedId,
                status = dao.status,
                IsUploadedSuccess = dao.IsUploadedSuccess
            )

            val response = mainRespository.attendanceCheckOut(attendanceCheckOut)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Toast.makeText(
                        requireContext(),
                        "Success: " + response.body(),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Error: " + response.body(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun saveCheckOutAttendance(chekOutTime: String, checkOutDate: String) {
        attendanceDao.insert(
            AttendanceCheckOut(
                id = 0,
                chekOutTime,
                latitude.toString(),
                longitude.toString(),
                checkOutDate,
                DevicePreference.getUser().mappedId,
                0,
                0
            )
        )
    }

    private fun getDate(): String {
        val date = DateTimeUtils.getDBCurrentDate()
        return date
    }

    private fun getTime(): String {
        val time = DateTimeUtils.getCurrentTimeWithoutDate()
        return time
    }

    private fun checkPermission() {
        if (PermissionUtils.checkAccessFineLocationGranted(requireContext())) {
            if (PermissionUtils.isLocationEnabled(requireActivity())) {
                locationProvider.getUserLocation(
                    object : ILocationListener {
                        override fun onLocationReceived(
                            locationResult: LocationResult?,
                            callback: LocationCallback?
                        ) {
                            locationProvider.stopUpdates(callback)
                            latitude = locationResult?.lastLocation?.latitude!!
                            longitude = locationResult.lastLocation.longitude
                        }
                    })
            } else {
                PermissionUtils.showGPSNotEnabledDialog(requireContext())
            }
        } else {
            showPermissionDialog()
            //PermissionUtils.askAccessFineLocationPermission(this, LOCATION_PERMISSION_REQUEST_CODE)
        }
    }

    private fun uploadAttendance() {
        if (isInternetAvailable(requireContext())) {
            val builder: Constraints.Builder =
                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)


            val syncWorkRequest: WorkRequest =
                OneTimeWorkRequestBuilder<AttendanceService>()
                    .setConstraints(builder.build())
                    .build()

            WorkManager.getInstance(requireContext()).enqueue(syncWorkRequest)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationProvider.getUserLocation(
                    object : ILocationListener {
                        override fun onLocationReceived(
                            locationResult: LocationResult?,
                            callback: LocationCallback?
                        ) {
                            locationProvider.stopUpdates(callback)
                            latitude = locationResult?.lastLocation?.latitude!!
                            longitude = locationResult.lastLocation.longitude
                        }
                    })
                checkPermission()
            } else {
                showPermissionDialog()

            }
        }
    }

    override fun onResume() {
        super.onResume()
        checkUserTodayAttendance()
    }

    override fun onStart() {
        super.onStart()
        checkPermission()
    }

    fun checkUserTodayAttendance() {
        val checkIn =
            attendanceDao.getTodayCheckInByUserId(DevicePreference.getUser().mappedId, getDate())
        val checkOut =
            attendanceDao.getTodayCheckOutByUserId(DevicePreference.getUser().mappedId, getDate())
        if (checkIn != null && checkOut != null) {
            viewModel.updateAttendanceStatus(AttendanceUIStatus.CHECKED_OUT)
        } else if (checkIn != null && checkOut == null) {
            viewModel.updateAttendanceStatus(AttendanceUIStatus.CHECKED_IN)
        } else {
            viewModel.updateAttendanceStatus(AttendanceUIStatus.NO_ATTENDANCE)
        }

        // if (checkIn != null && checkOut != null) status.noStatus() else if (checkIn != null && checkOut != null) status.checkInStatus() else status.checkOutStatus()
    }

}

enum class AttendanceUIStatus {
    CHECKED_IN,
    CHECKED_OUT,
    NO_ATTENDANCE
}
