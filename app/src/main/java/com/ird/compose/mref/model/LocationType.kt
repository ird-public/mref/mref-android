package com.ird.compose.mref.model

data class LocationType (

	val locationTypeId : Int,
	val typeName : String
)