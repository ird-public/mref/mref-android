package com.ird.compose.mref.di.modules

import com.ird.compose.mref.network.ApiService
import com.ird.compose.mref.repository.MainRepository
import dagger.Module
import dagger.Provides

@Module
object RepositoryModule {


    @Provides
    fun provideMainRepository(apiService: ApiService): MainRepository {
        return MainRepository(apiService)
    }
}