package com.ird.compose.mref.model

import android.content.SharedPreferences
import android.text.format.DateUtils
import com.google.gson.Gson
import com.ird.compose.mref.util.DateTimeUtils


object DevicePreference {


    const val SERVER_KEY = "server_address"
    const val PORT_KEY = "port_number"
    private const val IS_FIRST_TIME = "isFirstTime"
    private const val IS_CHECKED_IN = "isCheckedIn"
    private const val LOCATION = "Location"
    private const val DATE_TIME = "Date_Time"
    const val USER = "user"
    const val REMEMBER = "IS_REMEMBER"
    const val PROFILE = "profile"
    const val TOKEN = "Token"
    const val PUSH_TOKEN = "pushToken"

    const val LOTTERY_NOTIFICATION = "lotteryNotification"
    const val LANGUAGE = "language"

    const val ACTIVE_USER = "activeUser"
    lateinit var preferences: SharedPreferences


    fun init(sharedPreferences: SharedPreferences) {
        this.preferences = sharedPreferences
    }

    private fun putInteger(key: String, data: Int) {
        preferences.edit().putInt(key, data).apply()
    }

    private fun getInteger(key: String): Int {
        return preferences.getInt(key, 0)
    }

    private fun putString(key: String, data: String) {
        preferences.edit().putString(key, data).apply()
    }

    private fun getString(key: String): String? {
        return preferences.getString(key, "")
    }

    private fun getBoolean(key: String): Boolean {
        return preferences.getBoolean(key, true)
    }

/*    fun saveLanguage(language: Language?) {
        val json: String = Gson().toJson(language)
        preferences!!.edit().putString(LANGUAGE, json).apply()
    }

    fun getLanguage(): Language? {
        val json = preferences!!.getString(LANGUAGE, "")
        return Gson().fromJson(json, Language::class.java)
    }*/

    fun getUser(): User {
        val json = preferences.getString(USER, "")
        return Gson().fromJson(json, User::class.java)
    }

    fun saveUser(user: User) {
        val json: String = Gson().toJson(user)
        preferences.edit().putString(USER, json).apply()
    }

    fun savePassword(password: String){
        preferences.edit().putString("PASSWORD", password).apply()
    }

    fun getPassword(): String? {
        return preferences.getString("PASSWORD", "")
    }


    fun getFormCount(): FormCount {
        val json = preferences.getString(
            "FORM_COUNT", "{\n" +
                    "  \n" +
                    "\"date\": \"\",\n" +
                    "\"count\": 0\n" +
                    "}"
        )
        return Gson().fromJson(json, FormCount::class.java)
    }

    fun saveFormCount(formCount: FormCount) {
        val json: String = Gson().toJson(formCount)
        preferences.edit().putString("FORM_COUNT", json).apply()
    }


    fun isFirstTime(): Boolean {
        return preferences!!.getBoolean(IS_FIRST_TIME, true)
    }

    fun invalidateFirstTimeFlag() {
        preferences!!.edit().putBoolean(IS_FIRST_TIME, false).apply()
    }


    fun getCounter(): Int {
        return preferences.getInt("TOTAL_COUNT", 0)
    }

    fun updateCounter(date: String) {
        val totalCount = getCounter() + 1
        preferences.edit().putInt("TOTAL_COUNT", totalCount).apply()

        if (getFormCount().date.equals("")) {
            saveFormCount(FormCount(date = date, 1))
        } else if (getFormCount().date == DateTimeUtils.getCurrentDate()) {
            val form = getFormCount()
            val formCount = form.count + 1
            form.count = formCount
            saveFormCount(form)
        } else {
            saveFormCount(FormCount(date = date, 1))
        }

    }

}





