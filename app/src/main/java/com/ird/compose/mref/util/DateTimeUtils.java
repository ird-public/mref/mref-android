package com.ird.compose.mref.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.NumberPicker;
import android.widget.Toast;


import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class DateTimeUtils {


    private final static String TAG_DATE_TIME_UTILS = "DateTimeUtils";

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public static final String DATE_FORMAT_SHORT = "dd-MM-yyyy";
    public static final String DATE_FORMAT_SHORT_NORMAL = "dd-MM-yyyy";
    public static final String DATE_FORMAT_DATABASE = "yyyy-MM-dd";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String DB_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.S";

    /***
     * Method returns the difference between two dates and always returns as a postive integer
     *
     * @param d1
     * @param d2
     * @return
     */
    public static int daysBetween(Date d1, Date d2) {
        int diff = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
        return diff > 0 ? diff : (diff * -1);
    }

    public static SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    /**
     * Method to return date in String format to a java.util.Date object
     *
     * @param textDate String representing the value of the date
     * @param format   Date format of the given text
     * @return
     */
    public static Date StringToDate(String textDate, String format) {
        if (textDate == null || "".equals(textDate.trim()))
            return null;
        Date dt = null;
        if (format == null)
            format = DATE_FORMAT_SHORT;
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            dt = sdf.parse(textDate);
        } catch (ParseException e) {
            Log.e(TAG_DATE_TIME_UTILS, e.getMessage());
            e.printStackTrace();
        }
        // Same for any OTHER exception
        catch (Exception ex) {
            Log.e(TAG_DATE_TIME_UTILS, ex.getMessage());
            ex.printStackTrace();
        }
        return dt;
    }

    public static Date ComplexStringToDate(String textDate, String format) {
        if (textDate == null || "".equals(textDate.trim()))
            return null;
        Date dt = null;
        if (format == null)
            format = DATE_FORMAT_SHORT;
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
        //sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            dt = sdf.parse(textDate);
        } catch (ParseException e) {
            Log.e(TAG_DATE_TIME_UTILS, e.getMessage());
            e.printStackTrace();
        }
        // Same for any OTHER exception
        catch (Exception ex) {
            Log.e(TAG_DATE_TIME_UTILS, ex.getMessage());
            ex.printStackTrace();
        }
        return dt;
    }

    public static boolean validateCalculatorWidget(Context context, NumberPicker npYears, NumberPicker npMonths, NumberPicker npWeeks, NumberPicker npDays) {
        int year = npYears.getValue();
        int month = npMonths.getValue();
        int week = npWeeks.getValue();
        int day = npDays.getValue();
        if (year == 0 && month == 0 && week == 0 && day == 0) {
            Toast.makeText(context, "Must enter a value greater than 0 for at least one from Year, Month, Week or Day", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public static String StringDateToQueryString(String textDate, String format) {
        String queryString = "";

        if (textDate == null || "".equals(textDate.trim()))
            return null;

        if (format.equals(DATE_FORMAT_SHORT)) { // 20/10/2018
            queryString = textDate.substring(6) + textDate.substring(3, 5) + textDate.substring(0, 2);
        } else
            return null;

        return queryString;
    }


    public static String stringToSqlString(String textDate) {
        if (textDate == null || "".equals(textDate.trim()))
            return null;
        Date dt = StringToDate(textDate, DATE_FORMAT_SHORT);
        return DateToString(dt, DATE_FORMAT_DATABASE);
    }

    public static String DateToString(Date date, String format) {
        if (date == null)
            return "";
        SimpleDateFormat sdf;
        if (format == null) {
            sdf = dateFormat;
        } else {
            try {
                sdf = new SimpleDateFormat(format);
            } catch (Exception ex) {
                return null;
            }
        }
        return sdf.format(date);
    }

    /**
     * Method to fetch number of months between two dates
     * Credit: Roland Tepp @ StakOverflow
     *
     * @param olderDate
     * @param newerDate
     * @return
     */
    public static int getMonthsDifference(Date olderDate, Date newerDate) {
        int m1 = olderDate.getYear() * 12 + olderDate.getMonth();
        int m2 = newerDate.getYear() * 12 + newerDate.getMonth();
        return m2 - m1;
    }

    public static Date getTodaysDateWithoutTime() {
        Date today;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        today = cal.getTime();
        return today;
    }

    public static Date getTodaysDateWithoutTime(Date date) {
        Date today;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        today = cal.getTime();
        return today;
    }

    public static Date addOneday(Date date) {
        Date today;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);
        today = cal.getTime();
        return today;
    }

    public static Date reduceDatetoTwelveYears(Date date) {
        Date today;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 12);
        today = cal.getTime();
        return today;
    }

    public static String getCurrentDate()  {
        return DateTimeUtils.DateToString(
                DateTimeUtils.getTodaysDateWithoutTime(),
                DATE_FORMAT_SHORT
        );
    }

    public static String getDBCurrentDate()  {
        return DateTimeUtils.DateToString(
                DateTimeUtils.getTodaysDateWithoutTime(),
                DATE_FORMAT_DATABASE
        );
    }



    public static String dateToMilis(String date) {
        long millis = 0;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_SHORT);
            Date dateObject = null;
            dateObject = sdf.parse(date);
            millis = dateObject.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "" + millis;
    }

    public static String getCurrentTimeWithoutDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    public static boolean isPastDate(Date d) {
        return d.getTime() < new Date().getTime();
    }

    public static boolean isPastDate(Calendar c) {
        Date d = c.getTime();
        return d.getTime() < new Date().getTime();
    }

    public static Date convertStringToDateObject(String dob) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date parsedDate;
        parsedDate = formatter.parse(dob);
        return parsedDate;
    }

    public static Date getDate(String date) {
        if (date == null || "".equalsIgnoreCase(date.trim())) {
            return null;
        }
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();

            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String getCurrentYear() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        String yearInString = String.valueOf(year);
        return yearInString;
    }


    public static String calculateAge(Boolean isChild, Date birthDate) {

        if (birthDate == null) return "";

        int years = 0;
        int months = 0;
        int days = 0;

        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        int totalMonths = 0;
        int dateDiff = now.get(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH);

        if (dateDiff < 0) {
            int borrrow = now.getActualMaximum(Calendar.DAY_OF_MONTH);
            dateDiff = (now.get(Calendar.DAY_OF_MONTH) + borrrow) - birthDay.get(Calendar.DAY_OF_MONTH);
            totalMonths--;

            if (dateDiff > 0) {
                totalMonths++;
            }
        } else {
            totalMonths++;
        }
        totalMonths += now.get(Calendar.MONTH) - birthDay.get(Calendar.MONTH);
        totalMonths += (now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR)) * 12;
        totalMonths--;

        //Get difference between months
        months = currMonth - birthMonth;

        //if month difference is in negative then reduce years by one
        //and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }

        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }

        String age = "";

        if (!isChild) {//women
            age = String.valueOf(years) + "Y";
        } else {//child
            if (years >= 2) {
                if (months < 1) age += years + "Y";
                else age += years + "Y " + months + "M";
                return age;
            }
            if (totalMonths >= 3) {
                if (days < 1) age += totalMonths + "M";
                else age += totalMonths + "M " + days + "D";
                return age;
            }
            if (totalMonths < 3) {
                if (months == 0 && days == 0) age += "OD";
                else if (days < 1) age += getWeeksBetween(birthDate, now.getTime()) - 1 + "W";
                else {
                    Date a = birthDate;
                    Date b = Calendar.getInstance().getTime();
                    a = resetTime(a);
                    b = resetTime(b);

                    Calendar cal = new GregorianCalendar();
                    cal.setTime(a);
                    int weeks = 0;
                    while (cal.getTime().before(b)) {
                        // add another week
                        cal.add(Calendar.WEEK_OF_YEAR, 1);
                        weeks++;
                    }
                    cal.add(Calendar.WEEK_OF_YEAR, -1);
                    int extradays = 0;
                    while (cal.getTime().before(b)) {
                        cal.add(Calendar.DAY_OF_WEEK, 1);
                        extradays++;
                    }


                    if (weeks - 1 > 0) {
                        if (extradays == 7) age += weeks + "W";
                        else age += weeks - 1 + "W " + extradays + "D";
                    } else age += extradays + "D";

//                    if(weeks==0){
//                        age+= extradays + "D";
//                    } else{
//                        if (weeks>=1) {
//                            age += weeks + "W";
//                        }else age+= weeks-1 + "W " + extradays + "D";
//                    }
                }
                return age;
            }
            if ((getWeeksBetween(birthDate, now.getTime()) - 1) < 1) {
                age += days + "D";
                return age;
            }

        }


//        if (years > 0)
//            age = String.valueOf(years) + "Y";
//        if (isChild){
//            if (months > 0) {
//                age += " " + months + "M";
//
//                if (months <= 4 || (months == 0 && days >= 7)) {
//                    age += " " + getWeeksBetween(birthDate, now.getTime()) + "W";
//                } else
//                if (days < 0)
//                    age += " " + days + "D";
//            } else
//                if(days!=0) age += " " + days + "D";
//        }

        //Create new Age object
        return age;
    }

    public static int getWeeksBetween(Date a, Date b) {

        if (b.before(a)) {
            return -getWeeksBetween(b, a);
        }
        a = resetTime(a);
        b = resetTime(b);

        Calendar cal = new GregorianCalendar();
        cal.setTime(a);
        int weeks = 0;
        while (cal.getTime().before(b)) {
            // add another week
            cal.add(Calendar.WEEK_OF_YEAR, 1);
            weeks++;
        }
        return weeks;
    }

    public static Date resetTime(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static boolean isInternetAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable() && connectivityManager.getActiveNetworkInfo().isConnected();
        }
        return false;
    }

}
