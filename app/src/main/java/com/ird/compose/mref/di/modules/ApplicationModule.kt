package com.ird.compose.mref.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.ird.compose.mref.db.dao.AttendanceDao

import dagger.Module
import dagger.Provides
import javax.inject.Singleton


import com.ird.compose.mref.model.DevicePreference
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.service.AttendanceService


@Module
class ApplicationModule(private val app: Application) {
    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    fun provideApplication(): Application = app


    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @Singleton
    fun provideDevicePreference(sharedPreferences: SharedPreferences): DevicePreference {
        DevicePreference.init(sharedPreferences)
        return DevicePreference
    }

/*    @Provides
    @Singleton
    fun provideAttendanceService(context: Context,attendanceDao: AttendanceDao,mainRepository: MainRepository): AttendanceService {
        return AttendanceService(context,attendanceDao,mainRepository)
    }*/
}
