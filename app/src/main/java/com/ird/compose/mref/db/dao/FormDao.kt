package com.ird.compose.mref.db.dao

import androidx.room.*
import com.ird.compose.mref.model.ChildPayload
import com.ird.compose.mref.model.Location
import com.ird.compose.mref.model.OfflineForm


@Dao
interface FormDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(payload: ChildPayload)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(payload: OfflineForm)

    @Query("SELECT * FROM offline_form")
    fun getOfflineForms(): List<OfflineForm>


    @Query("SELECT * FROM form")
    fun getForms(): List<ChildPayload>

    @Delete
    fun delete(model: ChildPayload)

    @Delete
    fun delete(model: OfflineForm)

}