package com.ird.compose.mref.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.ird.compose.mref.db.Converter

@Entity(tableName = "attendance_check_out")
@TypeConverters(Converter::class)
data class AttendanceCheckOut(

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("checkOut") var checkOut: String = "",
    @SerializedName("checkOutLatitude") var checkOutLatitude: String = "",
    @SerializedName("checkOutLongitude") var checkOutLongitude: String = "",
    @SerializedName("attendanceDate") var attendanceDate: String = "",
    @SerializedName("mappedId") var mappedId: Int? = null,
    @SerializedName("status") var status: Int? = null,
    @SerializedName("IsUploadedSuccess") var IsUploadedSuccess: Int? = null
    )
