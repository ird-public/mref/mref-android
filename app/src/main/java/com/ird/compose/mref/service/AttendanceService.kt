package com.ird.compose.mref.service

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import com.ird.compose.mref.App
import com.ird.compose.mref.R
import com.ird.compose.mref.db.dao.AttendanceDao
import com.ird.compose.mref.model.*
import com.ird.compose.mref.network.OnResponseReceivedListener
import com.ird.compose.mref.repository.MainRepository
import com.ird.compose.mref.util.Constants.CHECK_IN_EVENT
import com.ird.compose.mref.util.Constants.CHECK_OUT_EVENT
import com.ird.compose.mref.util.DateTimeUtils
import kotlinx.coroutines.*
import java.lang.StringBuilder
import javax.inject.Inject
import kotlin.coroutines.suspendCoroutine
import kotlin.math.log

class AttendanceService(
    context: Context, parameters: WorkerParameters
) : CoroutineWorker(context, parameters) {


    @Inject
    lateinit var mainRespository: MainRepository

    @Inject
    lateinit var attendanceDao: AttendanceDao


    private lateinit var job: Any


    private val DELETE_KEY = "deleteKey"
    private val TODAY_DATE = "todayDate"
    private val TIME = (86400000 * 5).toLong()
    private val CHECKIN_DELETE_REQUESTCODE = 234324212
    private val CHECKOUT_DELETE_REQUESTCODE = 23432421
    private var mBuilder: NotificationCompat.Builder? = null
    private var mNotifyManager: NotificationManager? = null

    val channelId = "channel-attendance-sync-1"
    val channelName = "channel-name-attendance-sync-1"


    private val ATTENDANCE_SYNC_NOTIFICATION_ID: Int = 94812

    var notificationContentText = StringBuilder()


    private fun pendingDelete(deleteKey: String, deleteRequestCode: Int) {
        val intent = Intent(applicationContext, AttendanceDeleteReceiver::class.java)
        val todayDate: String = DateTimeUtils.DateToString(
            DateTimeUtils.getTodaysDateWithoutTime(),
            DateTimeUtils.DATE_FORMAT_DATABASE
        )
        intent.putExtra(DELETE_KEY, deleteKey)
        intent.putExtra(TODAY_DATE, todayDate)
        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext, deleteRequestCode, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val alarmManager =
            applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager[AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + TIME] = pendingIntent
    }

    private fun createNotification(): ForegroundInfo {
        mNotifyManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(channelId, channelName, importance)
            mNotifyManager!!.createNotificationChannel(mChannel)
        }
        mBuilder = NotificationCompat.Builder(applicationContext, channelId)
        mBuilder!!.setContentTitle(applicationContext.resources.getString(R.string.attendance))
            .setContentText(applicationContext.resources.getString(R.string.attendance_uploading))
            .setSmallIcon(R.drawable.ic_upload)

        val notification = mBuilder!!.build()
        mNotifyManager!!.notify(ATTENDANCE_SYNC_NOTIFICATION_ID, notification)

        return ForegroundInfo(ATTENDANCE_SYNC_NOTIFICATION_ID, notification)
    }

    private fun createForegroundInfo(progress: String): ForegroundInfo {

        val title = "Uploading Attendance"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }


        val notification = NotificationCompat.Builder(applicationContext, channelId)
            .setContentTitle(title)
            .setTicker(title)
            .setContentText(progress)
            .setSmallIcon(R.drawable.mref_logo)
            .setOngoing(true)
            .build()

        return ForegroundInfo(ATTENDANCE_SYNC_NOTIFICATION_ID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val name = applicationContext.getString(R.string.channel_name)
        val descriptionText = applicationContext.getString(R.string.channel_description)
        val importance = NotificationManager.IMPORTANCE_HIGH
        val mChannel = NotificationChannel(channelId, name, importance)
        mChannel.description = descriptionText
        mNotifyManager?.createNotificationChannel(mChannel)
    }


    private fun updateNotification(message: String, icon: Int, isSuccess: Boolean): ForegroundInfo {
        val text: String
        text = if (isSuccess) "Successfully uploaded" else "Upload failed"
        mBuilder!!.setContentText(text)
            .setSmallIcon(icon)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(message)
            )
        val notification = mBuilder!!.build()
        mNotifyManager!!.notify(ATTENDANCE_SYNC_NOTIFICATION_ID, notification)

        return ForegroundInfo(ATTENDANCE_SYNC_NOTIFICATION_ID, notification)
    }


    override suspend fun doWork(): Result {
        Log.d("attendance", "work started")
        (applicationContext as App).appComponent.inject(this)
        uploadAttendance()
        Log.d("attendance", "Uploaded")
        return Result.success()
    }

    suspend fun uploadAttendance() {
        val checkInAttendanceNotUploaded = attendanceDao.getCheckInAttendanceNotUploaded()
        val checkOutAttendanceNotUploaded = attendanceDao.getCheckOutAttendanceNotUploaded()

        if (checkInAttendanceNotUploaded.size > 0 || checkOutAttendanceNotUploaded.size > 0) {
            setForeground(createNotification())

            if (checkInAttendanceNotUploaded.size > 0) {
                    postCheckInAttendance(checkInAttendanceNotUploaded)
                pendingDelete(
                    CHECK_IN_EVENT,
                    CHECKIN_DELETE_REQUESTCODE
                )
            }

            if (checkOutAttendanceNotUploaded.size > 0) {
                postCheckOutAttendance(checkOutAttendanceNotUploaded)
                pendingDelete(
                    CHECK_OUT_EVENT,
                    CHECKOUT_DELETE_REQUESTCODE
                )
            }

        }

    }


    private suspend fun postCheckInAttendance(checkInAttendanceNotUploaded: List<AttendanceCheckIn>) {
        var successCount: Int = 0
        checkInAttendanceNotUploaded.forEach {
            val response = mainRespository.attendanceCheckInSync(it).execute()
            if (response.isSuccessful) {
                successCount++
                it.IsUploadedSuccess = 1
                attendanceDao.update(it)
                if (checkInAttendanceNotUploaded.size == successCount) {
                    setForeground(
                        updateNotification(
                            notificationContentText.append(
                                if (notificationContentText.toString().isEmpty()) "" else "\n"
                            ).append("Check In: Successfully uploaded").toString(),
                            R.drawable.ic_checked,
                            true
                        )
                    )
                    //  setForeground(createForegroundInfo("Check In: Successfully uploaded"))
                }
            }

        }
    }

    private suspend fun postCheckOutAttendance(checkOutAttendanceNotUploaded: List<AttendanceCheckOut>) {
        var successCount: Int = 0

        checkOutAttendanceNotUploaded.forEach {

            val response = mainRespository.attendanceCheckOutSync(it).execute()
            if (response.isSuccessful) {
                successCount++
                it.IsUploadedSuccess = 1
                attendanceDao.update(it)
                if (checkOutAttendanceNotUploaded.size == successCount) {
                    setForeground(
                        updateNotification(
                            notificationContentText.append(
                                if (notificationContentText.toString().isEmpty()) "" else "\n"
                            ).append("Check Out: Successfully uploaded").toString(),
                            R.drawable.ic_checked,
                            true
                        )
                    )
                    // setForeground(createForegroundInfo("Check Out: Successfully uploaded"))

                }
            }
        }

    }
}
