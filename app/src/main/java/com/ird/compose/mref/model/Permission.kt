package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class Permission(

    @SerializedName("description"    ) var description    : String  = "",
    @SerializedName("dateCreated"    ) var dateCreated    : String  = "",
    @SerializedName("isRetired"      ) var isRetired      : Boolean? = null,
    @SerializedName("dateRetired"    ) var dateRetired    : String  = "",
    @SerializedName("reasonRetired"  ) var reasonRetired  : String  = "",
    @SerializedName("permissionId"   ) var permissionId   : Int?     = null,
    @SerializedName("permissionName" ) var permissionName : String  = ""

)
