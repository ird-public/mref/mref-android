package com.ird.compose.mref.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.ird.compose.mref.db.Converter


@Entity
@TypeConverters(Converter::class)
data class ReferalReason (

	@PrimaryKey
	val referalReasonId : Int,
	val displayText : String,
	val dateCreated : String,
	val entityType : String,
	val name : String,
	val dateActivated : String,
	val isRetired : Boolean,
	val locale : String,
	val shortName : String
)