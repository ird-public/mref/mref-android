package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class SearchResults(
    @SerializedName("identifier" ) var identifier : String = "",
    @SerializedName("fatherName" ) var fatherName : String = "",
    @SerializedName("gender"     ) var gender     : String = "",
    @SerializedName("dob"        ) var dob        : String = "",
    @SerializedName("name"       ) var name       : String = ""
)
