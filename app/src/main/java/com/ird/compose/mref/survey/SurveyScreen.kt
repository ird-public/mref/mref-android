/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.survey

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.google.accompanist.insets.statusBarsPadding
import com.ird.compose.mref.R
import com.ird.compose.mref.model.SearchPayload
import com.ird.compose.mref.model.SearchResults
import com.ird.compose.mref.signinsignup.GradientButton
import com.ird.compose.mref.theme.progressIndicatorBackground
import com.ird.compose.mref.util.supportWideScreen

@Composable
fun SurveyQuestionsScreen(
    formType: FormType,
    questions: SurveyState.Questions,
    shouldAskPermissions: Boolean,
    onDoNotAskForPermissions: () -> Unit,
    onAction: (Int, SurveyActionType) -> Unit,
    onChange: (Int, String) -> Unit,
    onDonePressed: () -> Unit,
    onBackPressed: () -> Unit,
    openSettings: () -> Unit
) {
    val questionState = remember(questions.currentQuestionIndex) {
        questions.questionsState[questions.currentQuestionIndex]
    }

    Surface(modifier = Modifier.supportWideScreen()) {
        Scaffold(
            modifier = Modifier.statusBarsPadding()
                .navigationBarsWithImePadding()
            ,
            topBar = {
                if (formType != FormType.SEARCH) {
                    SurveyTopAppBar(
                        questionIndex = questions.questionCounter,
                        totalQuestionsCount = questionState.totalQuestionsCount,
                        onBackPressed = onBackPressed
                    )
                }
            },
            content = { innerPadding ->
                Question(
                    question = questionState.question,
                    answer = questionState.answer,
                    shouldAskPermissions = shouldAskPermissions,
                    onAnswer = {
                        if (it !is Answer.PermissionsDenied) {
                            questionState.answer = it
                        }
                        questionState.enableNext = true
                    },
                    onAction = onAction,
                    onInvalidAnswer = {
                        questionState.enableNext = false
                    },
                    openSettings = openSettings,
                    onDoNotAskForPermissions = onDoNotAskForPermissions,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            },
            bottomBar = {
                SurveyBottomBar(
                    questionState = questionState,
                    onPreviousPressed = {
                        val prevQuestionIndex = getPrevQuestionIndex(questionState, questions)
                        if (prevQuestionIndex != -1) {
                            questions.questionCounter = questions.questionCounter - 1
                            questions.currentQuestionIndex = prevQuestionIndex
                        } else {
                            onDonePressed()
                        }
                    },
                    onNextPressed = {

                        val nextQuestionIndex = getNextQuestionIndex(questionState, questions)

                        if (nextQuestionIndex != -1) {
                            questions.questionCounter = questions.questionCounter + 1
                            questions.currentQuestionIndex = nextQuestionIndex

                        } else {
                            onDonePressed()
                        }
                    },
                    onDonePressed = onDonePressed
                )
            }
        )
    }
}

fun getNextQuestionIndex(questionState: QuestionState, questions: SurveyState.Questions): Int {
    var questionIndex: Int;
    var state: QuestionState;


    val questionList =
        questions.questionsState.filter { mQuestion -> mQuestion.question.id == questionState.question.nextQuestion }

    if (!questionList.isEmpty()) {
        state = questionList.first()
        questionIndex = state.questionIndex

    } else {
        questionIndex = -1
    }
    return questionIndex;
}

fun getPrevQuestionIndex(questionState: QuestionState, questions: SurveyState.Questions): Int {
    var questionIndex: Int;
    var state: QuestionState;

    val questionList =
        questions.questionsState.filter { mQuestion -> mQuestion.question.id == questionState.question.prevQuestion }


    if (!questionList.isEmpty()) {
        state = questionList.first()
        questionIndex = state.questionIndex
    } else {
        questionIndex = 0
    }

    return questionIndex;
}


@Composable
fun SurveyResultScreen(
    result: SurveyState.Result,
    status: String,
    onUploadPressed: (SurveyState.Result) -> Unit,
    onSavedPressed: (SurveyState.Result) -> Unit,
    onPreviousPressed: () -> Unit,
    onClosedPressed: () -> Unit,
) {
    Surface(modifier = Modifier.supportWideScreen()) {
        Scaffold(
            modifier = Modifier.statusBarsPadding()
                .navigationBarsPadding(),
            content = { innerPadding ->
                val modifier = Modifier.padding(start = 0.dp, top = 0.dp,
                    end = 0.dp, bottom = 50.dp)

                SurveyResult(result = result, status = status,modifier = modifier)
            },
            bottomBar = {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp, vertical = 24.dp)
                ) {
                    if (status.equals("")) {

                        OutlinedButton(
                            onClick = onPreviousPressed,
                            modifier = Modifier
                                .weight(1f)
                                .height(48.dp),

                            ) {
                            Text(text = stringResource(id = R.string.back))
                        }
                        Spacer(modifier = Modifier.width(16.dp))

                        GradientButton(
                            modifier = Modifier
                                .weight(1f)
                                .height(48.dp),
                            enabled = true,
                            text = stringResource(id = R.string.save),
                            onClick = { onSavedPressed(result) }
                        )
                        Spacer(modifier = Modifier.width(16.dp))

                        GradientButton(
                            modifier = Modifier
                                .weight(1f)
                                .height(48.dp),
                            enabled = true,
                            text = stringResource(id = R.string.upload),
                            onClick = { onUploadPressed(result) }
                        )
                    } else if(status.equals("saved") || status.equals("success") ){
                        GradientButton(
                            modifier = Modifier
                                .weight(1f)
                                .height(48.dp),
                            enabled = true,
                            text = stringResource(id = R.string.close),
                            onClick = { onClosedPressed() }
                        )
                    } else {
                        GradientButton(
                            modifier = Modifier
                                .weight(1f)
                                .height(48.dp),
                            enabled = true,
                            text = stringResource(id = R.string.save),
                            onClick = { onSavedPressed(result) }
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        GradientButton(
                            modifier = Modifier
                                .weight(1f)
                                .height(48.dp),
                            enabled = true,
                            text = stringResource(id = R.string.close),
                            onClick = { onClosedPressed() }
                        )
                    }
                }
            }
        )
    }
}


@Composable
fun ResultItem(
    modifier: Modifier,
    iconId: Int,
    name: String,
    id: String,
    fatherName: String,
    date: String,
) {

    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(5.dp),
        contentColor = colorResource(id = R.color.blue_dark),
        backgroundColor = Color.Transparent,
        shape = RoundedCornerShape(7.dp),
        content = {
            Row(
                Modifier
                    .fillMaxWidth()
                    .background(
                        Brush.linearGradient(
                            listOf(
                                Color(0xFFEDF4F7),
                                Color(0xFFBEE4F3),

                                )
                        )
                    )
                    .padding(10.dp),
                //Arrangement.Center

            ) {
                Image(
                    modifier = Modifier

                        .padding(10.dp)
                        .width(50.dp)
                        .height(50.dp)
                        .align(Alignment.CenterVertically),
                    painter = painterResource(id = iconId),

                    contentDescription = "form",


                    )

                Column() {
                    Text(
                        text = name,
                        modifier = Modifier
                            .fillMaxWidth(),
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.Bold,
                        style = MaterialTheme.typography.body1
                    )


                    Text(
                        text = "Father name : $fatherName",
                        modifier = Modifier
                            .fillMaxWidth(),
                        color = colorResource(id = R.color.grey_dark),
                        textAlign = TextAlign.Justify,
                        style = MaterialTheme.typography.body2
                    )

                    Text(
                        text = "ID # $id",
                        modifier = Modifier
                            .fillMaxWidth(),
                        color = colorResource(id = R.color.grey_dark),
                        textAlign = TextAlign.Justify,
                        style = MaterialTheme.typography.body2
                    )


                    Text(
                        text = "DOB : $date",
                        modifier = Modifier
                            .fillMaxWidth(),
                        color = colorResource(id = R.color.grey_dark),
                        textAlign = TextAlign.Justify,
                        style = MaterialTheme.typography.body2
                    )

                }

            }

        }

    )


}


@Composable
fun SearchResultScreen(
    result: List<SearchResults>,
    onPreviousPressed: () -> Unit,
    onItemClicked: (SearchResults) -> Unit,
) {
    Surface(modifier = Modifier.supportWideScreen()) {
        Scaffold(
            modifier = Modifier.statusBarsPadding()
                .navigationBarsPadding(),
            content = { innerPadding ->
                val modifier = Modifier.padding(innerPadding)

                LazyColumn(
                    modifier = modifier
                        .fillMaxSize()
                        .padding(vertical = 40.dp, horizontal = 20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.spacedBy(4.dp)
                ) {
                    items(items = result) { item ->
                        ResultItem(
                            modifier = Modifier
                                .fillMaxWidth()
                                .clickable(onClick = { onItemClicked(item) }),
                            iconId = if (item.gender == "MALE") R.drawable.child_male else R.drawable.child_female,
                            name = item.name,
                            id = item.identifier,
                            fatherName = item.fatherName,
                            date = item.dob
                        )
                    }
                }

            },
            bottomBar = {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp, vertical = 24.dp)
                ) {


                    OutlinedButton(
                        onClick = onPreviousPressed,
                        modifier = Modifier
                            .weight(1f)
                            .height(48.dp),

                        ) {
                        Text(text = stringResource(id = R.string.back))
                    }
                }
            },
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = "Search Results",
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .fillMaxSize()
                                .wrapContentSize(Alignment.Center)
                        )
                    },
                    navigationIcon = {
                    },
                    // We need to balance the navigation icon, so we add a spacer.
                    actions = {
                        Spacer(modifier = Modifier.width(68.dp))
                    },
                    backgroundColor = MaterialTheme.colors.surface,
                    elevation = 0.dp
                )
            },

            )

    }

}


@Composable
fun SearchFiltersScreen(
    date: String,
    referralType: String,
    onAction: (SurveyActionType) -> Unit,
    onPreviousPressed: () -> Unit,
    onSearch: (SearchPayload) -> Unit
) {
    Surface(modifier = Modifier.supportWideScreen()) {
        val searchPayload = SearchPayload()

        Scaffold(
              modifier = Modifier.statusBarsPadding()
                .navigationBarsPadding(),
            bottomBar = {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp, vertical = 24.dp)
                ) {
                    OutlinedButton(
                        onClick = onPreviousPressed,
                        modifier = Modifier
                            .weight(1f)
                            .height(48.dp),

                        ) {
                        Text(text = stringResource(id = R.string.back))
                    }
                    Spacer(modifier = Modifier.width(16.dp))

                    GradientButton(
                        modifier = Modifier
                            .weight(1f)
                            .height(48.dp),
                        enabled = true,
                        text = stringResource(id = R.string.search),
                        onClick = { onSearch(searchPayload) }
                    )
                }
            },
            content = { innerPadding ->
                val modifier = Modifier.padding(innerPadding)


                LazyColumn(
                    modifier = modifier
                        .fillMaxSize()
                        .padding(vertical = 40.dp, horizontal = 20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {

                    item {
                        Icon(
                            modifier = Modifier.fillMaxWidth(),
                            painter = painterResource(id = R.drawable.ic_filter),
                            contentDescription = "Localized description",
                            tint = colorResource(id = R.color.blue)
                        )

                        Text(
                            text = "Search Filters",
                            style = MaterialTheme.typography.h5,
                            modifier = Modifier.padding(horizontal = 10.dp)
                        )

                        Spacer(modifier = Modifier.height(20.dp))


                        SearchFilterQuestion(
                            modifier = Modifier.fillMaxWidth(),
                            date = date,
                            referralType = referralType,
                            onAction = onAction,
                            searchPayload = searchPayload
                        )
                    }
                }
            }
        )
    }
}

@Composable
fun SearchFilterQuestion(
    modifier: Modifier,
    date: String,
    referralType: String,
    onAction: (SurveyActionType) -> Unit,
    searchPayload: SearchPayload
) {


    val radioOptions = listOf(
        stringResource(id = R.string.male),
        stringResource(id = R.string.female),
    )

    searchPayload.dob = date

    val mDate = remember { mutableStateOf(searchPayload.dob) }

    var name by remember { mutableStateOf(searchPayload.childName) }
    var fatherName by remember { mutableStateOf(searchPayload.fatherName) }
    var husbandName by remember { mutableStateOf(searchPayload.husbandFirstName) }
    var cnic by remember { mutableStateOf(searchPayload.fatherNIC) }
    var phone by remember { mutableStateOf(searchPayload.reminderPrimaryNo) }
    val (selectedOption, onOptionSelected) = remember { mutableStateOf(searchPayload.childGender) }




    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {


        // NAME & FATHER NAME

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {

            OutlinedTextField(
                value = name,

                onValueChange = {
                    name = it
                    searchPayload.childName = it
                    searchPayload.womenFirstName = it
                },
                label = { Text(text = stringResource(id = R.string.name)) },
                maxLines = 1,
                textStyle = MaterialTheme.typography.caption,
                modifier = modifier
                    .padding(vertical = 5.dp)
                    .height(54.dp)
                    .weight(1.0f),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Text
                )
            )

            androidx.compose.foundation.layout.Spacer(
                modifier = androidx.compose.ui.Modifier.padding(
                    horizontal = 10.dp
                )
            )
            OutlinedTextField(
                value = fatherName,

                onValueChange = {
                    fatherName = it
                    searchPayload.fatherName = it
                    searchPayload.fatherFirstName = it
                },
                label = { Text(text = stringResource(id = R.string.father_name)) },
                maxLines = 1,
                textStyle = MaterialTheme.typography.caption,
                modifier = modifier

                    .padding(vertical = 5.dp)
                    .height(54.dp)
                    .weight(1.0f),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Text
                )
            )

        }


        // HUSBAND NAME
        if (!referralType.equals("child", true)) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {

                OutlinedTextField(
                    value = husbandName,


                    onValueChange = {
                        husbandName = it
                        searchPayload.husbandFirstName = it

                    },
                    label = { Text(text = stringResource(id = R.string.husband_name)) },
                    maxLines = 1,
                    textStyle = MaterialTheme.typography.caption,
                    modifier = modifier

                        .padding(vertical = 5.dp)
                        .height(54.dp),
                    keyboardOptions = KeyboardOptions.Default.copy(
                        imeAction = ImeAction.Done,
                        keyboardType = KeyboardType.Text
                    )
                )
                androidx.compose.foundation.layout.Spacer(
                    modifier = androidx.compose.ui.Modifier.padding(
                        horizontal = 10.dp
                    )
                )
            }

        }
        // CNIC

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {

            OutlinedTextField(
                value = cnic,

                onValueChange = {
                    cnic = it
                    searchPayload.fatherNIC = it
                    searchPayload.nic = it
                },
                label = { Text(text = stringResource(id = R.string.cnic)) },
                maxLines = 1,
                textStyle = MaterialTheme.typography.caption,
                modifier = modifier

                    .padding(vertical = 5.dp)
                    .height(54.dp),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Number
                )
            )
            androidx.compose.foundation.layout.Spacer(
                modifier = androidx.compose.ui.Modifier.padding(
                    horizontal = 10.dp
                )
            )
        }


        // phone No
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {

            OutlinedTextField(
                value = phone,

                onValueChange = {
                    phone = it
                    searchPayload.reminderPrimaryNo = it
                },
                label = { Text(text = stringResource(id = R.string.primary)) },
                maxLines = 1,
                textStyle = MaterialTheme.typography.caption,
                modifier = modifier

                    .padding(vertical = 5.dp)
                    .height(54.dp),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done,
                    keyboardType = KeyboardType.Number
                )
            )
            androidx.compose.foundation.layout.Spacer(
                modifier = androidx.compose.ui.Modifier.padding(
                    horizontal = 10.dp
                )
            )
        }


        //AGE

        Text(
            text = "Date of birth",
            style = MaterialTheme.typography.caption,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 10.dp, horizontal = 0.dp)
                .align(Alignment.End)
        )

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {


            Surface(
                //onClick = { onAction(questionId, SurveyActionType.PICK_DATE) },

                shape = MaterialTheme.shapes.small,
                modifier = modifier
                    .height(54.dp)
                    .weight(0.6f),
                border = BorderStroke(1.dp, MaterialTheme.colors.onSurface.copy(alpha = 0.12f))

            ) {
                Text(
                    text = mDate.value,
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
                        .padding(10.dp),
                    textAlign = TextAlign.Center

                )

            }

            androidx.compose.foundation.layout.Spacer(
                modifier = androidx.compose.ui.Modifier.padding(
                    horizontal = 10.dp
                )
            )

            IconButton(
                modifier = Modifier.weight(0.2f),
                onClick = { onAction(SurveyActionType.PICK_DATE()) },
            ) {
                Column() {


                    Icon(
                        painter = painterResource(id = R.drawable.ic_calendar),
                        contentDescription = null,
                        modifier = Modifier
                            .height(25.dp)
                            .width(25.dp)
                            .align(Alignment.CenterHorizontally)

                    )
                    Text(
                        "Calendar",
                        style = MaterialTheme.typography.caption,
                        textAlign = TextAlign.Center
                    )

                }
            }

            IconButton(
                modifier = Modifier.weight(0.2f),
                onClick = { onAction(SurveyActionType.AGE_PICKER()) },
            ) {
                Column() {

                    Icon(
                        painter = painterResource(id = R.drawable.ic_calculator),
                        contentDescription = null,
                        modifier = Modifier
                            .height(25.dp)
                            .width(25.dp)
                            .align(Alignment.CenterHorizontally)
                    )

                    Text(
                        "Age",
                        // modifier = modifier.weight(0.5f),
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.caption
                    )


                }
            }

        }

        // GENDER

        if (referralType.equals("child", true)) {
            Text(
                text = "Gender",
                style = MaterialTheme.typography.caption,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 5.dp, horizontal = 0.dp)
            )

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                radioOptions.forEach { text ->
                    val onClickHandle = {
                        onOptionSelected(text)
                        searchPayload.childGender = text.uppercase()
                        /* options[text]?.let { onAnswerSelected(it.first, it.second) }
                     Unit*/
                    }
                    val optionSelected = text == selectedOption

                    val answerBorderColor = if (optionSelected) {
                        MaterialTheme.colors.primary.copy(alpha = 0.5f)
                    } else {
                        MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
                    }
                    val answerBackgroundColor = if (optionSelected) {
                        MaterialTheme.colors.primary.copy(alpha = 0.12f)
                    } else {
                        MaterialTheme.colors.background
                    }
                    Surface(
                        shape = MaterialTheme.shapes.small,
                        border = BorderStroke(
                            width = 1.dp,
                            color = answerBorderColor
                        ),
                        modifier = Modifier
                            .padding(vertical = 3.dp)
                            .weight(1.0f)
                    ) {
                        Row(
                            modifier = Modifier

                                .selectable(
                                    selected = optionSelected,
                                    onClick = onClickHandle
                                )
                                .background(answerBackgroundColor)
                                .padding(vertical = 4.dp, horizontal = 8.dp),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Text(
                                text = text
                            )

                            RadioButton(
                                selected = optionSelected,
                                onClick = onClickHandle,
                                colors = RadioButtonDefaults.colors(
                                    selectedColor = MaterialTheme.colors.primary
                                )
                            )
                        }
                    }
                }

            }
        }

    }


}


@Preview
@Composable
private fun showResult() {

    SurveyTopAppBar(
        1,
        2,{

        }
    )

    /*  SurveyResult(
          result = SurveyState.Result(
              R.string.referral_form,
              SurveyResult("", "", ChildPayload(), R.string.child)
          )
      )*/

/*    SearchFiltersScreen(
        referralType = "Child",
        onAction = {},
        date = "",
        onPreviousPressed = {}
    )
    {

    }*/
}


@Composable
private fun SurveyResult(
    result: SurveyState.Result,
    status: String = "",
    modifier: Modifier = Modifier

) {

    var iconRes: Int = R.drawable.ic_form
    var tintColor: Int = R.color.blue
    var responseMessage: String = ""


    if (status.equals("success")) {
        iconRes = R.drawable.ic_checked
        tintColor = R.color.green
        responseMessage = "Submitted successfully"
    } else if (status.equals("saved")) {
        iconRes = R.drawable.ic_checked
        tintColor = R.color.green
        responseMessage = "Saved successfully"
    } else if (status.equals("failure")) {
        iconRes = R.drawable.ic_cancel
        tintColor = R.color.red_image
        responseMessage = "Submission failed"
    }/* else {
        iconRes = R.drawable.ic_form
    }*/

    LazyColumn(
        modifier = modifier
            .fillMaxSize()
            .padding(vertical = 40.dp, horizontal = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item {


            Icon(
                modifier = Modifier.fillMaxWidth(),
                painter = painterResource(id = iconRes),
                contentDescription = "Localized description",
                tint = colorResource(id = tintColor)
            )

            Text(
                text = responseMessage,
                style = MaterialTheme.typography.h5,
                modifier = Modifier.padding(horizontal = 20.dp)
            )

            Spacer(modifier = Modifier.height(44.dp))

            Text(
                text = result.surveyResult.formName,
                style = MaterialTheme.typography.h4,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(horizontal = 20.dp)
            )
            //result.surveyResult.

            Text(
                text = result.surveyResult.result,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(20.dp)
            )
            /* Text(
                 text = "",
                 style = MaterialTheme.typography.body1,
                 modifier = Modifier.padding(horizontal = 20.dp)
             )*/
        }
    }
}

@Composable
private fun TopAppBarTitle(
    questionIndex: Int,
    totalQuestionsCount: Int,
    modifier: Modifier = Modifier
) {
    val indexStyle = MaterialTheme.typography.caption.toSpanStyle().copy(
        fontWeight = FontWeight.Bold
    )
    val totalStyle = MaterialTheme.typography.caption.toSpanStyle()
    val text = buildAnnotatedString {
        withStyle(style = indexStyle) {
            append("Question # ${questionIndex + 1}")
        }
      /*  withStyle(style = totalStyle) {
            append(stringResource(R.string.question_count, totalQuestionsCount))
        }*/
    }
    Text(
        text = text,
        style = MaterialTheme.typography.caption,
        modifier = modifier
    )
}

@Composable
private fun SurveyTopAppBar(
    questionIndex: Int,
    totalQuestionsCount: Int,
    onBackPressed: () -> Unit
) {
    Column(modifier = Modifier.fillMaxWidth()) {
        Box(modifier = Modifier.fillMaxWidth()) {
            TopAppBarTitle(
                questionIndex = questionIndex,
                totalQuestionsCount = totalQuestionsCount,
                modifier = Modifier
                    .padding(vertical = 20.dp)
                    .align(Alignment.Center)
            )

            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                IconButton(
                    onClick = onBackPressed,
                    modifier = Modifier
                        .padding(horizontal = 20.dp, vertical = 20.dp).align(Alignment.CenterEnd)
                        //.fillMaxWidth()
                ) {
                    Icon(
                        Icons.Filled.Close,
                        contentDescription = stringResource(id = R.string.close),
                        modifier = Modifier.align(Alignment.CenterEnd)
                    )
                }
            }
        }
        val animatedProgress by animateFloatAsState(
            targetValue = (questionIndex + 1) / totalQuestionsCount.toFloat(),
            animationSpec = ProgressIndicatorDefaults.ProgressAnimationSpec
        )
        LinearProgressIndicator(
            progress = animatedProgress,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp),
            backgroundColor = MaterialTheme.colors.progressIndicatorBackground
        )
    }
}

@Composable
private fun SurveyBottomBar(
    questionState: QuestionState,
    onPreviousPressed: () -> Unit,
    onNextPressed: () -> Unit,
    onDonePressed: () -> Unit
) {
    Surface(
        elevation = 7.dp,
        modifier = Modifier.fillMaxWidth() // .border(1.dp, MaterialTheme.colors.primary)
    ) {

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 20.dp)
        ) {
            if (questionState.showPrevious) {
                OutlinedButton(
                    modifier = Modifier
                        .weight(1f)
                        .height(48.dp),
                    onClick = onPreviousPressed
                ) {
                    Text(text = stringResource(id = R.string.previous))
                }
                Spacer(modifier = Modifier.width(16.dp))
            }
            if (questionState.showDone) {
                GradientButton(
                    modifier = Modifier
                        .weight(1f)
                        .height(48.dp),
                    onClick = onDonePressed,
                    text = stringResource(id = R.string.done),
                    enabled = questionState.enableNext
                )
            } else {
                GradientButton(
                    modifier = Modifier
                        .weight(1f)
                        .height(48.dp),
                    onClick = onNextPressed,
                    text = stringResource(id = R.string.next),
                    enabled = questionState.enableNext
                )
            }
        }
    }
}
