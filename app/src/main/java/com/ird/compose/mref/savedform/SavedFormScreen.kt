package com.ird.compose.mref.savedform

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.material.icons.filled.History
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.navigationBarsWithImePadding
import com.google.accompanist.insets.statusBarsPadding
import com.ird.compose.mref.R
import com.ird.compose.mref.home.HistoryItem
import com.ird.compose.mref.model.OfflineForm
import com.ird.compose.mref.signinsignup.GradientButton
import com.ird.compose.mref.util.supportWideScreen


@Composable
public fun SavedForm(forms: List<OfflineForm>,onUpload: () -> Unit,onBackPressed: ()-> Unit) {
    Surface(modifier = Modifier.supportWideScreen().fillMaxSize()) {
        Scaffold(
            modifier = Modifier.statusBarsPadding()
                .navigationBarsWithImePadding(),
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = "Saved Forms",
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .fillMaxSize()
                                .wrapContentSize(Alignment.Center)
                        )
                    },
                    navigationIcon = {
                        IconButton(onClick = onBackPressed) {
                            Icon(
                                imageVector = Icons.Filled.ChevronLeft,
                                contentDescription = stringResource(id = R.string.back)
                            )
                        }
                    },
                    // We need to balance the navigation icon, so we add a spacer.
                    actions = {
                        Spacer(modifier = Modifier.width(68.dp))
                    },
                    backgroundColor = MaterialTheme.colors.surface,
                    elevation = 0.dp
                )
            },
            content = {
                if (forms.isEmpty()) {

                    Box(modifier = Modifier.fillMaxHeight(), contentAlignment = Alignment.Center) {
                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            Icon(imageVector = Icons.Default.History, contentDescription = "hisory")

                            Text(
                                "No Saved Forms Found",
                                modifier = Modifier
                                    .fillMaxWidth(),
                                textAlign = TextAlign.Center
                            )
                        }
                    }

                } else {
                    LazyColumn(verticalArrangement = Arrangement.spacedBy(4.dp), modifier = Modifier.padding(horizontal = 10.dp))
                    {
                        items(forms) { form ->

                            HistoryItem(
                                modifier = Modifier.fillMaxWidth(),
                                iconId = if (form.payload.referalEntity != "child") R.drawable.woman else if (form.payload.gender == "MALE") R.drawable.child_male else R.drawable.child_female,
                                name = form.payload.name,
                                id = form.payload.identifier,
                                referTo = form.payload.centerName,
                                date = form.payload.dateRefered
                            )

                        }
                    }
                }
            },
            bottomBar = {
               if(forms.size > 0) {
                   Row(
                       modifier = Modifier
                           .fillMaxWidth()
                           .padding(horizontal = 20.dp, vertical = 24.dp)
                   ) {
                       GradientButton(
                           modifier = Modifier.fillMaxWidth()
                               .height(48.dp),
                           onClick = onUpload,
                           text = stringResource(id = R.string.uploadAll)
                       )
                   }
               }
            }
        )
    }
}