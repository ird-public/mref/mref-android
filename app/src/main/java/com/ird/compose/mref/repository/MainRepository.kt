package com.ird.compose.mref.repository

import androidx.compose.runtime.Immutable
import com.ird.compose.mref.model.*
import com.ird.compose.mref.network.ApiService
import retrofit2.Call
import retrofit2.Response


sealed class User {
    @Immutable
    data class LoggedInUser(val email: String) : User()
    object NoUserLoggedIn : User()
}


class MainRepository constructor(private val apiService: ApiService) {

    suspend fun getLocations(): Response<MRefMetadata> = apiService.getLocations()


    suspend fun getUser(
        username: String,
        password: String
    ): Response<com.ird.compose.mref.model.User> {
        return apiService.getUser(username, password)
    }

    suspend fun getPatientDetails(identifier: String): Response<Patient> {
        return apiService.getPatient(identifier)
    }

    suspend fun getFollowupDetails(identifier: String): Response<ChildPayload> {
        return apiService.getFollowUpPatient(identifier)
    }

    suspend fun submitReferralSlip(childPayload: ChildPayload): Response<Any> {
        return apiService.submitReferralSlip(childPayload)
    }

    suspend fun submitFollowup(childPayload: ChildPayload): Response<Any> {
        return apiService.submitFollowUp(childPayload)
    }

    suspend fun search(type: String, searchPayload: SearchPayload): Response<List<SearchResults>> {
        val path = if (type.equals("child", ignoreCase = true)) {
            "search"
        } else {
            "womansearch"
        }

        return apiService.search(path, searchPayload)
    }

    fun submitFollowUpSync(childPayload: ChildPayload): Call<Any> {
        return apiService.submitFollowUpSync(childPayload)
    }

    fun submitReferralSlipSync(childPayload: ChildPayload): Call<Any> {
        return apiService.submitReferralSlipSync(childPayload)
    }

    fun attendanceCheckInSync(attendanceCheckIn: AttendanceCheckIn): Call<Void> {
        return apiService.attendanceCheckInSync(attendanceCheckIn)
    }

    fun attendanceCheckOutSync(attendanceCheckOut: AttendanceCheckOut): Call<Void> {
        return apiService.attendanceCheckOutSync(attendanceCheckOut)
    }

    suspend fun attendanceCheckIn(attendanceCheckIn: AttendanceCheckIn): Response<Any>{
        return apiService.attendanceCheckIn(attendanceCheckIn)
    }

    suspend fun attendanceCheckOut(attendanceCheckOut: AttendanceCheckOut): Response<Any>{
        return apiService.attendanceCheckOut(attendanceCheckOut)
    }

    suspend fun getStatistics(userId: Int): Response<StatisticsResponse>{
        return apiService.getStatistics(userId = userId)
    }

    private var _user: User = User.NoUserLoggedIn
    val user: User
        get() = _user


    @Suppress("UNUSED_PARAMETER")
    fun signIn(email: String, password: String) {
        //TODO call data
        _user = User.LoggedInUser(email)
    }


}