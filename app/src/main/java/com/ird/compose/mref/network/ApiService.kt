package com.ird.compose.mref.network


import com.google.gson.JsonObject
import com.ird.compose.mref.model.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("api/metadata")
    suspend fun getLocations(): Response<MRefMetadata>

    @POST("api/login")
    suspend fun getUser(
        @Query("username") username: String,
        @Query("password") password: String
    ): Response<User>


    @GET("child/fetch")
    suspend fun getPatient(@Query("zmId") identifier: String): Response<Patient>

    @GET("child/fetch/mref")
    suspend fun getFollowUpPatient(
        @Query("mrefId") mrefId: String
    ): Response<ChildPayload>


    @POST("child/enrollment")
    suspend fun submitReferralSlip(@Body payload: ChildPayload): Response<Any>


    @POST("child/enrollment")
    fun submitReferralSlipSync(@Body payload: ChildPayload): Call<Any>


    @POST("child/update")
    fun submitFollowUpSync(@Body childPayload: ChildPayload): Call<Any>

    @POST("api/attendance/checkIn")
    fun attendanceCheckInSync(@Body attendanceCheckIn: AttendanceCheckIn): Call<Void>

    @POST("api/attendance/checkOut")
    fun attendanceCheckOutSync(@Body attendanceCheckOut: AttendanceCheckOut): Call<Void>


    @POST("child/update")
    suspend fun submitFollowUp(@Body childPayload: ChildPayload): Response<Any>


    @POST("child/{type}")
    suspend fun search(
        @Path(value = "type") type: String,
        @Body searchPayload: SearchPayload
    ): Response<List<SearchResults>>

    @POST("api/attendance/checkIn")
    suspend fun attendanceCheckIn(@Body attendanceCheckIn: AttendanceCheckIn): Response<Any>

    @POST("api/attendance/checkOut")
    suspend fun attendanceCheckOut(@Body attendanceCheckOut: AttendanceCheckOut): Response<Any>

    @GET("api/report/summary/{userId}")
    suspend fun getStatistics( @Path(value = "userId") userId: Int): Response<StatisticsResponse>

//mref/api/
}