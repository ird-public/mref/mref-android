package com.ird.compose.mref.network

interface OnResponseReceivedListener {
    fun onSuccessReceived(response: Any?)
    fun onFailureReceived(errorMessage: String?, responseCode: Int)
}