package com.ird.compose.mref.db.dao

import androidx.room.*
import com.ird.compose.mref.model.AttendanceCheckIn
import com.ird.compose.mref.model.AttendanceCheckOut

@Dao
interface AttendanceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(attendance: AttendanceCheckIn)

    @Query("SELECT * FROM attendance_check_in WHERE status = 1")
    fun getCheckInAttendance(): List<AttendanceCheckIn>

    @Delete
    fun deleteAllUploadedCheckIns(attendance: List<AttendanceCheckIn>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(attendanceCheckOut: AttendanceCheckOut)

    @Delete
    fun deleteAllUploadedCheckOuts(model: List<AttendanceCheckOut>)

    @Query("SELECT * FROM attendance_check_out WHERE status = 0")
    fun getCheckOutAttendance(): List<AttendanceCheckOut>

    @Query("SELECT * FROM attendance_check_in WHERE mappedId = :userId AND status = 1 AND attendanceDate = :date")
    fun getTodayCheckInByUserId(userId: Int, date: String): AttendanceCheckIn

    @Query("SELECT * FROM attendance_check_out WHERE mappedId = :userId AND status = 0 AND attendanceDate = :date")
    fun getTodayCheckOutByUserId(userId: Int, date: String): AttendanceCheckOut

    @Query("SELECT * FROM attendance_check_in WHERE status = 1 AND IsUploadedSuccess = 0")
    fun getCheckInAttendanceNotUploaded(): List<AttendanceCheckIn>

    @Query("SELECT * FROM attendance_check_out WHERE status = 0 AND IsUploadedSuccess = 0")
    fun getCheckOutAttendanceNotUploaded(): List<AttendanceCheckOut>

    @Query("SELECT * FROM attendance_check_in WHERE status = 1 AND IsUploadedSuccess = 1")
    fun getCheckInAttendanceUploaded(): List<AttendanceCheckIn>

    @Query("SELECT * FROM attendance_check_out WHERE status = 0 AND IsUploadedSuccess = 1")
    fun getCheckOutAttendanceUploaded(): List<AttendanceCheckOut>

/*
    fun setIsUploadedSuccessCheckIn(){
       val checkIns: List<AttendanceCheckIn> = getCheckInAttendance()
        for (i in checkIns.indices){
            checkIns[i].IsUploadedSuccess = 1
            update(checkIns.get(i))
        }
    }

    fun setIsUploadedSuccessCheckOut(){
        val checkOuts: List<AttendanceCheckOut> = getCheckOutAttendance()
        for (i in checkOuts.indices){
            checkOuts[i].IsUploadedSuccess = 1
            update(checkOuts.get(i))
        }
    }
*/

    @Update(entity = AttendanceCheckIn::class)
    fun update(checkIn: AttendanceCheckIn)

    @Update(entity = AttendanceCheckOut::class)
    fun update(checkOut: AttendanceCheckOut)
}