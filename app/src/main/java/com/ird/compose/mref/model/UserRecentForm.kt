package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class UserRecentForm (

    @SerializedName("referredType"  ) var referredType  : String= "",
    @SerializedName("referTo"       ) var referTo       : String= "",
    @SerializedName("gender"        ) var gender        : String= "",
    @SerializedName("encounterType" ) var encounterType : String= "",
    @SerializedName("createdDate"   ) var createdDate   : String= "",
    @SerializedName("dob"           ) var dob           : String= "",
    @SerializedName("name"          ) var name          : String= "",
    @SerializedName("id"            ) var id            : String= ""

)