package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName

data class SearchPayload(

    @SerializedName("childName"         ) var childName         : String = "",
    @SerializedName("fatherNIC"         ) var fatherNIC         : String = "",
    @SerializedName("fatherName"        ) var fatherName        : String = "",
    @SerializedName("dob"               ) var dob               : String = "",
    @SerializedName("childGender"       ) var childGender       : String = "",
    @SerializedName("town"              ) var town              : String = "",
    @SerializedName("district"          ) var district          : String = "",
    @SerializedName("uc"                ) var uc                : String = "",
    @SerializedName("reminderPrimaryNo" ) var reminderPrimaryNo : String = "",
    @SerializedName("womenFirstName"    ) var womenFirstName    : String = "",
    @SerializedName("nic"               ) var nic               : String = "",
    @SerializedName("fatherFirstName"   ) var fatherFirstName   : String = "",
    @SerializedName("husbandFirstName"  ) var husbandFirstName  : String = "",
)