package com.ird.compose.mref.service;


import static com.ird.compose.mref.util.Constants.CHECK_IN_EVENT;
import static com.ird.compose.mref.util.Constants.CHECK_OUT_EVENT;
import static com.ird.compose.mref.util.Constants.DELETE_KEY;
import static com.ird.compose.mref.util.Constants.TODAY_DATE;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ird.compose.mref.db.dao.AttendanceDao;
import com.ird.compose.mref.model.AttendanceCheckIn;
import com.ird.compose.mref.model.AttendanceCheckOut;

import java.util.List;

import javax.inject.Inject;

public class AttendanceDeleteReceiver extends BroadcastReceiver {

    @Inject
    AttendanceDao attendanceDao;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            String date = intent.getExtras().getString(TODAY_DATE);
            String deleteKey = intent.getExtras().getString(DELETE_KEY);

            if (date != null && deleteKey != null) {
                switch (deleteKey) {
                    case CHECK_IN_EVENT:
                        List<AttendanceCheckIn> checkInAttendanceNotUploaded = attendanceDao.getCheckInAttendanceUploaded();
                        attendanceDao.deleteAllUploadedCheckIns(checkInAttendanceNotUploaded);
                        break;
                    case CHECK_OUT_EVENT:
                        List<AttendanceCheckOut> checkOutAttendanceNotUploaded = attendanceDao.getCheckOutAttendanceUploaded();
                        attendanceDao.deleteAllUploadedCheckOuts(checkOutAttendanceNotUploaded);
                        break;
                }
            }
        }
    }
}