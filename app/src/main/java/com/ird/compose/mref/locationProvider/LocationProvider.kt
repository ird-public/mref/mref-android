package com.ird.compose.mref.locationProvider

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult

class LocationProvider {

    lateinit var client: FusedLocationProviderClient

    fun init(client: FusedLocationProviderClient ) {
        this.client = client
    }

    @SuppressLint("MissingPermission")
    fun getUserLocation(
        locationListener: ILocationListener
    ) {
        val request: LocationRequest
        request = LocationRequest()
        request.interval = 5000
        request.fastestInterval = 4000
        request.smallestDisplacement = 1f
        request.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        client.requestLocationUpdates(request, object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                val callback: LocationCallback = this
                locationListener.onLocationReceived(locationResult, callback)
            }
        }, Looper.myLooper()!!)
    }

    fun stopUpdates(locationCallback: LocationCallback?) {
        client.removeLocationUpdates(locationCallback!!)
    }


}