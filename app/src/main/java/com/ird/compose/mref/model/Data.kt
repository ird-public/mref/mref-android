package com.ird.compose.mref.model

import com.google.gson.annotations.SerializedName


data class Data(

    @SerializedName("fatherName") var fatherName: String?= "",

    @SerializedName("districtId") var districtId: Int = -1,
    @SerializedName("address") var address: String = "",
    @SerializedName("husbandFirstName") var husbandFirstName: String? = "",
    @SerializedName("gender") var gender: String = "",
    @SerializedName("dob") var dob: String = "",
    @SerializedName("name") var name: String = "",
    @SerializedName("nic") var nic: String = "",
    @SerializedName("primaryNumber") var primaryNumber: String = "",
    @SerializedName("secondaryNumber") var secondaryNumber: String = "",
    @SerializedName("townId") var townId: Int = -1,
    @SerializedName("epiNo") var epiNo: String = "",
    @SerializedName("ucId") var ucId: Int? = -1

)