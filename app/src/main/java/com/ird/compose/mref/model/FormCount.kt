package com.ird.compose.mref.model

data class FormCount(val date: String = "", var count: Int = 0)
