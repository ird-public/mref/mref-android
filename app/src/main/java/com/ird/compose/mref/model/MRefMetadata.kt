package com.ird.compose.mref.model


data class MRefMetadata (

	val referalReason : List<ReferalReason>,
	val locationType : List<LocationType>,
	val location : List<Location>
)