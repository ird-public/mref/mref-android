Mref Android Application
=============================


## Building the Mref App

First, clone the repo:

`git clone https://gitlab.com/ird-public/mref/mref-android.git`



Open the file `app/src/main/java/com/ird/compose/mref/di/modules/NetworkModule.kt` and change the url of mref web
collection name.

Building the sample then depends on your build tools.

### Developement Environment

````
1. Android Studio Electric Eel | 2022.1.1 Patch 2
2. Minimum Sdk Version 21 (Android Lollipop)
3. Compiled Sdk version : 31
4. gradle : 7.1.1

````

* Open Android Studio and select `File->Open...` or from the Android Launcher select `Import project (Eclipse ADT, Gradle, etc.)` and navigate to the root directory of your project.
* Select the directory or drill in and select the file `build.gradle` in the cloned repo.
* Click 'OK' to open the the project in Android Studio.
* A Gradle sync should start, but you can force a sync and build the 'app' module as needed.

### Gradle (command line)

* Build the APK: `./gradlew build`

### Eclipse

* Download the latest Android SDK from [Maven Central](http://repo1.maven.org/maven2/io/keen/keen-client-api-android)
    * Note: We publish both an AAR and a JAR; you may use whichever is more convenient based on your infrastructure and needs.


## Running the Sample App

Connect an Android device to your development machine.

### Android Studio

* Select `Run -> Run 'app'` (or `Debug 'app'`) from the menu bar
* Select the device you wish to run the app on and click 'OK'

### Gradle

* Install the debug APK on your device `./gradlew installDebug`
* Start the APK: `<path to Android SDK>/platform-tools/adb -d shell am start com.ird.compose.mref/com.ird.compose.mref.MainActivity.kt`


## Using the MREF App

To login into mref android application you need to create a user from dashboard.
