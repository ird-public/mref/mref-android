/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ird.compose.mref.buildsrc

object Versions {
    const val ktlint = "0.42.1"
}

object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:7.0.3"
    const val jdkDesugar = "com.android.tools:desugar_jdk_libs:1.1.5"

    const val junit = "junit:junit:4.13"

    const val material = "com.google.android.material:material:1.3.0"

    const val lottie = "com.airbnb.android:lottie-compose:4.2.0"

    object Accompanist {
        const val version = "0.21.0-beta"
        const val permissions = "com.google.accompanist:accompanist-permissions:$version"
    }

    object Kotlin {
        private const val version = "1.5.10"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
        const val extensions = "org.jetbrains.kotlin:kotlin-android-extensions:$version"
    }

    object Coroutines {
        private const val version = "1.5.2"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
        const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
    }

    object Retrofit {
        private const val version = "2.9.0"
        private const val okhttpVersion = "5.0.0-alpha.2"
        const val core = "com.squareup.retrofit2:retrofit:$version"
        const val gson = "com.squareup.retrofit2:converter-gson:$version"
        const val okhttp = "com.squareup.okhttp3:okhttp:$okhttpVersion"
        const val logging = "com.squareup.okhttp3:logging-interceptor:$okhttpVersion"

    }


    object Dagger {

        private const val version = "2.40.1"


        const val core = "com.google.dagger:dagger:$version"
        const val daggerAndroid = "com.google.dagger:dagger-android:$version"
        const val daggerAndroidSupport = "com.google.dagger:dagger-android-support:$version"
        const val annotationCompiler = "com.google.dagger:dagger-compiler:$version"
        const val androidProccesorCompiler = "com.google.dagger:dagger-android-processor:$version"

    }


    object ZXing {
        const val embedded = "com.journeyapps:zxing-android-embedded:4.3.0"
        const val desaguring = "com.android.tools:desugar_jdk_libs:1.1.5"

    }

    object Room {


        private const val version = "2.4.0-alpha05"

        const val room = "androidx.room:room-runtime:$version"
        const val annotationCompiler = "androidx.room:room-compiler:$version"


    }


    object AndroidX {
        const val appcompat = "androidx.appcompat:appcompat:1.3.0"
        const val coreKtx = "androidx.core:core-ktx:1.6.0"
        const val preference = "androidx.preference:preference:1.1.1"





        object Lifecycle {
            private const val version = "2.4.0-rc01"
            const val viewModelCompose = "androidx.lifecycle:lifecycle-viewmodel-compose:$version"
            const val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
        }

        object Multidex {
            const val multidex = "androidx.multidex:multidex:2.0.1"
        }

        object Activity {
            const val activityCompose = "androidx.activity:activity-compose:1.3.1"
        }

        object Compose {
            const val snapshot = ""
            const val version = "1.1.0-beta04"

            @get:JvmStatic
            val snapshotUrl: String
                get() = "https://androidx.dev/snapshots/builds/$snapshot/artifacts/repository/"

            const val foundation = "androidx.compose.foundation:foundation:$version"
            const val layout = "androidx.compose.foundation:foundation-layout:$version"
            const val material = "androidx.compose.material:material:$version"
            const val materialIconsExtended =
                "androidx.compose.material:material-icons-extended:$version"
            const val runtime = "androidx.compose.runtime:runtime:$version"
            const val runtimeLivedata = "androidx.compose.runtime:runtime-livedata:$version"
            const val tooling = "androidx.compose.ui:ui-tooling:$version"
            const val test = "androidx.compose.test:test-core:$version"
            const val uiTest = "androidx.compose.ui:ui-test:$version"
        }

        object Navigation {
            private const val version = "2.3.4"
            const val fragment = "androidx.navigation:navigation-fragment-ktx:$version"

            const val uiKtx = "androidx.navigation:navigation-ui-ktx:$version"
        }

        object Material {
            private const val version = "1.3.0"
            const val material = "com.google.android.material:material:$version"
        }

        object Test {
            private const val version = "1.4.0"
            const val core = "androidx.test:core:$version"
            const val rules = "androidx.test:rules:$version"

            object Ext {
                private const val version = "1.1.2"
                const val junit = "androidx.test.ext:junit-ktx:$version"
            }

            const val espressoCore = "androidx.test.espresso:espresso-core:3.2.0"
        }
    }

    object Appcenter {
        private const val version = "4.3.1"
        const val analytics = "com.microsoft.appcenter:appcenter-analytics:${version}"
        const val crashlytics = "com.microsoft.appcenter:appcenter-crashes:${version}"

    }

    object Coil {
        const val coilCompose = "io.coil-kt:coil-compose:1.3.2"
    }
}
